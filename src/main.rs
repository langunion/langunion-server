//! # Langunion-server
//! > Backend server for the whole langunion project
//! > Licensed under `GNU GPLv3`
//! Made with ♥ by me (Marc Mäurer).
//!
//! The goal is to create a service in which everyone can learn languages
//! and connect with other people
pub mod app;
pub mod cli;
pub mod config;
pub mod dbclient;
pub mod error;
pub mod extractors;
pub mod models;
pub mod openapi;
pub mod scopes;
pub mod server;
pub mod success;
pub mod utils;
pub mod ws;

use crate::app::App;
use crate::cli::{Cli, Commands};
use crate::config::use_config;
use crate::server::{AppState, Server};
use clap::Parser;
use log::debug;

/// Start the langunion server
#[actix_web::main]
async fn main() {
    App::init_logger();

    let cli: Cli = Cli::parse();
    debug!("{:#?}", &cli);

    let config = use_config(); // TODO pass config path into here or ::new_from_path

    if cli.commands == Commands::Run {
        Server::run(config).await;
    } else {
        cli.do_action(config).await;
    }
}
