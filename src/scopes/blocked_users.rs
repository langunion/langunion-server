use crate::dbclient::DbClient;
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{ClientUser, DbBlockedUser, ToPayload};
use crate::openapi::PaginatedResponseClientUser;
use crate::success::SuccessMessage;
use crate::utils::paginate::Paginator;
use crate::ws::events::{SocketAction, SocketEvent};
use crate::AppState;
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToResponse, ToSchema};
use validator::Validate;

pub fn blocked_users_scope() -> Scope {
    web::scope("/blocked-users")
        .route("/", web::get().to(get_blocked_users))
        .route("/", web::post().to(block_user))
        .route("/{user_id}/", web::get().to(get_blocked_user))
        .route("/{user_id}/", web::delete().to(unblock_user))
}

#[derive(Serialize, Deserialize, Validate, IntoParams)]
struct GetBlockedUsersQuery {
    #[validate(range(min = 1, message = "'page' param has to be at least 1"))]
    page: Option<usize>,
    #[validate(range(
        min = 1,
        max = 50,
        message = "'page_size' param has to be in the range of 1 and 50"
    ))]
    page_size: Option<usize>,
}

/// Get a list of all the users that you've blocked
#[utoipa::path(
    get,
    path = "/users/blocked-users/",
    params(GetBlockedUsersQuery),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status=200, response = PaginatedResponseClientUser),
    ),
)]
async fn get_blocked_users(
    query: web::Query<GetBlockedUsersQuery>,
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let query: GetBlockedUsersQuery = query.into_inner();
    let user_id: usize = auth_token.id;

    query
        .validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let mut users: Vec<ClientUser> = Vec::new();

    for blocked_user in app_state.dbclient.get_blocked_users(user_id, None).await {
        if let Some(user) = app_state
            .dbclient
            .get_user(blocked_user.blocked_user_id as usize, None)
            .await
        {
            users.push(
                user.into_client(
                    app_state.config.domain(),
                    app_state.websocket_server_handle.clone(),
                )
                .await,
            );
        }
    }

    users.sort_by_key(|user: &ClientUser| user.username.to_lowercase());

    Ok(HttpResponse::Ok().json(
        Paginator::new()
            .page(query.page)
            .page_size(query.page_size)
            .url(app_state.config.domain())
            .pathname("/blocked-users")
            .data(users)
            .into_paginated_response(),
    ))
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct BlockUserBody {
    #[schema(example = "2")]
    user_id: usize,
}

#[derive(Serialize, Deserialize, ToResponse)]
pub struct BlockUserResponse {
    pub message: String,
    pub result: ClientUser,
}

/// Block a user
/// Blocks and removes user from user's friends
#[utoipa::path(
    post,
    path = "/users/blocked-users/",
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    request_body=BlockUserBody,
    responses((status = 200, response = BlockUserResponse)),
)]
async fn block_user(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    body: web::Json<BlockUserBody>,
) -> Result<HttpResponse, HttpError> {
    let body: BlockUserBody = body.into_inner();
    let user_id: usize = auth_token.id;
    let block_user_id: usize = body.user_id;
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    if app_state
        .dbclient
        .get_user(block_user_id, Some(&mut transaction))
        .await
        .is_none()
    {
        return Err(HttpError::bad_request(ErrorMessage::NoUserFound));
    }

    if app_state
        .dbclient
        .get_friend(user_id, block_user_id, Some(&mut transaction))
        .await
        .is_some()
    {
        app_state
            .dbclient
            .delete_friend(user_id, block_user_id, Some(&mut transaction))
            .await?;
    }

    app_state
        .dbclient
        .block_user(user_id, block_user_id, Some(&mut transaction))
        .await?;

    let blocked_user: ClientUser = app_state
        .dbclient
        .get_user(block_user_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    app_state.websocket_server_handle.emit_socket_event(
        SocketEvent {
            action: SocketAction::BlockUser,
            payload: blocked_user.to_payload()?,
            message: "You've blocked a user".into(),
        },
        &[user_id],
    );

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(BlockUserResponse {
        message: SuccessMessage::BlockedUser.into(),
        result: blocked_user,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully got the user that you blocked")]
pub struct GetBlockedUserResponse {
    pub message: String,
    pub result: ClientUser,
}

/// Get a specific user
#[utoipa::path(
    get,
    path = "/users/blocked-users/{user_id}/",
    params(("user_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = GetBlockedUserResponse)),
)]
async fn get_blocked_user(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let blocked_user_id: usize = path.into_inner();

    let blocked_user: DbBlockedUser = app_state
        .dbclient
        .get_blocked_user(user_id, blocked_user_id, None)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::UserIsntBlocked))?;

    let user: ClientUser = app_state
        .dbclient
        .get_user(blocked_user.blocked_user_id as usize, None)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Ok().json(GetBlockedUserResponse {
        message: SuccessMessage::GotBlockedUser.into(),
        result: user,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Unblocked the user")]
pub struct UnblockUserResponse {
    pub message: String,
    pub result: ClientUser,
}

/// Unblock a user
#[utoipa::path(
    delete,
    path = "/users/blocked-users/{user_id}/",
    params(("user_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = UnblockUserResponse)),
)]
async fn unblock_user(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let unblock_user_id: usize = path.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    app_state
        .dbclient
        .unblock_user(user_id, unblock_user_id, Some(&mut transaction))
        .await?;

    let user: ClientUser = app_state
        .dbclient
        .get_user(unblock_user_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    app_state.websocket_server_handle.emit_socket_event(
        SocketEvent {
            action: SocketAction::UnblockUser,
            payload: user.to_payload()?,
            message: "You've unblocked a user".into(),
        },
        &[user_id],
    );

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(UnblockUserResponse {
        message: SuccessMessage::UnblockedUser.into(),
        result: user,
    }))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::VERSION;
    use crate::dbclient::DbClient;
    use crate::server::Server;
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils;
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, AppState};
    use actix_web::{body::MessageBody, http, test, web, App};
    use futures::{SinkExt, StreamExt};
    use pretty_assertions::assert_eq;
    use reqwest::Method;
    use serde_json::json;
    use sqlx::mysql::MySqlPool;
    use tokio::time::{sleep, Duration};
    use tokio_tungstenite::tungstenite::Message;

    #[sqlx::test]
    async fn test_get_blocked_users(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_blocked_users_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.block_user(2, 1, None).await.unwrap();

        let blocked_users: ClientUser = dbclient
            .get_user(1, None)
            .await
            .unwrap()
            .into_client(config.domain(), websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![blocked_users],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_blocked_users_next_url(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.block_user(1, 2, None).await.unwrap();
        dbclient.block_user(1, 3, None).await.unwrap();

        let user: ClientUser = dbclient
            .get_user(3, None)
            .await
            .unwrap()
            .into_client(config.domain(), websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/v1/blocked-users/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![user],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page=1&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_blocked_users_previous_url(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.block_user(1, 2, None).await.unwrap();
        dbclient.block_user(1, 3, None).await.unwrap();

        let user: ClientUser = dbclient
            .get_user(2, None)
            .await
            .unwrap()
            .into_client(config.domain(), websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some(
                "http://localhost:4000/v1/blocked-users/?page=1&page_size=1".to_string(),
            ),
            result: vec![user],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page=2&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_blocked_users_is_sorted_by_username(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("bleon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("athomas", "thomas@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.block_user(1, 2, None).await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient.block_user(1, 3, None).await.unwrap();

        let blocked_users: Vec<ClientUser> = vec![
            dbclient
                .get_user(3, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
            dbclient
                .get_user(2, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
        ];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: blocked_users,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_blocked_users_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_block_user(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(block_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(BlockUserBody { user_id: 2 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_block_user_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let blocked_user: ClientUser = dbclient
            .get_user(2, None)
            .await
            .unwrap()
            .into_client(config.domain(), websocket_server_handle.clone())
            .await;
        let expected: String = serde_json::to_string(&BlockUserResponse {
            message: SuccessMessage::BlockedUser.into(),
            result: blocked_user,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(block_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(BlockUserBody { user_id: 2 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_block_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(block_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(BlockUserBody { user_id: 2 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_block_user_who_is_already_blocked(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_blocked_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(block_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(BlockUserBody { user_id: 2 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_block_user_who_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(block_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(BlockUserBody { user_id: 2 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_block_user_removes_from_friends_if_they_are_friends(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_friend(1, 2, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(block_user)),
        )
        .await;

        test::call_service(
            &app,
            test::TestRequest::post()
                .uri("/")
                .insert_header((
                    http::header::AUTHORIZATION,
                    format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
                ))
                .set_json(BlockUserBody { user_id: 2 })
                .to_request(),
        )
        .await;
        assert!(dbclient.get_friend(1, 2, None).await.is_none());
    }

    #[sqlx::test]
    async fn test_block_user_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;

        let response: BlockUserResponse = test_utils::do_http_request(
            1,
            Method::POST,
            format!("http://127.0.0.1:{port}/{VERSION}/users/blocked-users/"),
            Some(json!({
                "user_id": 2,
            })),
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::BlockUser {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientUser =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
    }

    #[sqlx::test]
    async fn test_get_blocked_user(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_blocked_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_blocked_user)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_blocked_user_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_blocked_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let blocked_user: ClientUser = dbclient
            .get_user(2, None)
            .await
            .unwrap()
            .into_client(config.domain(), websocket_server_handle.clone())
            .await;
        let expected: String = serde_json::to_string(&GetBlockedUserResponse {
            message: SuccessMessage::GotBlockedUser.into(),
            result: blocked_user,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_blocked_user)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_blocked_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_blocked_user)),
        )
        .await;

        let req = test::TestRequest::default().uri("/2").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_blocked_user_who_isnt_blocked(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_blocked_user)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_unblock_user(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_blocked_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(unblock_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_unblock_user_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_blocked_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&UnblockUserResponse {
            message: SuccessMessage::UnblockedUser.into(),
            result: dbclient
                .get_user(2, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(unblock_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_unblock_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(unblock_user)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/2").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_unblock_user_who_wasnt_blocked(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(unblock_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_unblock_user_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.block_user(1, 2, None).await.unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;

        let response: UnblockUserResponse = test_utils::do_http_request(
            1,
            Method::DELETE,
            format!("http://127.0.0.1:{port}/{VERSION}/users/blocked-users/2"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UnblockUser {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientUser =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
    }
}
