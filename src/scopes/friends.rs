use crate::dbclient::DbClient;
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{ClientUser, DbFriend, ToPayload};
use crate::openapi::PaginatedResponseClientUser;
use crate::success::SuccessMessage;
use crate::utils::paginate::Paginator;
use crate::ws::events::{SocketAction, SocketEvent};
use crate::AppState;
use actix_web::{web, HttpResponse, Scope};
use log::error;
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToResponse};
use validator::Validate;

pub fn friends_scope() -> Scope {
    web::scope("/friends")
        .route("/", web::get().to(get_friends))
        .route("/{user_id}/", web::get().to(get_friend))
        .route("/{user_id}/", web::delete().to(remove_friend))
}

#[derive(Serialize, Deserialize, Validate, IntoParams)]
pub struct GetFriendsQuery {
    #[validate(range(min = 1, message = "'page' param has to be at least 1"))]
    page: Option<usize>,
    #[validate(range(
        min = 1,
        max = 50,
        message = "'page_size' param has to be in the range of 1 and 50"
    ))]
    page_size: Option<usize>,
}

/// Get a list of all your friends
#[utoipa::path(
    get,
    path = "/users/friends/",
    params(GetFriendsQuery),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = PaginatedResponseClientUser)),
)]
async fn get_friends(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    query: web::Query<GetFriendsQuery>,
) -> HttpResponse {
    let query: GetFriendsQuery = query.into_inner();
    let user_id: usize = auth_token.id;
    let friend_items: Vec<DbFriend> = app_state.dbclient.get_friends(user_id, None).await;
    let mut friends: Vec<ClientUser> = Vec::new();

    // TODO Pass this logic into the paginator with FnOnce
    // with that this logic, which is time intensive, can be done only on the
    // neccessary items of the data/result
    for friend_item in friend_items {
        let user_id_of_friend: usize = friend_item.get_user_id_of_friend(user_id).unwrap();

        if let Some(friend) = app_state.dbclient.get_user(user_id_of_friend, None).await {
            friends.push(
                friend
                    .into_client(
                        app_state.config.domain(),
                        app_state.websocket_server_handle.clone(),
                    )
                    .await,
            );
        }
    }
    friends.sort_by_key(|user: &ClientUser| user.username.to_lowercase());

    HttpResponse::Ok().json(
        Paginator::new()
            .page(query.page)
            .page_size(query.page_size)
            .url(app_state.config.domain())
            .pathname("/friends")
            .data(friends)
            .into_paginated_response(),
    )
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully got your friend")]
pub struct GetFriendResponse {
    pub message: String,
    pub result: ClientUser,
}

/// Get a specific friend
#[utoipa::path(
    get,
    path = "/users/friends/{user_id}/",
    params(("user_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = GetFriendResponse)),
)]
async fn get_friend(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let user_id_of_friend: usize = path.into_inner();

    // Check if they're friends
    app_state
        .dbclient
        .get_friend(user_id, user_id_of_friend, None)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoFriendFound))?;

    let friend: ClientUser = app_state
        .dbclient
        .get_user(user_id_of_friend, None)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Ok().json(GetFriendResponse {
        message: SuccessMessage::GotFriend.into(),
        result: friend,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully removed your friend")]
pub struct RemoveFriendResponse {
    pub message: String,
    pub result: ClientUser,
}

/// Unfriend a user
#[utoipa::path(
    delete,
    path = "/users/friends/{user_id}/",
    params(("user_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = RemoveFriendResponse)),
)]
async fn remove_friend(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let user_id_of_friend: usize = path.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    /* Check if they're friends and get the user for socket event */
    let friend: DbFriend = app_state
        .dbclient
        .get_friend(user_id, user_id_of_friend, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoFriendFound))?;
    let friend: ClientUser = app_state
        .dbclient
        .get_user(
            friend.get_user_id_of_friend(user_id).unwrap(),
            Some(&mut transaction),
        )
        .await
        .ok_or_else(|| {
            error!("Friend found but no user found");
            HttpError::server_error()
        })?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;
    let user: ClientUser = app_state
        .dbclient
        .get_user(user_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    /* Try to remove the friend */
    app_state
        .dbclient
        .delete_friend(user_id, user_id_of_friend, Some(&mut transaction))
        .await?;

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::RemoveFriend,
                payload: friend.to_payload()?,
                message: "You've removed a friend".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::RemoveFriend,
                payload: user.to_payload()?,
                message: "A friend of yours removed you from his friends".into(),
            },
            &[user_id_of_friend],
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(RemoveFriendResponse {
        message: SuccessMessage::RemovedFriend.into(),
        result: friend,
    }))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::VERSION;
    use crate::server::Server;
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils;
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, dbclient::DbClient, AppState};
    use actix_web::{body::MessageBody, http, test, web, App};
    use futures::{SinkExt, StreamExt};
    use pretty_assertions::{assert_eq, assert_ne};
    use reqwest::Method;
    use sqlx::mysql::MySqlPool;
    use tokio::time::{sleep, Duration};
    use tokio_tungstenite::tungstenite::Message;

    #[sqlx::test]
    async fn test_get_friends(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_friends_paginated_response(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        let friend: ClientUser = dbclient
            .get_user(2, None)
            .await
            .unwrap()
            .into_client(config.domain(), websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![friend],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friends_next_url(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("amarc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("martin", "martin@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(1, 3, None).await.unwrap();

        let user: ClientUser = dbclient
            .get_user(2, None)
            .await
            .unwrap()
            .into_client(config.domain(), websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/v1/friends/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![user],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page_size=1&page=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friends_previous_url(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("amarc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("martin", "martin@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(1, 3, None).await.unwrap();

        let user: ClientUser = dbclient
            .get_user(3, None)
            .await
            .unwrap()
            .into_client(config.domain(), websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some("http://localhost:4000/v1/friends/?page=1&page_size=1".to_string()),
            result: vec![user],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page_size=1&page=2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friends_is_sorted_by_username(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("thomas", "thomas@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(1, 3, None).await.unwrap();

        let friends: Vec<ClientUser> = vec![
            dbclient
                .get_user(3, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
            dbclient
                .get_user(2, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
        ];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: friends,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friends_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_friend)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_friend_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&GetFriendResponse {
            message: SuccessMessage::GotFriend.into(),
            result: dbclient
                .get_user(2, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_friend)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friend_but_isnt_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_friend)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_friend_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_friend)),
        )
        .await;

        let req = test::TestRequest::default().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_remove_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(remove_friend)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_remove_friend_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&RemoveFriendResponse {
            message: SuccessMessage::RemovedFriend.into(),
            result: dbclient
                .get_user(2, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(remove_friend)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_remove_friend_who_isnt_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(remove_friend)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_remove_friend_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: RemoveFriendResponse = test_utils::do_http_request(
            1,
            Method::DELETE,
            format!("http://127.0.0.1:{port}/{VERSION}/users/friends/2"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::RemoveFriend {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientUser =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::RemoveFriend {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientUser =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_ne!(socket_event_client_two_payload, response.result);
    }

    #[sqlx::test]
    async fn test_remove_friend_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(remove_friend)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }
}
