use crate::dbclient::DbClient;
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{ClientMessage, ClientUser, DbMessage, ToPayload};
use crate::openapi::PaginatedResponseClientMessage;
use crate::success::SuccessMessage;
use crate::utils::paginate::Paginator;
use crate::ws::events::{SocketAction, SocketEvent};
use crate::AppState;
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToResponse, ToSchema};
use validator::Validate;

/// Scope for the "/messages" route
pub fn messages_scope() -> Scope {
    web::scope("/messages")
        .route("/", web::get().to(get_messages))
        .route("/", web::post().to(send_message))
        .route("/{message_id}/", web::get().to(get_message))
        .route("/{message_id}/", web::patch().to(update_message))
        .route("/{message_id}/", web::delete().to(delete_message))
        .route(
            "/{message_id}/delete/",
            web::patch().to(delete_message_only_for_me),
        )
        .route("/{user_id}/chat/", web::get().to(get_chat))
        .route("/{user_id}/chat/", web::delete().to(delete_chat))
}

#[derive(Serialize, Deserialize, Validate, IntoParams)]
struct GetMessagesQuery {
    #[validate(range(min = 1, message = "'page' param has to be at least 1"))]
    page: Option<usize>,
    #[validate(range(
        min = 1,
        max = 50,
        message = "'page_size' param has to be in the range of 1 and 50"
    ))]
    page_size: Option<usize>,
}

/// Get a list of all your messages
#[utoipa::path(
    get,
    path = "/messages/",
    params(GetMessagesQuery),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status=200, response = PaginatedResponseClientMessage)),
)]
async fn get_messages(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    query: web::Query<GetMessagesQuery>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let query: GetMessagesQuery = query.into_inner();

    query
        .validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let messages: Vec<ClientMessage> = app_state
        .dbclient
        .get_messages(user_id, None)
        .await
        .into_iter()
        .map(|m: DbMessage| m.into_client(app_state.config.domain()))
        .collect();

    Ok(HttpResponse::Ok().json(
        Paginator::new()
            .page(query.page)
            .page_size(query.page_size)
            .url(app_state.config.domain())
            .pathname("/messages")
            .data(messages)
            .into_paginated_response(),
    ))
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct SendMessageBody {
    to_user: usize,
    content: String,
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Created a new message")]
pub struct SendMessageResponse {
    message: String,
    result: ClientMessage,
}

/// Send a message
#[utoipa::path(
    post,
    path = "/messages/",
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    request_body=SendMessageBody,
    responses((status = 200, response = SendMessageResponse)),
)]
async fn send_message(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    body: web::Json<SendMessageBody>,
) -> Result<HttpResponse, HttpError> {
    let body: SendMessageBody = body.into_inner();
    let user_id: usize = auth_token.id;
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    let message_id: usize = app_state
        .dbclient
        .save_message(user_id, body.to_user, body.content, Some(&mut transaction))
        .await?;
    let message: ClientMessage = app_state
        .dbclient
        .get_message(message_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::server_error())?
        .into_client(app_state.config.domain());

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::SendMessage,
                payload: message.to_payload()?,
                message: "You've send a message".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::SendMessage,
                payload: message.to_payload()?,
                message: "You received a message".into(),
            },
            &[body.to_user],
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(SendMessageResponse {
        message: SuccessMessage::SentMessage.into(),
        result: message,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully got the message")]
pub struct GetMessageResponse {
    message: String,
    result: ClientMessage,
}

/// Get a specific message
#[utoipa::path(
    get,
    path = "/messages/{message_id}/",
    params(("message_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = GetMessageResponse)),
)]
async fn get_message(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let message_id: usize = path.into_inner();
    let message: ClientMessage = app_state
        .dbclient
        .get_message(message_id, None)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoMessageFound))
        .and_then(|message: DbMessage| {
            let is_allowed_to_see: bool =
                user_id == message.sender as usize || user_id == message.receiver as usize;
            if !is_allowed_to_see {
                return Err(HttpError::bad_request(ErrorMessage::NotYourMessage));
            }
            Ok(message)
        })?
        .into_client(app_state.config.domain());

    Ok(HttpResponse::Ok().json(GetMessageResponse {
        message: SuccessMessage::GotMessage.into(),
        result: message,
    }))
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct UpdateMessageBody {
    content: Option<String>,
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully edited one of your messages")]
pub struct UpdateMessageResponse {
    message: String,
    result: ClientMessage,
}

/// Update the content of a message
#[utoipa::path(
    patch,
    path = "/messages/{message_id}/",
    params(("message_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    request_body=UpdateMessageBody,
    responses((status = 200, response = UpdateMessageResponse)),
)]
async fn update_message(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
    body: web::Json<UpdateMessageBody>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let message_id: usize = path.into_inner();
    let body: UpdateMessageBody = body.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    // check if its legal to update/edit the message
    let (sender, receiver): (usize, usize) = app_state
        .dbclient
        .get_message(message_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoMessageFound))
        .and_then(
            |message: DbMessage| match user_id == message.sender as usize {
                true => Ok((message.sender as usize, message.receiver as usize)),
                false => Err(HttpError::bad_request(ErrorMessage::NotYourMessage)),
            },
        )?;

    if let Some(new_content) = &body.content {
        app_state
            .dbclient
            .update_message(message_id, new_content, Some(&mut transaction))
            .await?;
    }

    let updated_message: ClientMessage = app_state
        .dbclient
        .get_message(message_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::server_error())?
        .into_client(app_state.config.domain());

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::UpdateMessage,
                payload: updated_message.to_payload()?,
                message: "You've updated a message".into(),
            },
            &[sender],
        ),
        (
            SocketEvent {
                action: SocketAction::UpdateMessage,
                payload: updated_message.to_payload()?,
                message: "A message has been updated".into(),
            },
            &[receiver],
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(UpdateMessageResponse {
        message: SuccessMessage::UpdatedMessage.into(),
        result: updated_message,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully removed the message")]
pub struct DeleteMessageResponse {
    message: String,
    result: ClientMessage,
}

/// Delete a message
/// Deletes a message for good
#[utoipa::path(
    delete,
    path = "/messages/{message_id}/",
    params(("message_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = DeleteMessageResponse)),
)]
async fn delete_message(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let message_id: usize = path.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    let (sender, receiver, deleted_message): (usize, usize, ClientMessage) = app_state
        .dbclient
        .get_message(message_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoMessageFound))
        .and_then(|msg: DbMessage| {
            if msg.sender as usize == user_id {
                return Ok((
                    msg.sender as usize,
                    msg.receiver as usize,
                    msg.into_client(app_state.config.domain()),
                ));
            } else {
                return Err(HttpError::bad_request(ErrorMessage::CannotDeleteMessage));
            }
        })?;

    app_state
        .dbclient
        .delete_message(message_id, Some(&mut transaction))
        .await?;

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::DeleteMessage,
                payload: deleted_message.to_payload()?,
                message: "You've deleted a message".into(),
            },
            &[sender],
        ),
        (
            SocketEvent {
                action: SocketAction::DeleteMessage,
                payload: deleted_message.to_payload()?,
                message: "A message has been deleted".into(),
            },
            &[receiver],
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(DeleteMessageResponse {
        message: SuccessMessage::DeletedMessage.into(),
        result: deleted_message,
    }))
}

/// "Privately" deletes a message
/// Remove a message only on your side, it doesnt actually removes the message
#[utoipa::path(
    patch,
    path = "/messages/{message_id}/delete/",
    params(("message_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = DeleteMessageResponse)),
)]
async fn delete_message_only_for_me(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let message_id: usize = path.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    // check if its legal to delete
    let message: DbMessage = app_state
        .dbclient
        .get_message(message_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoMessageFound))?;

    let is_sender: bool = user_id == message.sender as usize;
    let is_receiver: bool = user_id == message.receiver as usize;

    if is_sender {
        app_state
            .dbclient
            .delete_message_for_sender(message_id, Some(&mut transaction))
            .await?;
    } else if is_receiver {
        app_state
            .dbclient
            .delete_message_for_receiver(message_id, Some(&mut transaction))
            .await?;
    } else {
        return Err(HttpError::bad_request(ErrorMessage::NotYourMessage));
    }

    let message: ClientMessage = message.into_client(app_state.config.domain());

    app_state.websocket_server_handle.emit_socket_events(&[(
        SocketEvent {
            action: SocketAction::DeleteMessagePrivately,
            payload: message.to_payload()?,
            message: "You've privately deleted a message".into(),
        },
        &[user_id],
    )]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(DeleteMessageResponse {
        message: SuccessMessage::DeletedMessage.into(),
        result: message,
    }))
}

#[derive(Serialize, Deserialize, Validate, IntoParams)]
struct GetChatQuery {
    #[validate(range(min = 1, message = "'page' param has to be at least 1"))]
    page: Option<usize>,
    #[validate(range(
        min = 1,
        max = 50,
        message = "'page_size' param has to be in the range of 1 and 50"
    ))]
    page_size: Option<usize>,
}

/// Get the whole chat with a user
#[utoipa::path(
    get,
    path = "/messages/{user_id}/chat/",
    params(GetChatQuery, ("user_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status=200, response = PaginatedResponseClientMessage)),
)]
async fn get_chat(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
    query: web::Query<GetChatQuery>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let user_id_of_friend: usize = path.into_inner();
    let query: GetChatQuery = query.into_inner();

    query
        .validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let messages: Vec<ClientMessage> = app_state
        .dbclient
        .get_chat(user_id, user_id_of_friend, None)
        .await
        .into_iter()
        .map(|m: DbMessage| m.into_client(app_state.config.domain()))
        .collect();

    Ok(HttpResponse::Ok().json(
        Paginator::new()
            .page(query.page)
            .page_size(query.page_size)
            .pathname("/messages/chat")
            .data(messages)
            .into_paginated_response(),
    ))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully deleted the whole chat")]
pub struct DeleteChatResponse {
    message: String,
    result: ClientUser,
}

/// Delete a whole chat with a user
#[utoipa::path(
    delete,
    path = "/messages/{user_id}/chat/",
    params(("user_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status=200, response = DeleteChatResponse)),
)]
async fn delete_chat(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let user_id_of_friend: usize = path.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    app_state
        .dbclient
        .delete_chat(user_id, user_id_of_friend, Some(&mut transaction))
        .await?;

    let user_of_deleted_chat: ClientUser = app_state
        .dbclient
        .get_user(user_id_of_friend, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    app_state.websocket_server_handle.emit_socket_events(&[(
        SocketEvent {
            action: SocketAction::DeleteChat,
            payload: user_of_deleted_chat.to_payload()?,
            message: "You've deleted a whole chat".into(),
        },
        &[user_id],
    )]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(DeleteChatResponse {
        message: SuccessMessage::DeletedChat.into(),
        result: user_of_deleted_chat,
    }))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::VERSION;
    use crate::dbclient::DbClient;
    use crate::server::Server;
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils;
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, AppState};
    use actix_web::{body::MessageBody, http, test, web, App};
    use futures::{SinkExt, StreamExt};
    use pretty_assertions::{assert_eq, assert_ne};
    use reqwest::Method;
    use serde_json::json;
    use sqlx::mysql::MySqlPool;
    use tokio::time::{sleep, Duration};
    use tokio_tungstenite::tungstenite::Message;

    #[sqlx::test]
    async fn test_get_messages(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_messages_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let message_id: usize = dbclient.save_message(1, 2, "Hi", None).await.unwrap();
        let message: ClientMessage = dbclient
            .get_message(message_id, None)
            .await
            .unwrap()
            .into_client(config.domain());

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_messages_next_url(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let message_id: usize = dbclient.save_message(1, 2, "hi", None).await.unwrap();
        dbclient.get_message(message_id, None).await.unwrap();

        sleep(Duration::from_secs(1)).await;

        let message_id: usize = dbclient.save_message(1, 2, "hii", None).await.unwrap();
        let message = dbclient
            .get_message(message_id, None)
            .await
            .unwrap()
            .into_client(config.domain());

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/v1/messages/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page=1&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_messages_previous_url(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let message_id: usize = dbclient.save_message(1, 2, "hi", None).await.unwrap();
        let message: ClientMessage = dbclient
            .get_message(message_id, None)
            .await
            .unwrap()
            .into_client(config.domain());

        sleep(Duration::from_secs(1)).await;

        dbclient.save_message(1, 2, "hey", None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some("http://localhost:4000/v1/messages/?page=1&page_size=1".to_string()),
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page=2&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_messages_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        let first_id: usize = dbclient.save_message(1, 2, "Hi", None).await.unwrap();
        let first: ClientMessage = dbclient
            .get_message(first_id, None)
            .await
            .unwrap()
            .into_client(config.domain());
        let second_id: usize = dbclient.save_message(1, 2, "Hi", None).await.unwrap();
        let second: ClientMessage = dbclient
            .get_message(second_id, None)
            .await
            .unwrap()
            .into_client(config.domain());

        let messages: Vec<ClientMessage> = vec![second, first];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: messages,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_messages_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_send_message(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(SendMessageBody {
                to_user: 2,
                content: "Hi".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_send_message_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(SendMessageBody {
                to_user: 2,
                content: "Hi".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        let expected: String = serde_json::to_string(&SendMessageResponse {
            message: SuccessMessage::SentMessage.into(),
            result: dbclient
                .get_message(1, None)
                .await
                .unwrap()
                .into_client(config.domain()),
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_send_message_to_user_who_isnt_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(SendMessageBody {
                to_user: 999,
                content: "Hi".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_send_message_to_yourself(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(SendMessageBody {
                to_user: 1,
                content: "Hi".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_send_message_to_blocked_user(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.block_user(1, 2, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(SendMessageBody {
                to_user: 2,
                content: "Hi".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_send_message_with_no_content(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(SendMessageBody {
                to_user: 2,
                content: "".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_send_message_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: SendMessageResponse = test_utils::do_http_request(
            1,
            Method::POST,
            format!("http://127.0.0.1:{port}/{VERSION}/messages/"),
            Some(json!({
                "to_user": 2,
                "content": "hi how are you?",
            })),
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::SendMessage {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientMessage =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::SendMessage {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientMessage =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
    }

    #[sqlx::test]
    async fn test_send_message_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_message(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::get().to(get_message)),
        )
        .await;

        let req = test::TestRequest::get()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_message_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&GetMessageResponse {
            message: SuccessMessage::GotMessage.into(),
            result: dbclient
                .get_message(1, None)
                .await
                .unwrap()
                .into_client(config.domain()),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::get().to(get_message)),
        )
        .await;

        let req = test::TestRequest::get()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_message_that_isnt_mine(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::get().to(get_message)),
        )
        .await;

        let req = test::TestRequest::get()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_message_that_doesnt_exist(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::get().to(get_message)),
        )
        .await;

        let req = test::TestRequest::get()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_message_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::get().to(get_message)),
        )
        .await;

        let req = test::TestRequest::get().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_update_message(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                content: Some("new content".to_string()),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_message_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                content: Some("new content".to_string()),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        let expected: String = serde_json::to_string(&UpdateMessageResponse {
            message: SuccessMessage::UpdatedMessage.to_string(),
            result: dbclient
                .get_message(1, None)
                .await
                .unwrap()
                .into_client(config.domain()),
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_update_message_with_no_content(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                content: Some("".to_string()),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_message_that_doesnt_exist(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/999")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                content: Some("F".to_string()),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_message_that_isnt_mine(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                content: Some("F".to_string()),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_message_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient
            .save_message(1, 2, "hi how are you?", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: UpdateMessageResponse = test_utils::do_http_request(
            1,
            Method::PATCH,
            format!("http://127.0.0.1:{port}/{VERSION}/messages/1"),
            Some(json!({
                "content": "nevermind",
            })),
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UpdateMessage {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientMessage =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UpdateMessage {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientMessage =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
    }

    #[sqlx::test]
    async fn test_update_message_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_update_message_updates_content(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_message(1, 2, "old content", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                content: Some("new content".to_string()),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let updated_message: DbMessage = dbclient.get_message(1, None).await.unwrap();
        assert_eq!(updated_message.content, "new content");
    }

    #[sqlx::test]
    async fn test_update_message_updates_edited_at(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let old_message_id: usize = dbclient
            .save_message(1, 2, "old content", None)
            .await
            .unwrap();
        let old_message: DbMessage = dbclient.get_message(old_message_id, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                content: Some("new content".to_string()),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let updated_message: DbMessage = dbclient.get_message(1, None).await.unwrap();
        assert_ne!(old_message.edited_at, updated_message.edited_at);
    }

    #[sqlx::test]
    async fn test_delete_message(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_delete_message_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&DeleteMessageResponse {
            message: SuccessMessage::DeletedMessage.to_string(),
            result: dbclient
                .get_message(1, None)
                .await
                .unwrap()
                .into_client(config.domain()),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_delete_message_that_doesnt_exist(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_message_that_isnt_mine(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_message_removes_the_message(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let _ = test::call_service(&app, req).await;
        assert!(dbclient.get_message(1, None).await.is_none());
    }

    #[sqlx::test]
    async fn test_delete_message_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient
            .save_message(1, 2, "hi how are you?", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: DeleteMessageResponse = test_utils::do_http_request(
            1,
            Method::DELETE,
            format!("http://127.0.0.1:{port}/{VERSION}/messages/1"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::DeleteMessage {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientMessage =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::DeleteMessage {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientMessage =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
    }

    #[sqlx::test]
    async fn test_delete_message_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_message_only_for_me(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(delete_message_only_for_me)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_delete_message_only_for_me_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&DeleteMessageResponse {
            message: SuccessMessage::DeletedMessage.to_string(),
            result: dbclient
                .get_message(1, None)
                .await
                .unwrap()
                .into_client(config.domain()),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(delete_message_only_for_me)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_delete_message_only_for_me_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(delete_message_only_for_me)),
        )
        .await;

        let req = test::TestRequest::patch().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_message_only_for_me_is_that_doesnt_exist(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(delete_message_only_for_me)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_message_only_for_me_is_that_isnt_mine(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(delete_message_only_for_me)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_message_only_for_me_as_the_sender(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_message(1, 2, "hii", None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(delete_message_only_for_me)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let message: DbMessage = dbclient.get_message(1, None).await.unwrap();
        assert!(message.is_deleted_by_sender.is_some());
    }

    #[sqlx::test]
    async fn test_delete_message_only_for_me_as_the_receiver(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_message(1, 2, "hii", None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(delete_message_only_for_me)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .to_request();
        let _ = test::call_service(&app, req).await;

        let message: DbMessage = dbclient.get_message(1, None).await.unwrap();
        assert!(message.is_deleted_by_receiver.is_some());
    }

    #[sqlx::test]
    async fn test_delete_message_only_for_me_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient
            .save_message(1, 2, "hi how are you?", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;

        let response: DeleteMessageResponse = test_utils::do_http_request(
            1,
            Method::PATCH,
            format!("http://127.0.0.1:{port}/{VERSION}/messages/1/delete"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::DeleteMessagePrivately {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientMessage =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;
        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
    }

    #[sqlx::test]
    async fn test_get_chat(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_chat_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let message_id: usize = dbclient.save_message(1, 2, "Hi", None).await.unwrap();
        let message: ClientMessage = dbclient
            .get_message(message_id, None)
            .await
            .unwrap()
            .into_client(config.domain());

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_next_url(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_message(1, 2, "hi", None).await.unwrap();

        sleep(Duration::from_secs(1)).await;

        let message_id: usize = dbclient.save_message(1, 2, "hey", None).await.unwrap();
        let message: ClientMessage = dbclient
            .get_message(message_id, None)
            .await
            .unwrap()
            .into_client(config.domain());

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/messages/chat/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2?page=1&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_previous_url(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let message_id: usize = dbclient.save_message(1, 2, "hi", None).await.unwrap();
        let message: ClientMessage = dbclient
            .get_message(message_id, None)
            .await
            .unwrap()
            .into_client(config.domain());

        sleep(Duration::from_secs(1)).await;

        dbclient.save_message(1, 2, "hey", None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some("http://localhost:4000/messages/chat/?page=1&page_size=1".to_string()),
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2?page=2&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        let first_id: usize = dbclient.save_message(1, 2, "Hi", None).await.unwrap();
        let first: ClientMessage = dbclient
            .get_message(first_id, None)
            .await
            .unwrap()
            .into_client(config.domain());
        let second_id: usize = dbclient.save_message(1, 2, "Hey", None).await.unwrap();
        let second: ClientMessage = dbclient
            .get_message(second_id, None)
            .await
            .unwrap()
            .into_client(config.domain());

        let messages: Vec<ClientMessage> = vec![second, first];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: messages,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default().uri("/2").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_chat(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(delete_chat)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_delete_chat_deleted_chat(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(delete_chat)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let deleted_chat: Vec<DbMessage> = dbclient_clone.get_chat(1, 2, None).await;
        for message in deleted_chat {
            let is_soft_deleted: bool =
                message.is_deleted_by_sender.is_some() || message.is_deleted_by_receiver.is_some();
            assert!(is_soft_deleted);
        }
    }

    #[sqlx::test]
    async fn test_delete_chat_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&DeleteChatResponse {
            message: SuccessMessage::DeletedChat.into(),
            result: dbclient
                .get_user(2, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(delete_chat)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_delete_chat_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient
            .save_message(1, 2, "hi how are you?", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;

        let response: DeleteChatResponse = test_utils::do_http_request(
            1,
            Method::DELETE,
            format!("http://127.0.0.1:{port}/{VERSION}/messages/1/chat"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::DeleteChat {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientUser =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;
        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
    }

    #[sqlx::test]
    async fn test_delete_chat_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(delete_chat)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/2").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }
}
