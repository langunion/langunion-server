use crate::app::AVATAR_DIRECTORY;
use crate::dbclient::DbClient;
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{
    ClientUser, DbBlockedUser, DbFriend, DbFriendrequest, DbPost, DbUser, Status, ToPayload,
};
use crate::openapi::PaginatedResponseClientUser;
use crate::scopes::{
    blocked_users::blocked_users_scope, chat_partners::chat_partners_scope, friends::friends_scope,
};
use crate::server::AppState;
use crate::success::SuccessMessage;
use crate::utils::paginate::Paginator;
use crate::utils::{password_utils, storage, upload};
use crate::ws::events::{SocketAction, SocketEvent};
use actix_multipart::form::{tempfile::TempFile, MultipartForm};
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use tokio::fs;
use utoipa::{IntoParams, ToResponse, ToSchema};
use validator::Validate;

/// Scope aka. Router for the "/users" route
pub fn users_scope() -> Scope {
    web::scope("/users")
        .service(friends_scope())
        .service(blocked_users_scope())
        .service(chat_partners_scope())
        .route("/", web::get().to(get_users))
        .route("/", web::post().to(register_user))
        .route("/", web::delete().to(delete_user))
        .route("/", web::patch().to(update_user))
        .route("/me/", web::get().to(get_me))
        .route("/{user_id}/", web::get().to(get_user))
        .route("/{user_id}/avatar/", web::get().to(get_avatar))
        .route("/{user_id}/avatar/", web::post().to(update_avatar))
}

#[derive(Serialize, Deserialize, Validate, IntoParams)]
struct GetUsersQuery {
    /// Query to fuzzy search users
    query: Option<String>,
    #[validate(range(min = 1, message = "'page' param has to be at least 1"))]
    page: Option<usize>,
    #[validate(range(
        min = 1,
        max = 50,
        message = "'page_size' param has to be in the range of 1 and 50"
    ))]
    page_size: Option<usize>,
    /// Filter my account from the results
    filter_me: Option<bool>,
    /// Filter all my friends from the results
    filter_friends: Option<bool>,
    /// Filter all pending friend requests from the results
    filter_pending_friendrequests: Option<bool>,
    /// Filter all the users that I've blocked from the results
    filter_blocked_users: Option<bool>,
}

/// Get users
/// Fuzzy search for a list of users
#[utoipa::path(
    get,
    path = "/users/",
    params(GetUsersQuery),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status=200, response = PaginatedResponseClientUser)),
)]
async fn get_users(
    query: web::Query<GetUsersQuery>,
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let query_params: GetUsersQuery = query.into_inner();
    let user_id: usize = auth_token.id;

    query_params
        .validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let query: String = query_params.query.unwrap_or(String::new());
    let filter_me: bool = query_params.filter_me.unwrap_or(false);
    let filter_friends: bool = query_params.filter_friends.unwrap_or(false);
    let filter_pending_friendrequests: bool =
        query_params.filter_pending_friendrequests.unwrap_or(false);
    let filter_blocked_users: bool = query_params.filter_blocked_users.unwrap_or(false);

    let mut users: Vec<ClientUser> = Vec::new();

    let friends: Vec<usize> = {
        match filter_friends {
            true => app_state
                .dbclient
                .get_friends(user_id, None)
                .await
                .into_iter()
                .map(|friend: DbFriend| friend.get_user_id_of_friend(user_id).unwrap())
                .collect(),
            false => Vec::new(),
        }
    };

    let friendrequests: Vec<usize> = {
        match filter_pending_friendrequests {
            true => app_state
                .dbclient
                .get_friendrequests(user_id, None)
                .await
                .into_iter()
                .map(|req: DbFriendrequest| {
                    if req.from_user as usize == user_id {
                        return Some(req.to_user);
                    }

                    if req.to_user as usize == user_id {
                        return Some(req.from_user);
                    }

                    None
                })
                .map(|user_id: Option<u64>| user_id.unwrap() as usize)
                .collect(),
            false => Vec::new(),
        }
    };

    let blocked_users: Vec<usize> = {
        match filter_blocked_users {
            true => app_state
                .dbclient
                .get_blocked_users(user_id, None)
                .await
                .into_iter()
                .map(|blocked_user: DbBlockedUser| blocked_user.blocked_user_id as usize)
                .collect(),
            false => Vec::new(),
        }
    };

    if !query.is_empty() {
        let db_users: Vec<DbUser> = app_state.dbclient.get_users(&query, None).await;
        for db_user in db_users {
            let db_user_id: usize = db_user.id as usize;

            if filter_me && db_user_id == user_id {
                continue;
            }

            if filter_friends && friends.contains(&db_user_id) {
                continue;
            }

            if filter_pending_friendrequests && friendrequests.contains(&db_user_id) {
                continue;
            }

            if filter_blocked_users && blocked_users.contains(&db_user_id) {
                continue;
            }

            users.push(
                db_user
                    .into_client(
                        app_state.config.domain(),
                        app_state.websocket_server_handle.clone(),
                    )
                    .await,
            );
        }
    }

    Ok(HttpResponse::Ok().json(
        Paginator::new()
            .page(query_params.page)
            .page_size(query_params.page_size)
            .url(app_state.config.domain())
            .pathname("/users")
            .extra_params(vec![("query".to_string(), query)])
            .data(users)
            .into_paginated_response(),
    ))
}

#[derive(Serialize, Deserialize, Validate, ToSchema)]
pub struct RegisterUserBody {
    #[validate(
        length(
            min = 4,
            max = 20,
            message = "Username has to be 4 to 20 characters long"
        ),
        does_not_contain(pattern = " ", message = "Username should not contain any spaces")
    )]
    #[schema(example = "marc")]
    username: String,
    #[validate(email(message = "Has to be email"))]
    #[schema(example = "marc@langunion.com")]
    email: Option<String>,
    #[validate(length(
        min = 8,
        max = 2000,
        message = "Password has to be at least 4 to 20 characters long and does not contain any spaces"
    ))]
    password: String,
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully created a new account")]
pub struct RegisterUserResponse {
    pub message: String,
    pub result: ClientUser,
}

/// Create a new account
/// Register aka. create a new account
#[utoipa::path(
    post,
    path = "/users/",
    request_body=RegisterUserBody,
    responses((status = 201, response = RegisterUserResponse)),
)]
async fn register_user(
    body: web::Json<RegisterUserBody>,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let body: RegisterUserBody = body.into_inner();

    body.validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    /* try to save user in DB (checks also for username, email) */
    let email: String = body.email.unwrap_or(String::new());
    app_state
        .dbclient
        .save_user(&body.username, &email, &body.password, None)
        .await?;

    let user: ClientUser = app_state
        .dbclient
        .get_user_by_email(email, None)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Created().json(RegisterUserResponse {
        message: SuccessMessage::Register.into(),
        result: user,
    }))
}

#[derive(Serialize, Deserialize, Validate, ToSchema)]
pub struct DeleteUserBody {
    #[validate(email(message = "Has to be email"))]
    #[schema(example = "marc@langunion.com")]
    email: String,
    #[validate(length(
        min = 8,
        max = 2000,
        message = "Password has to be at least 8 characters long"
    ))]
    password: String,
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Your account has been deleted")]
pub struct DeleteUserResponse {
    pub message: String,
    pub result: ClientUser,
}

/// Delete a user
/// Delete/remove your account
#[utoipa::path(
    delete,
    path = "/users/",
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    request_body=DeleteUserBody,
    responses((status = 200, response = DeleteUserResponse)),
)]
async fn delete_user(
    body: web::Json<DeleteUserBody>,
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let body: DeleteUserBody = body.into_inner();
    body.validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    /* Check credentials */
    let user: DbUser = app_state
        .dbclient
        .get_user(user_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::unauthorized(ErrorMessage::WrongCredentials))
        .and_then(|user: DbUser| {
            let is_email_correct: bool = &user.email == &body.email;
            let is_password_correct: bool = password_utils::compare(&body.password, &user.password);
            match is_email_correct && is_password_correct {
                true => Ok(user),
                false => Err(HttpError::unauthorized(ErrorMessage::WrongCredentials)),
            }
        })?;
    let user: ClientUser = user
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    let all_my_friends_user_ids: Vec<usize> = app_state
        .dbclient
        .get_friends_as_user(user_id, Some(&mut transaction))
        .await
        .into_iter()
        .map(|u| u.id as usize)
        .collect();

    let posts: Vec<DbPost> = app_state
        .dbclient
        .get_posts(user_id, Some(&mut transaction))
        .await;
    /* Delete user specific data */
    app_state
        .dbclient
        .delete_refresh_tokens(user_id, Some(&mut transaction))
        .await?;
    app_state
        .dbclient
        .delete_friends(user_id, Some(&mut transaction))
        .await?;
    app_state
        .dbclient
        .delete_friendrequests(user_id, Some(&mut transaction))
        .await?;
    app_state
        .dbclient
        .unblock_users(user_id, Some(&mut transaction))
        .await?;
    app_state
        .dbclient
        .delete_messages(user_id, Some(&mut transaction))
        .await?;
    app_state
        .dbclient
        .delete_posts(user_id, Some(&mut transaction))
        .await?;

    app_state
        .dbclient
        .delete_user(user_id, Some(&mut transaction))
        .await?;

    storage::delete_post_pics(posts).await?;
    storage::delete_avatar(user_id).await?;

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::DeleteUser,
                payload: user.to_payload()?,
                message: "You've deleted your account".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::DeleteUser,
                payload: user.to_payload()?,
                message: "A friend of yours has deleted his account".into(),
            },
            &all_my_friends_user_ids,
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(DeleteUserResponse {
        message: SuccessMessage::DeletedUser.into(),
        result: user,
    }))
}

#[derive(Serialize, Deserialize, Validate, Debug, ToSchema, Default)]
pub struct UpdateUserBody {
    #[validate(
        length(
            min = 4,
            max = 20,
            message = "Username has to be 4 to 20 characters long"
        ),
        does_not_contain(pattern = " ", message = "Username should not contain any spaces")
    )]
    username: Option<String>,
    #[validate(
        length(
            min = 4,
            max = 20,
            message = "Display username has to be 4 to 20 characters long"
        ),
        does_not_contain(
            pattern = " ",
            message = "Display username should not contain any spaces"
        )
    )]
    display_username: Option<String>,
    #[validate(email(message = "Has to be email"))]
    email: Option<String>,
    #[validate(length(
        min = 8,
        max = 2000,
        message = "The new password has to be at least 8 characters long"
    ))]
    password: Option<String>,
    old_password: Option<String>,
    #[schema(example = "away")]
    status: Option<String>,
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully updated your account")]
pub struct UpdateUserResponse {
    pub message: String,
    pub result: ClientUser,
}

/// Update a user
/// Update your account's username, passwort and/or password
#[utoipa::path(
    patch,
    path = "/users/",
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    request_body=UpdateUserBody,
    responses((status = 200, response = UpdateUserResponse)),
)]
async fn update_user(
    body: web::Json<UpdateUserBody>,
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let body: UpdateUserBody = body.into_inner();
    let user_id: usize = auth_token.id;
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    let user: DbUser = app_state
        .dbclient
        .get_user(user_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::unauthorized(ErrorMessage::Unauthorized))?;

    body.validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    /* Check password */
    let is_password_correct: bool =
        password_utils::compare(body.old_password.unwrap_or(String::new()), &user.password);

    /* Update username */
    if let Some(new_username) = &body.username {
        if !is_password_correct {
            return Err(HttpError::wrong_credentials());
        }
        let is_username_same: bool = new_username == &user.username;
        if is_username_same {
            return Err(HttpError::bad_request(ErrorMessage::NewUsernameIsTheSame));
        }
        app_state
            .dbclient
            .update_user(
                user_id,
                Some(new_username),
                None,
                None,
                None,
                None,
                Some(&mut transaction),
            )
            .await?;
    }
    /* Update display_username */
    if let Some(new_display_username) = &body.display_username {
        app_state
            .dbclient
            .update_user(
                user_id,
                None,
                Some(new_display_username),
                None,
                None,
                None,
                Some(&mut transaction),
            )
            .await?;
    }
    /* Update email */
    if let Some(new_email) = &body.email {
        if !is_password_correct {
            return Err(HttpError::wrong_credentials());
        }
        let is_new_email_same: bool = new_email == &user.email;
        if is_new_email_same {
            return Err(HttpError::bad_request(ErrorMessage::NewEmailIsTheSame));
        }
        app_state
            .dbclient
            .update_user(
                user_id,
                None,
                None,
                None,
                Some(new_email),
                None,
                Some(&mut transaction),
            )
            .await?;
    }
    /* Update password */
    if let Some(new_password) = &body.password {
        if !is_password_correct {
            return Err(HttpError::wrong_credentials());
        }
        app_state
            .dbclient
            .update_user(
                user_id,
                None,
                None,
                Some(new_password),
                None,
                None,
                Some(&mut transaction),
            )
            .await?;
    }
    /* Update online status */
    if let Some(new_status) = &body.status {
        app_state
            .dbclient
            .update_user(
                user_id,
                None,
                None,
                None,
                None,
                Some(Status::new(new_status)),
                Some(&mut transaction),
            )
            .await?;
    }

    let updated_user: ClientUser = app_state
        .dbclient
        .get_user(user_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::server_error())?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    let all_my_friends_user_ids: Vec<usize> = app_state
        .dbclient
        .get_friends_as_user(user_id, Some(&mut transaction))
        .await
        .into_iter()
        .map(|u| u.id as usize)
        .collect();

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::UpdateUser,
                payload: updated_user.to_payload()?,
                message: "You've updated your user's informations".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::UpdateUser,
                payload: updated_user.to_payload()?,
                message: "A friend of yours updated his user's informations".into(),
            },
            &all_my_friends_user_ids,
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(UpdateUserResponse {
        message: SuccessMessage::UpdatedUser.into(),
        result: updated_user,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully got yourself")]
pub struct GetMeResponse {
    pub message: String,
    pub result: ClientUser,
}

/// Get me
/// Get the user by the authentication token
#[utoipa::path(
    get,
    path = "/users/me/",
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = GetMeResponse)),
)]
async fn get_me(
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;

    let user: ClientUser = app_state
        .dbclient
        .get_user(user_id, None)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Ok().json(GetMeResponse {
        message: SuccessMessage::GotUser.into(),
        result: user,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Got a user")]
pub struct GetUserResponse {
    pub message: String,
    pub result: ClientUser,
}

/// Get a specific user
#[utoipa::path(
    get,
    path = "/users/{user_id}/",
    params(("user_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = GetUserResponse)),
)]
async fn get_user(
    path: web::Path<usize>,
    _auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = path.into_inner();
    let user: ClientUser = app_state
        .dbclient
        .get_user(user_id, None)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;
    Ok(HttpResponse::Ok().json(GetUserResponse {
        message: SuccessMessage::GotUser.into(),
        result: user,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Your profile picture has been updated")]
pub struct UpdateAvatarResponse {
    pub message: String,
    pub result: ClientUser,
}

#[derive(MultipartForm, ToSchema)]
#[multipart(deny_unknown_fields)]
#[multipart(duplicate_field = "deny")]
pub struct AvatarUpload {
    #[schema(value_type = String, format = Binary)]
    #[multipart(limit = "1 MiB")]
    pub file: TempFile,
}

/// Update your profile's avatar picture
#[utoipa::path(
    post,
    path = "/users/{user_id}/avatar/",
    params(("user_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    request_body(content = AvatarUpload, content_type = "multipart/form-data"),
    responses((status = 200, response = UpdateAvatarResponse)),
)]
async fn update_avatar(
    path: web::Path<usize>,
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
    MultipartForm(form): MultipartForm<AvatarUpload>,
) -> Result<HttpResponse, HttpError> {
    let path_user_id: usize = path.into_inner();
    let user_id: usize = auth_token.id;

    if user_id != path_user_id {
        return Err(HttpError::bad_request("TODO"));
    }

    upload::save_avatar(form, user_id).await?;

    let user: ClientUser = app_state
        .dbclient
        .get_user(user_id, None)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    let all_my_friends_user_ids: Vec<usize> = app_state
        .dbclient
        .get_friends_as_user(user_id, None)
        .await
        .into_iter()
        .map(|u| u.id as usize)
        .collect();

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::UpdateUserAvatar,
                payload: user.to_payload()?,
                message: "You've updated your avatar picture".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::UpdateUserAvatar,
                payload: user.to_payload()?,
                message: "A friend of yours updated his avatar picture".into(),
            },
            &all_my_friends_user_ids,
        ),
    ]);

    Ok(HttpResponse::Ok().json(UpdateAvatarResponse {
        message: SuccessMessage::ChangedAvatar.into(),
        result: user,
    }))
}

/// Get profile's avatar pricture of a user
#[utoipa::path(
    get,
    path = "/users/{user_id}/avatar/",
    params(("user_id" = u64, Path,)),
    responses((status = 200, description = "Got avatar", content(("image/jpeg" = String))))
)]
async fn get_avatar(path: web::Path<usize>) -> HttpResponse {
    let user_id: usize = path.into_inner();
    let user_img_path = AVATAR_DIRECTORY.as_path().join(format!("{}.jpeg", user_id));

    let img: Vec<u8> = fs::read(user_img_path)
        .await
        .unwrap_or(include_bytes!("../../assets/default-avatar.jpeg").to_vec());

    HttpResponse::Ok().content_type("image/jpeg").body(img)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::POSTS_DIRECTORY;
    use crate::app::VERSION;
    use crate::dbclient::DbClient;
    use crate::models::{
        DbBlockedUser, DbFriend, DbFriendrequest, DbMessage, DbPost, DbPostComment, DbPostLike,
    };
    use crate::server::Server;
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils;
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, AppState};
    use actix_web::{body::MessageBody, http, http::header::ContentType, test, web, App};
    use futures::{SinkExt, StreamExt};
    use mime;
    use pretty_assertions::assert_eq;
    use reqwest::Method;
    use serde_json;
    use serde_json::json;
    use sqlx::mysql::MySqlPool;
    use tokio::time::{sleep, Duration};
    use tokio_tungstenite::tungstenite::Message;

    #[sqlx::test]
    async fn test_get_users(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_users_response(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user(1, None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![
                user.into_client(config.domain(), websocket_server_handle.clone())
                    .await,
            ],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(69, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_next_url(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("amarc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("bmarc", "amarc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("cmarc", "bmarc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user(1, None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 3,
            next: Some("http://localhost:4000/v1/users/?page=2&page_size=1&query=marc".to_string()),
            previous: None,
            result: vec![
                user.into_client(config.domain(), websocket_server_handle.clone())
                    .await,
            ],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc&page_size=1&page=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(69, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_previous_url(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("amarc", "amarc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("bmarc", "bmarc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user(2, None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some(
                "http://localhost:4000/v1/users/?page=1&page_size=1&query=marc".to_string(),
            ),
            result: vec![
                user.into_client(config.domain(), websocket_server_handle.clone())
                    .await,
            ],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc&page_size=1&page=2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(69, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_but_without_query(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let expected: &str = "{\"count\":0,\"next\":null,\"previous\":null,\"result\":[]}";
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_but_page_size_is_too_big(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc&page_size=99999")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_users_but_page_is_zero(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc&page=0")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_users_filters_me(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 0,
            next: None,
            previous: None,
            result: Vec::new(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc&filter_me=true")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_filters_friends(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 0,
            next: None,
            previous: None,
            result: Vec::new(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=leon&filter_friends=true")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_filters_pending_friendrequests(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friendrequest(1, 2, None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 0,
            next: None,
            previous: None,
            result: Vec::new(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=leon&filter_pending_friendrequests=true")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_filters_blocked_users(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.block_user(1, 2, None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 0,
            next: None,
            previous: None,
            result: Vec::new(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=leon&filter_blocked_users=true")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_is_sorted_by_username(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("cmarc", "cmarc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("amarc", "amarc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        let users: Vec<ClientUser> = vec![
            dbclient
                .get_user(2, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
            dbclient
                .get_user(1, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
        ];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: users,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(69, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc")
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_register_user(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "marc".to_string(),
                email: Some("marc@mail.de".to_string()),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::CREATED);
    }

    #[sqlx::test]
    async fn test_register_user_response(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle: websocket_server_handle.clone(),
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "marc".to_string(),
                email: Some("marc@mail.de".to_string()),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;

        let user: ClientUser = dbclient
            .get_user(1, None)
            .await
            .unwrap()
            .into_client(config.domain(), websocket_server_handle)
            .await;
        let expected: String = serde_json::to_string(&RegisterUserResponse {
            message: SuccessMessage::Register.into(),
            result: ClientUser {
                id: 1,
                username: "marc".to_string(),
                display_username: "marc".to_string(),
                register_date: user.register_date,
                status: user.status,
                avatar: user.avatar,
            },
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_register_user_but_username_contains_spaces(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: " ma rc ".to_string(),
                email: Some("marc@gmail.com".to_string()),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_register_user_but_username_is_taken(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "marc".to_string(),
                email: Some("marc@gmail.com".to_string()),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_register_user_but_email_is_taken(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "warc".to_string(),
                email: Some("marc@mail.de".to_string()),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_register_user_but_username_is_too_short(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "ma".to_string(),
                email: Some("marc@mail.de".to_string()),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_register_user_but_email_is_invalid(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "marc".to_string(),
                email: Some("mail.de".to_string()),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_register_user_but_password_is_too_short(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "marc".to_string(),
                email: Some("marc@mail.de".to_string()),
                password: "1234".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_register_user_can_register_a_user_without_a_email(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "marc".to_string(),
                email: None,
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::CREATED);
        let user = dbclient.get_user(1, None).await.unwrap();
        assert_eq!(user.email, "");
    }

    #[sqlx::test]
    async fn test_get_user(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        let expected: String = serde_json::to_string(&GetUserResponse {
            message: SuccessMessage::GotUser.into(),
            result: dbclient
                .get_user(1, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_user)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_user_response(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle: websocket_server_handle.clone(),
                }))
                .route("/{user_id}", web::get().to(get_user)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let user: DbUser = dbclient.get_user(1, None).await.unwrap();
        let expected: String = serde_json::to_string(&GetUserResponse {
            message: SuccessMessage::GotUser.into(),
            result: user
                .into_client(config.domain(), websocket_server_handle)
                .await,
        })
        .unwrap();
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_user)),
        )
        .await;

        let req = test::TestRequest::default().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_user(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_delete_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_user_but_without_body(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_user_but_with_wrong_credentials(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "asdf1234".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_refresh_tokens(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_refresh_token(1, "shhh!", None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let refresh_tokens: Vec<String> = dbclient.get_refresh_tokens(1, None).await;
        assert_eq!(refresh_tokens.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_friends(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let friends: Vec<DbFriend> = dbclient.get_friends(1, None).await;
        assert_eq!(friends.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_friendrequests(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("sven", "sven@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friendrequest(1, 2, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let friendrequests: Vec<DbFriendrequest> = dbclient.get_friendrequests(1, None).await;
        assert_eq!(friendrequests.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_blocked_users(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.block_user(1, 2, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let blocked_users: Vec<DbBlockedUser> = dbclient.get_blocked_users(1, None).await;
        assert_eq!(blocked_users.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_messages(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_message(1, 2, "hi", None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let messages: Vec<DbMessage> = dbclient.get_messages(1, None).await;
        assert_eq!(messages.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_posts_with_likes_and_comments(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_post(1, "hiii", 0, None).await.unwrap();
        dbclient.like_post(1, 1, None).await.unwrap();
        dbclient
            .save_post_comment(1, 1, "hahah", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let posts: Vec<DbPost> = dbclient.get_posts(1, None).await;
        let post_comments: Vec<DbPostComment> = dbclient.get_post_comments(1, None).await;
        let post_likes: Vec<DbPostLike> = dbclient.get_post_likes(1, None).await;
        assert_eq!(posts.len(), 0);
        assert_eq!(post_comments.len(), 0);
        assert_eq!(post_likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_all_post_pictures(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        const BLACK_PIXEL_PNG: [u8; 72] = [
            137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 1, 0, 0, 0, 1,
            8, 2, 0, 0, 0, 144, 119, 83, 222, 0, 0, 0, 15, 73, 68, 65, 84, 8, 29, 1, 4, 0, 251,
            255, 0, 0, 0, 0, 0, 4, 0, 1, 47, 82, 180, 141, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96,
            130,
        ];

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_post(1, "hii", 1, None).await.unwrap();

        image::load_from_memory(&BLACK_PIXEL_PNG)
            .unwrap()
            .save(POSTS_DIRECTORY.as_path().join("1_0.jpeg"))
            .unwrap();

        assert!(POSTS_DIRECTORY.as_path().join("1_0.jpeg").exists());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        assert_eq!(POSTS_DIRECTORY.as_path().join("1_0.jpeg").exists(), false);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_avatar(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        const BLACK_PIXEL_PNG: [u8; 72] = [
            137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 1, 0, 0, 0, 1,
            8, 2, 0, 0, 0, 144, 119, 83, 222, 0, 0, 0, 15, 73, 68, 65, 84, 8, 29, 1, 4, 0, 251,
            255, 0, 0, 0, 0, 0, 4, 0, 1, 47, 82, 180, 141, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96,
            130,
        ];

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        image::load_from_memory(&BLACK_PIXEL_PNG)
            .unwrap()
            .save(AVATAR_DIRECTORY.as_path().join("1.jpeg"))
            .unwrap();

        assert!(AVATAR_DIRECTORY.as_path().join("1.jpeg").exists());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        assert_eq!(AVATAR_DIRECTORY.as_path().join("1.jpeg").exists(), false);
    }

    #[sqlx::test]
    async fn test_delete_user_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: DeleteUserResponse = test_utils::do_http_request(
            1,
            Method::DELETE,
            format!("http://127.0.0.1:{port}/{VERSION}/users/"),
            Some(json!({
                "email": "marc@langunion.com",
                "password": "asdfasdf",
            })),
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::DeleteUser {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientUser =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::DeleteUser {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientUser =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
    }

    #[sqlx::test]
    async fn test_update_user(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                username: Some("leon".to_string()),
                display_username: Some("leon_the_king".to_string()),
                email: Some("leon@mail.de".to_string()),
                password: Some("asdf1234".to_string()),
                old_password: Some("1234asdf".to_string()),
                status: Some("dnd".to_string()),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        let updated_user: DbUser = dbclient_clone.get_user(1, None).await.unwrap();

        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(updated_user.username, "leon");
        assert_eq!(updated_user.display_username, "leon_the_king");
        assert_eq!(updated_user.email, "leon@mail.de");
        assert_eq!(
            password_utils::compare("1234asdf", &updated_user.password),
            false
        );
        assert_eq!(updated_user.status, "dnd");
    }

    #[sqlx::test]
    async fn test_update_user_response(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        let pre_user: DbUser = dbclient.get_user(1, None).await.unwrap();
        let post_user: ClientUser = ClientUser {
            id: 1,
            username: "leon".to_string(),
            display_username: "leon_the_king".to_string(),
            register_date: pre_user.register_date,
            status: "off".to_string(),
            avatar: "http://localhost:4000/v1/users/1/avatar/".to_string(),
        };
        let expected: String = serde_json::to_string(&UpdateUserResponse {
            message: SuccessMessage::UpdatedUser.into(),
            result: post_user,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                username: Some("leon".to_string()),
                display_username: Some("leon_the_king".to_string()),
                old_password: Some("1234asdf".to_string()),
                status: Some("away".to_string()),
                ..Default::default()
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_update_user_email_but_email_is_taken(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                email: Some("leon@mail.de".to_string()),
                old_password: Some("1234asdf".to_string()),
                ..Default::default()
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_user_email_but_new_is_same_as_old(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                email: Some("marc@mail.de".to_string()),
                old_password: Some("1234asdf".to_string()),
                ..Default::default()
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_user_with_wrong_password(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                password: Some("asdf1234".to_string()),
                old_password: Some("12345asdfg".to_string()),
                ..Default::default()
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                old_password: Some("1234asdf".to_string()),
                ..Default::default()
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_update_user_status_to_off(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .set_json(UpdateUserBody {
                status: Some(Status::Off.to_string()),
                ..Default::default()
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let new_status: String = dbclient.get_user(1, None).await.unwrap().status.to_string();
        assert_eq!(new_status, Status::Off.to_string());
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_user_status_to_on(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .set_json(UpdateUserBody {
                status: Some(Status::On.to_string()),
                ..Default::default()
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let new_status: String = dbclient_clone
            .get_user(1, None)
            .await
            .unwrap()
            .status
            .to_string();
        assert_eq!(new_status, Status::On.to_string());
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_user_status_to_away(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .set_json(UpdateUserBody {
                status: Some(Status::Away.to_string()),
                ..Default::default()
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let new_status: String = dbclient.get_user(1, None).await.unwrap().status.to_string();
        assert_eq!(new_status, Status::Away.to_string());
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_user_status_to_dnd(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .set_json(UpdateUserBody {
                status: Some(Status::Dnd.to_string()),
                ..Default::default()
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let new_status: String = dbclient.get_user(1, None).await.unwrap().status.to_string();
        assert_eq!(new_status, Status::Dnd.to_string());
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_user_status_to_unknown(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .set_json(UpdateUserBody {
                status: Some("unknown".to_string()),
                ..Default::default()
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let new_status: String = dbclient.get_user(1, None).await.unwrap().status.to_string();
        assert_eq!(new_status, Status::On.to_string());
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_user_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: UpdateUserResponse = test_utils::do_http_request(
            1,
            Method::PATCH,
            format!("http://127.0.0.1:{port}/{VERSION}/users/"),
            Some(json!({
                "username": "marcel",
                "old_password": "asdfasdf",
            })),
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UpdateUser {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientUser =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UpdateUser {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientUser =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
    }

    #[sqlx::test]
    async fn test_get_me(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_me)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_me_response(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        let me: ClientUser = dbclient
            .get_user(1, None)
            .await
            .unwrap()
            .into_client(config.domain(), websocket_server_handle.clone())
            .await;

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_me)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp: Vec<u8> = test::call_service(&app, req)
            .await
            .into_body()
            .try_into_bytes()
            .unwrap()
            .to_vec();
        let body: GetMeResponse =
            serde_json::from_str(std::str::from_utf8(&resp).unwrap()).unwrap();

        assert_eq!(me.id, body.result.id);
        assert_eq!(me.username, body.result.username);
    }

    #[sqlx::test]
    async fn test_get_me_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_me)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_update_avatar(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "asdfasdf", None)
            .await
            .unwrap();

        let mut payload: Vec<u8> = Vec::new();
        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"file\"; filename=\"avatar.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n".into_iter().for_each(|b: &u8| payload.push(*b));

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::post().to(update_avatar)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert!(AVATAR_DIRECTORY.as_path().join("1.jpeg").exists());
    }

    #[sqlx::test]
    async fn test_update_avatar_but_without_picture(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::post().to(update_avatar)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .insert_header(ContentType(mime::MULTIPART_FORM_DATA))
            .append_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_avatar_but_with_garbage(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut payload: Vec<u8> = Vec::new();
        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"file\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          some_garbage\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n"
            .into_iter()
            .for_each(|b: &u8| payload.push(*b));

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::post().to(update_avatar)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
        assert_eq!(AVATAR_DIRECTORY.as_path().join("1.jpeg").exists(), false);
    }

    #[sqlx::test]
    async fn test_update_avatar_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::post().to(update_avatar)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .insert_header(ContentType(mime::MULTIPART_FORM_DATA))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_update_avatar_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: UpdateUserResponse = test_utils::do_multipart_request(
            1,
            Method::POST,
            format!("http://127.0.0.1:{port}/{VERSION}/users/1/avatar"),
            reqwest::multipart::Form::new().part(
                "file",
                reqwest::multipart::Part::bytes(include_bytes!("../../assets/default-avatar.jpeg"))
                    .file_name("pixel.jpeg")
                    .mime_str("image/jpeg")
                    .unwrap(),
            ),
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UpdateUserAvatar {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientUser =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UpdateUserAvatar {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientUser =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
    }

    #[sqlx::test]
    async fn test_get_avatar(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_avatar)),
        )
        .await;

        let req = test::TestRequest::default().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }
}
