use crate::app::POSTS_DIRECTORY;
use crate::dbclient::DbClient;
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{ClientPost, DbPost, DbPostComment, ToPayload};
use crate::openapi::PaginatedResponseClientPost;
use crate::server::AppState;
use crate::success::SuccessMessage;
use crate::utils::paginate::Paginator;
use crate::utils::{posts_utils, storage, upload};
use crate::ws::events::{SocketAction, SocketEvent};
use actix_multipart::form::{tempfile::TempFile, text::Text, MultipartForm};
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use tokio::fs;
use utoipa::{IntoParams, ToResponse, ToSchema};
use validator::Validate;

pub fn posts_scope() -> Scope {
    web::scope("/posts")
        .route("/", web::get().to(get_posts))
        .route("/", web::post().to(save_post))
        .route("/{post_id}/", web::get().to(get_post))
        .route("/{post_id}/", web::delete().to(delete_post))
        .route("/{post_id}/like/", web::patch().to(like_post))
        .route("/{post_id}/unlike/", web::patch().to(unlike_post))
        .route("/{post_id}/comments/", web::post().to(post_comment))
        .route(
            "/{post_id}/comments/{comment_id}/",
            web::delete().to(delete_comment),
        )
        .route(
            "/{post_id}/pictures/{post_pic_name}/",
            web::get().to(get_post_pic),
        )
}

#[derive(Serialize, Deserialize, Validate, IntoParams)]
struct GetPostsQuery {
    #[validate(range(min = 1, message = "'page' param has to be at least 1"))]
    page: Option<usize>,
    #[validate(range(
        min = 1,
        max = 50,
        message = "'page_size' param has to be in the range of 1 and 50"
    ))]
    page_size: Option<usize>,
}

/// Get a list of your and your friends posts
#[utoipa::path(
    get,
    path = "/posts/",
    params(GetPostsQuery),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = PaginatedResponseClientPost)),
)]
async fn get_posts(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    query: web::Query<GetPostsQuery>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let query: GetPostsQuery = query.into_inner();

    query
        .validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let posts: Vec<DbPost> = app_state.dbclient.get_posts(user_id, None).await;
    let mut client_posts: Vec<ClientPost> = Vec::new();

    for post in posts {
        let post = post
            .into_client((&app_state.dbclient, None), app_state.config.domain())
            .await;
        client_posts.push(post);
    }

    Ok(HttpResponse::Ok().json(
        Paginator::new()
            .page(query.page)
            .page_size(query.page_size)
            .url(app_state.config.domain())
            .pathname("/posts")
            .data(client_posts)
            .into_paginated_response(),
    ))
}

#[derive(MultipartForm, ToSchema)]
#[multipart(deny_unknown_fields)]
#[multipart(duplicate_field = "deny")]
pub struct PostUpload {
    #[multipart(rename = "files[]")]
    #[schema(value_type = Option<String>, format = Binary, rename="files[]")]
    pub files: Vec<TempFile>,
    #[schema(value_type = Option<String>)]
    content: Option<Text<String>>,
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Posted a new post")]
pub struct SavePostResponse {
    pub message: String,
    pub result: ClientPost,
}

/// Create a new post
#[utoipa::path(
    post,
    path = "/posts/",
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    request_body(content = PostUpload, content_type = "multipart/form-data"),
    responses((status = 200, response = SavePostResponse)),
)]
async fn save_post(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    MultipartForm(form): MultipartForm<PostUpload>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let content: String = match form.content.as_ref() {
        Some(text) => text.to_string(),
        None => String::new(),
    };

    if content.is_empty() && form.files.len() <= 0 {
        return Err(HttpError::bad_request(ErrorMessage::EmptyPost));
    }

    let mut transaction = app_state.dbclient.begin_transaction().await?;

    let post_id: usize = app_state
        .dbclient
        .save_post(user_id, &content, form.files.len(), Some(&mut transaction))
        .await?;
    let post: ClientPost = app_state
        .dbclient
        .get_post(post_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::server_error())?
        .into_client(
            (&app_state.dbclient, Some(&mut transaction)),
            app_state.config.domain(),
        )
        .await;

    upload::save_posts(form, post.id).await?;

    let all_my_friends_user_ids: Vec<usize> = app_state
        .dbclient
        .get_friends_as_user(user_id, Some(&mut transaction))
        .await
        .into_iter()
        .map(|u| u.id as usize)
        .collect();

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::CreatePost,
                payload: post.to_payload()?,
                message: "You've created new post".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::CreatePost,
                payload: post.to_payload()?,
                message: "A friend of yours created a new post".into(),
            },
            &all_my_friends_user_ids,
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(SavePostResponse {
        message: SuccessMessage::SavedPost.into(),
        result: post,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully got the post")]
pub struct GetPostResponse {
    pub message: String,
    pub result: ClientPost,
}

/// Get a specific post
/// Get one of your or your friends post by the post id
#[utoipa::path(
    get,
    path = "/posts/{post_id}/",
    params(("post_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = GetPostResponse)),
)]
async fn get_post(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let post_id: usize = path.into_inner();
    let post: ClientPost = app_state
        .dbclient
        .get_post(post_id, None)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?
        .into_client((&app_state.dbclient, None), app_state.config.domain())
        .await;

    if !posts_utils::is_my_or_friends_post(user_id, post_id, &app_state.dbclient).await {
        return Err(HttpError::bad_request(ErrorMessage::CannotViewThisPost));
    }

    Ok(HttpResponse::Ok().json(GetPostResponse {
        message: SuccessMessage::GotPost.into(),
        result: post,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Deleted your comment to a post")]
pub struct DeletePostResponse {
    pub message: String,
    pub result: ClientPost,
}

/// Delete one of your posts
#[utoipa::path(
    delete,
    path = "/posts/{post_id}/",
    params(("post_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = DeletePostResponse)),
)]
async fn delete_post(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let post_id: usize = path.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    let db_post: DbPost = app_state
        .dbclient
        .get_post(post_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))
        .and_then(|post| match post.author as usize == user_id {
            true => Ok(post),
            false => Err(HttpError::bad_request(ErrorMessage::NotYourPost)),
        })?;
    let deleted_post: ClientPost = db_post
        .clone()
        .into_client(
            (&app_state.dbclient, Some(&mut transaction)),
            app_state.config.domain(),
        )
        .await;

    app_state
        .dbclient
        .delete_post(post_id, Some(&mut transaction))
        .await?;
    storage::delete_post_pics(vec![db_post]).await?;

    let all_my_friends_user_ids: Vec<usize> = app_state
        .dbclient
        .get_friends_as_user(user_id, Some(&mut transaction))
        .await
        .into_iter()
        .map(|u| u.id as usize)
        .collect();

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::RemovePost,
                payload: deleted_post.to_payload()?,
                message: "You've deleted a post".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::RemovePost,
                payload: deleted_post.to_payload()?,
                message: "A friend of yours deleted a post".into(),
            },
            &all_my_friends_user_ids,
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(DeletePostResponse {
        message: SuccessMessage::DeletedPost.into(),
        result: deleted_post,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Liked the post")]
pub struct LikePostResponse {
    pub message: String,
    pub result: ClientPost,
}

/// Like a post
#[utoipa::path(
    patch,
    path = "/posts/{post_id}/like/",
    params(("post_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = LikePostResponse)),
)]
async fn like_post(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let post_id: usize = path.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    if !posts_utils::is_my_or_friends_post(user_id, post_id, &app_state.dbclient).await {
        return Err(HttpError::bad_request(ErrorMessage::CannotLikeThisPost));
    }

    app_state
        .dbclient
        .like_post(post_id, user_id, Some(&mut transaction))
        .await?;

    let db_post: DbPost = app_state
        .dbclient
        .get_post(post_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?;
    let post: ClientPost = db_post
        .clone()
        .into_client(
            (&app_state.dbclient, Some(&mut transaction)),
            app_state.config.domain(),
        )
        .await;

    let all_friends_of_the_posts_author: Vec<usize> = {
        let mut u: Vec<usize> = app_state
            .dbclient
            .get_friends_as_user(db_post.author as usize, Some(&mut transaction))
            .await
            .into_iter()
            .map(|u| u.id as usize)
            .collect();
        u.push(db_post.author as usize); // add the author
        u.retain(|id| *id != user_id as usize); // remove me (i could be also the author)
        u
    };

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::LikePost,
                payload: post.to_payload()?,
                message: "You've liked a post".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::LikePost,
                payload: post.to_payload()?,
                message: "A friend of yours liked a posts".into(),
            },
            &all_friends_of_the_posts_author,
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(LikePostResponse {
        message: SuccessMessage::LikedPost.into(),
        result: post,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Unliked the post")]
pub struct UnlikePostResponse {
    pub message: String,
    pub result: ClientPost,
}

/// Unlike a post
#[utoipa::path(
    patch,
    path = "/posts/{post_id}/unlike/",
    params(("post_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = UnlikePostResponse)),
)]
async fn unlike_post(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let post_id: usize = path.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    if !posts_utils::is_my_or_friends_post(user_id, post_id, &app_state.dbclient).await {
        return Err(HttpError::bad_request(ErrorMessage::CannotUnlikeThisPost));
    }

    app_state
        .dbclient
        .unlike_post(post_id, user_id, Some(&mut transaction))
        .await?;

    let db_post: DbPost = app_state
        .dbclient
        .get_post(post_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?;
    let post: ClientPost = db_post
        .clone()
        .into_client(
            (&app_state.dbclient, Some(&mut transaction)),
            app_state.config.domain(),
        )
        .await;

    let all_friends_of_the_posts_author: Vec<usize> = {
        let mut u: Vec<usize> = app_state
            .dbclient
            .get_friends_as_user(db_post.author as usize, Some(&mut transaction))
            .await
            .into_iter()
            .map(|u| u.id as usize)
            .collect();
        u.push(db_post.author as usize); // add the author
        u.retain(|id| *id != user_id as usize); // remove me (i could be also the author)
        u
    };

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::UnlikePost,
                payload: post.to_payload()?,
                message: "You've unliked a post".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::UnlikePost,
                payload: post.to_payload()?,
                message: "A friend of yours unliked a posts".into(),
            },
            &all_friends_of_the_posts_author,
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(UnlikePostResponse {
        message: SuccessMessage::UnlikedPost.into(),
        result: post,
    }))
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct PostCommentBody {
    content: String,
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Posted a new comment to a post")]
pub struct PostCommentResponse {
    pub message: String,
    pub result: ClientPost,
}

/// Comment a post
#[utoipa::path(
    post,
    path = "/posts/{post_id}/comments/",
    params(("post_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    request_body=PostCommentBody,
    responses((status = 200, response = PostCommentResponse)),
)]
async fn post_comment(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
    body: web::Json<PostCommentBody>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let post_id: usize = path.into_inner();
    let body: PostCommentBody = body.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    /* Check if user is allowed to post comment */
    if !posts_utils::is_my_or_friends_post(user_id, post_id, &app_state.dbclient).await {
        return Err(HttpError::bad_request(
            ErrorMessage::CannotCommentOnThisPost,
        ));
    }

    app_state
        .dbclient
        .save_post_comment(post_id, user_id, body.content, Some(&mut transaction))
        .await?;

    let db_post: DbPost = app_state
        .dbclient
        .get_post(post_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::server_error())?;
    let post: ClientPost = db_post
        .clone()
        .into_client(
            (&app_state.dbclient, Some(&mut transaction)),
            app_state.config.domain(),
        )
        .await;

    let all_friends_of_the_posts_author: Vec<usize> = {
        let mut u: Vec<usize> = app_state
            .dbclient
            .get_friends_as_user(db_post.author as usize, Some(&mut transaction))
            .await
            .into_iter()
            .map(|u| u.id as usize)
            .collect();
        u.push(db_post.author as usize); // add the author
        u.retain(|id| *id != user_id as usize); // remove me (i could be also the author)
        u
    };

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::CommentPost,
                payload: post.to_payload()?,
                message: "You've commented a post".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::CommentPost,
                payload: post.to_payload()?,
                message: "A friend commented a post".into(),
            },
            &all_friends_of_the_posts_author,
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(PostCommentResponse {
        message: SuccessMessage::CommentPost.into(),
        result: post,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Deleted your comment to a post")]
pub struct DeleteCommentResponse {
    pub message: String,
    pub result: ClientPost,
}

/// Delete your comment of a post
#[utoipa::path(
    delete,
    path = "/posts/{post_id}/comments/{comment_id}/",
    params(("post_id" = u64, Path,), ("comment_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = DeleteCommentResponse)),
)]
async fn delete_comment(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<(usize, usize)>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let (comment_id, post_id): (usize, usize) = path.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    app_state
        .dbclient
        .get_post_comment(comment_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoPostCommentFound))
        .and_then(|comment: DbPostComment| {
            let am_i_author: bool = comment.author as usize == user_id;
            let does_post_id_match: bool = comment.post_id as usize == post_id;
            match am_i_author && does_post_id_match {
                true => Ok(()),
                false => Err(HttpError::bad_request(
                    ErrorMessage::CannotDeletePostComment,
                )),
            }
        })?;

    app_state
        .dbclient
        .delete_post_comment(comment_id, Some(&mut transaction))
        .await?;

    let db_post: DbPost = app_state
        .dbclient
        .get_post(post_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::server_error())?;
    let post: ClientPost = db_post
        .clone()
        .into_client(
            (&app_state.dbclient, Some(&mut transaction)),
            app_state.config.domain(),
        )
        .await;

    let all_friends_of_the_posts_author: Vec<usize> = {
        let mut u: Vec<usize> = app_state
            .dbclient
            .get_friends_as_user(db_post.author as usize, Some(&mut transaction))
            .await
            .into_iter()
            .map(|u| u.id as usize)
            .collect();
        u.push(db_post.author as usize); // add the author
        u.retain(|id| *id != user_id as usize); // remove me (i could be also the author)
        u
    };

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::RemoveCommentPost,
                payload: post.to_payload()?,
                message: "You've removed a comment of a post".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::RemoveCommentPost,
                payload: post.to_payload()?,
                message: "A friend of yours removed a comment of a posts".into(),
            },
            &all_friends_of_the_posts_author,
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(DeleteCommentResponse {
        message: SuccessMessage::DeletedPostComment.into(),
        result: post,
    }))
}

/// Get a picture of a post
#[utoipa::path(
    get,
    path = "/posts/{post_id}/pictures/{post_pic_name}/",
    params(("post_id" = u64, Path,), ("post_pic_name" = &str, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, description = "Got post pic", content(("image/jpeg" = String))))
)]
async fn get_post_pic(
    app_state: web::Data<AppState>,
    path: web::Path<(usize, String)>,
    auth_token: AuthenticationToken,
) -> Result<HttpResponse, HttpError> {
    let (post_id, post_pic_name): (usize, String) = path.into_inner();
    let user_id: usize = auth_token.id;

    let post_pic_path = POSTS_DIRECTORY
        .as_path()
        .join(format!("{}.jpeg", post_pic_name));

    if !posts_utils::is_my_or_friends_post(user_id, post_id, &app_state.dbclient).await {
        return Err(HttpError::bad_request(ErrorMessage::CannotViewThisPost));
    }

    let pic: Vec<u8> = fs::read(post_pic_path)
        .await
        .map_err(|_| HttpError::bad_request(ErrorMessage::NoPostPicFound))?;

    Ok(HttpResponse::Ok().content_type("image/jpeg").body(pic))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::VERSION;
    use crate::models::{ClientPostComment, DbPostComment, DbPostLike};
    use crate::server::Server;
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils;
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, dbclient::DbClient, AppState};
    use actix_web::web::Bytes;
    use actix_web::{body::MessageBody, http, test, web, App};
    use futures::{SinkExt, StreamExt};
    use pretty_assertions::assert_eq;
    use reqwest::Method;
    use serde_json::{self, json};
    use sqlx::mysql::MySqlPool;
    use tokio::time::{sleep, Duration};
    use tokio_tungstenite::tungstenite::Message;

    #[sqlx::test]
    async fn test_get_posts(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_posts_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "content", 0, None).await.unwrap();
        let post: DbPost = dbclient.get_post(1, None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![post.into_client((&dbclient, None), config.domain()).await],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_posts_next_url(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());
        dbclient.save_post(1, "content", 0, None).await.unwrap();
        dbclient.save_post(1, "content", 0, None).await.unwrap();
        let post: DbPost = dbclient.get_post(2, None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/v1/posts/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![post.into_client((&dbclient, None), config.domain()).await],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?&page_size=1&page=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_posts_previous_url(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "content", 0, None).await.unwrap();
        dbclient.save_post(1, "content", 0, None).await.unwrap();
        let post: DbPost = dbclient.get_post(1, None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some("http://localhost:4000/v1/posts/?page=1&page_size=1".to_string()),
            result: vec![post.into_client((&dbclient, None), config.domain()).await],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?&page_size=1&page=2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_posts_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let first_id: usize = dbclient.save_post(1, "content", 0, None).await.unwrap();
        let first: ClientPost = dbclient
            .get_post(first_id, None)
            .await
            .unwrap()
            .into_client((&dbclient, None), config.domain())
            .await;
        let second_id: usize = dbclient.save_post(1, "content", 0, None).await.unwrap();
        let second: ClientPost = dbclient
            .get_post(second_id, None)
            .await
            .unwrap()
            .into_client((&dbclient, None), config.domain())
            .await;
        let posts: Vec<ClientPost> = vec![second, first];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: posts,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_posts_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_save_post(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let payload: Bytes = Bytes::from(
            "testasdadsad\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"content\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 15\r\n\r\n\
             This is a post!\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
        );

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();

        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post = dbclient_clone.get_post(1, None).await.unwrap();
        assert_eq!(post.content, "This is a post!");
    }

    #[sqlx::test]
    async fn test_save_post_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let payload: Bytes = Bytes::from(
            "testasdadsad\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"content\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 15\r\n\r\n\
             This is a post!\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
        );

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();

        let resp = test::call_service(&app, req).await;

        let post: ClientPost = dbclient_clone
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient_clone, None), config.domain())
            .await;
        let expected: String = serde_json::to_string(&SavePostResponse {
            message: SuccessMessage::SavedPost.into(),
            result: post,
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_save_post_with_nothing(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut payload: Vec<u8> = Vec::new();

        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"content\"\r\n\
          Content-Type: text/plain; charset=utf-8\r\nContent-Length: 0\r\n\r\n\\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n"
            .into_iter()
            .for_each(|b: &u8| payload.push(*b));

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header(http::header::ContentType(mime::MULTIPART_FORM_DATA))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .set_payload(payload)
            .to_request();

        let body = test::call_and_read_body(&app, req).await;
        assert_eq!(
            "{\"status\":400,\"message\":\"Cannot post a empty post.\"}",
            body
        );
    }

    #[sqlx::test]
    async fn test_save_post_with_too_many_characters(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut content: String = String::new();

        for _ in 0..=255 {
            content.push_str("w");
        }

        let payload: Bytes = Bytes::from(format!(
            "testasdadsad\r\n\
                 --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
                 Content-Disposition: form-data; name=\"content\"\r\n\
                 Content-Type: text/plain; charset=utf-8\r\nContent-Length: 256\r\n\r\n\
                 {}\r\n\
                 --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
            content,
        ));

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();

        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_save_post_with_text_and_images(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut payload: Vec<u8> = Vec::new();

        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"content\"\r\n\
          Content-Type: text/plain; charset=utf-8\r\nContent-Length: 15\r\n\r\n\
          This is a post!\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"files[]\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n".into_iter().for_each(|b: &u8| payload.push(*b));

        let payload: Bytes = Bytes::from(payload);

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();

        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post = dbclient_clone.get_post(1, None).await.unwrap();
        assert_eq!(post.content, "This is a post!");
        assert_eq!(post.image_count, 1);
        assert!(POSTS_DIRECTORY.as_path().join("1_0.jpeg").exists());
    }

    #[sqlx::test]
    async fn test_save_post_with_only_images(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut payload: Vec<u8> = Vec::new();
        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"files[]\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n".into_iter().for_each(|b: &u8| payload.push(*b));

        let payload: Bytes = Bytes::from(payload);

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();
        let resp = test::call_service(&app, req).await;
        let post = dbclient.get_post(1, None).await.unwrap();

        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(post.content, "");
        assert_eq!(post.image_count, 1);
        assert!(POSTS_DIRECTORY.as_path().join("1_0.jpeg").exists());
    }

    #[sqlx::test]
    async fn test_save_post_with_many_images(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut payload: Vec<u8> = Vec::new();
        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"files[]\"; filename=\"first.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"files[]\"; filename=\"second.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n".into_iter().for_each(|b: &u8| payload.push(*b));

        let payload: Bytes = Bytes::from(payload);

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();
        let resp = test::call_service(&app, req).await;
        let post = dbclient.get_post(1, None).await.unwrap();

        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(post.content, "");
        assert_eq!(post.image_count, 2);
        assert!(POSTS_DIRECTORY.join("1_0.jpeg").exists());
        assert!(POSTS_DIRECTORY.join("1_1.jpeg").exists());
    }

    #[sqlx::test]
    async fn test_save_post_with_garbage_as_image(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());
        let config = Config::new_test();

        let mut payload: Vec<u8> = Vec::new();

        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"files[]\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          some_hacked_garbage\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n"
            .into_iter()
            .for_each(|b: &u8| payload.push(*b));

        let payload: Bytes = Bytes::from(payload);

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();

        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);

        let post: Option<DbPost> = dbclient_clone.get_post(1, None).await;
        assert_eq!(post.is_none(), true);

        let is_posts_dir_empty: bool = std::fs::read_dir(POSTS_DIRECTORY.as_path())
            .unwrap()
            .next()
            .is_none();
        assert!(is_posts_dir_empty);
    }

    #[sqlx::test]
    async fn test_save_post_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: SavePostResponse = test_utils::do_multipart_request(
            1,
            Method::POST,
            format!("http://127.0.0.1:{port}/{VERSION}/posts/"),
            reqwest::multipart::Form::new().text("content", "i went to korea!"),
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::CreatePost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientPost =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::CreatePost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientPost =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
    }

    #[sqlx::test]
    async fn test_save_post_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header(http::header::ContentType(mime::MULTIPART_FORM_DATA))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_post(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_post_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let post_id: usize = dbclient.save_post(1, "hii", 0, None).await.unwrap();
        let post: ClientPost = dbclient
            .get_post(post_id, None)
            .await
            .unwrap()
            .into_client((&dbclient, None), config.domain())
            .await;

        let expected: String = serde_json::to_string(&GetPostResponse {
            message: SuccessMessage::GotPost.into(),
            result: post,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_post_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_post_which_im_not_allowed_to_view(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_post_which_belongs_to_a_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_post_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_post(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_delete_post_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();
        let post: ClientPost = dbclient
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient, None), config.domain())
            .await;

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(
            resp.into_body().try_into_bytes().unwrap(),
            serde_json::to_string(&DeletePostResponse {
                message: SuccessMessage::DeletedPost.into(),
                result: post,
            })
            .unwrap(),
        );
    }

    #[sqlx::test]
    async fn test_delete_post_deletes_all_likes(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_posts(&pool).await;
        test_utils::init_test_post_likes(&pool).await;
        let dbclient = DbClient::from_pool(pool.clone());
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        test::call_service(
            &app,
            test::TestRequest::delete()
                .uri("/1")
                .insert_header((
                    http::header::AUTHORIZATION,
                    format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
                ))
                .to_request(),
        )
        .await;

        let likes: Vec<DbPostLike> = dbclient.get_post_likes(1, None).await;
        assert_eq!(likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_post_deletes_all_comments(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_posts(&pool).await;
        test_utils::init_test_post_comments(&pool).await;
        let dbclient = DbClient::from_pool(pool.clone());
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        test::call_service(
            &app,
            test::TestRequest::delete()
                .uri("/1")
                .insert_header((
                    http::header::AUTHORIZATION,
                    format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
                ))
                .to_request(),
        )
        .await;

        let comments: Vec<DbPostComment> = dbclient.get_post_comments(1, None).await;
        assert_eq!(comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_post_deletes_all_images(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool.clone());
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut payload: Vec<u8> = Vec::new();
        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"files[]\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n".into_iter().for_each(|b: &u8| payload.push(*b));

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req_save_post = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();

        let req_delete_post = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();

        let resp_save_post = test::call_service(&app, req_save_post).await;

        assert!(POSTS_DIRECTORY.as_path().join("1_0.jpeg").exists());

        let resp_delete_post = test::call_service(&app, req_delete_post).await;

        assert_eq!(POSTS_DIRECTORY.as_path().join("1_0.jpeg").exists(), false);

        assert_eq!(resp_save_post.status(), http::StatusCode::OK);
        assert_eq!(resp_delete_post.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_delete_post_which_isnt_mine(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool.clone());
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_post_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_post_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient
            .save_post(1, "i went to korea!", 0, None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: DeletePostResponse = test_utils::do_http_request(
            1,
            Method::DELETE,
            format!("http://127.0.0.1:{port}/{VERSION}/posts/1"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::RemovePost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientPost =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::RemovePost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientPost =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
    }

    #[sqlx::test]
    async fn test_delete_post_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_like_post(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient_clone
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient_clone, None), "")
            .await;
        assert_eq!(post.likes.len(), 1);
    }

    #[sqlx::test]
    async fn test_like_post_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let config_clone = config.clone();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient_clone
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient_clone, None), config_clone.domain())
            .await;

        let expected: String = serde_json::to_string(&LikePostResponse {
            message: SuccessMessage::LikedPost.into(),
            result: post,
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_like_post_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_like_post_twice(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();
        dbclient.like_post(1, 1, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_like_post_of_whom_isnt_your_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_like_post_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("rena", "rena@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(2, 3, None).await.unwrap();
        dbclient
            .save_post(2, "i went to korea!", 0, None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;
        let (mut client_three_write, mut client_three_read) =
            test_utils::create_ws_client(3, port).await;

        let response: LikePostResponse = test_utils::do_http_request(
            1,
            Method::PATCH,
            format!("http://127.0.0.1:{port}/{VERSION}/posts/1/like"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::LikePost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientPost =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::LikePost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientPost =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let socket_event_client_three: SocketEvent = loop {
            match client_three_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::LikePost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_three_payload: ClientPost =
            serde_json::from_str(&socket_event_client_three.payload).unwrap();

        let _ = client_one_write.close().await;
        let _ = client_two_write.close().await;
        let _ = client_three_write.close().await;
        server.stop(false).await;
        drop(server);

        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
        assert_eq!(socket_event_client_three_payload, response.result);
    }

    #[sqlx::test]
    async fn test_like_post_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_unlike_post(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();
        dbclient.like_post(1, 1, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient_clone
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient_clone, None), config.domain())
            .await;
        assert_eq!(post.likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_unlike_post_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();
        dbclient.like_post(1, 1, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient_clone
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient_clone, None), config.domain())
            .await;
        let expected: String = serde_json::to_string(&UnlikePostResponse {
            message: SuccessMessage::UnlikedPost.into(),
            result: post,
        })
        .unwrap();
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_unlike_post_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_unlike_post_twice(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_unlike_post_of_whom_isnt_your_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0, None).await.unwrap();
        dbclient.like_post(1, 2, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_unlike_post_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("rena", "rena@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(2, 3, None).await.unwrap();
        dbclient
            .save_post(2, "i went to korea!", 0, None)
            .await
            .unwrap();
        dbclient.like_post(1, 1, None).await.unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;
        let (mut client_three_write, mut client_three_read) =
            test_utils::create_ws_client(3, port).await;

        let response: UnlikePostResponse = test_utils::do_http_request(
            1,
            Method::PATCH,
            format!("http://127.0.0.1:{port}/{VERSION}/posts/1/unlike"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UnlikePost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientPost =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UnlikePost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientPost =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let socket_event_client_three: SocketEvent = loop {
            match client_three_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UnlikePost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_three_payload: ClientPost =
            serde_json::from_str(&socket_event_client_three.payload).unwrap();

        let _ = client_one_write.close().await;
        let _ = client_two_write.close().await;
        let _ = client_three_write.close().await;
        server.stop(false).await;
        drop(server);

        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
        assert_eq!(socket_event_client_three_payload, response.result);
    }

    #[sqlx::test]
    async fn test_unlike_post_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_post_comment(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::post().to(post_comment)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .set_json(PostCommentBody {
                content: "lol".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let comments: Vec<ClientPostComment> = dbclient_clone
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient_clone, None), config.domain())
            .await
            .comments;
        assert_eq!(comments.len(), 1);
    }

    #[sqlx::test]
    async fn test_post_comment_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::post().to(post_comment)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .set_json(PostCommentBody {
                content: "lol".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient, None), config.domain())
            .await;
        let expected: String = serde_json::to_string(&PostCommentResponse {
            message: SuccessMessage::CommentPost.into(),
            result: post,
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_post_comment_to_which_doesnt_exist(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::post().to(post_comment)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .set_json(PostCommentBody {
                content: "lol".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_post_comment_of_whom_isnt_your_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::post().to(post_comment)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .set_json(PostCommentBody {
                content: "lol".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_post_comment_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("rena", "rena@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(2, 3, None).await.unwrap();
        dbclient
            .save_post(2, "i went to korea!", 0, None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;
        let (mut client_three_write, mut client_three_read) =
            test_utils::create_ws_client(3, port).await;

        let response: PostCommentResponse = test_utils::do_http_request(
            1,
            Method::POST,
            format!("http://127.0.0.1:{port}/{VERSION}/posts/1/comments"),
            Some(json!({
                "content": "wow thats cool",
            })),
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::CommentPost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientPost =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::CommentPost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientPost =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let socket_event_client_three: SocketEvent = loop {
            match client_three_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::CommentPost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_three_payload: ClientPost =
            serde_json::from_str(&socket_event_client_three.payload).unwrap();

        let _ = client_one_write.close().await;
        let _ = client_two_write.close().await;
        let _ = client_three_write.close().await;
        server.stop(false).await;
        drop(server);

        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
        assert_eq!(socket_event_client_three_payload, response.result);
    }

    #[sqlx::test]
    async fn test_post_comment_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::post().to(post_comment)),
        )
        .await;

        let req = test::TestRequest::post().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_comment(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();
        dbclient.save_post_comment(1, 1, "lol", None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{post_id}/{comment_id}", web::delete().to(delete_comment)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let comments: Vec<ClientPostComment> = dbclient
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient, None), config.domain())
            .await
            .comments;
        assert_eq!(comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_comment_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();
        dbclient.save_post_comment(1, 1, "lol", None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{post_id}/{comment_id}", web::delete().to(delete_comment)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient, None), config.domain())
            .await;
        let expected: String = serde_json::to_string(&DeleteCommentResponse {
            message: SuccessMessage::DeletedPostComment.into(),
            result: post,
        })
        .unwrap();
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_delete_comment_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}/{comment_id}", web::delete().to(delete_comment)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_comment_which_im_not_the_author(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0, None).await.unwrap();
        dbclient.save_post_comment(1, 2, "lol", None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}/{comment_id}", web::delete().to(delete_comment)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_comment_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("rena", "rena@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(2, 3, None).await.unwrap();
        dbclient
            .save_post(2, "i went to korea!", 0, None)
            .await
            .unwrap();
        dbclient
            .save_post_comment(1, 1, "wow thats cool", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;
        let (mut client_three_write, mut client_three_read) =
            test_utils::create_ws_client(3, port).await;

        let response: DeleteCommentResponse = test_utils::do_http_request(
            1,
            Method::DELETE,
            format!("http://127.0.0.1:{port}/{VERSION}/posts/1/comments/1"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::RemoveCommentPost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientPost =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::RemoveCommentPost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientPost =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let socket_event_client_three: SocketEvent = loop {
            match client_three_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::RemoveCommentPost {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_three_payload: ClientPost =
            serde_json::from_str(&socket_event_client_three.payload).unwrap();

        let _ = client_one_write.close().await;
        let _ = client_two_write.close().await;
        let _ = client_three_write.close().await;
        server.stop(false).await;
        drop(server);

        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
        assert_eq!(socket_event_client_three_payload, response.result);
    }

    #[sqlx::test]
    async fn test_delete_comment_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}/{comment_id}", web::delete().to(delete_comment)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/1/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_post_pic(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut payload: Vec<u8> = Vec::new();
        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"files[]\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n".into_iter().for_each(|b: &u8| payload.push(*b));

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post))
                .route("/{post_id}/{post_pic_name}", web::get().to(get_post_pic)),
        )
        .await;

        let req_save_post = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();
        let req_post_pic = test::TestRequest::default()
            .uri("/1/1_0")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();

        let resp_save_post = test::call_service(&app, req_save_post).await;
        let resp_post_pic = test::call_service(&app, req_post_pic).await;
        assert_eq!(resp_save_post.status(), http::StatusCode::OK);
        assert_eq!(resp_post_pic.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_post_pic_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}/{post_pic_name}", web::get().to(get_post_pic)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1/1_0")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }
}
