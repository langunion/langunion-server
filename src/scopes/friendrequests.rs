use crate::dbclient::DbClient;
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{ClientFriendrequest, ClientUser, DbFriendrequest, ToPayload};
use crate::openapi::PaginatedResponseClientFriendrequest;
use crate::success::SuccessMessage;
use crate::utils::paginate::Paginator;
use crate::ws::events::{SocketAction, SocketEvent};
use crate::AppState;
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToResponse, ToSchema};
use validator::Validate;

pub fn friendrequests_scope() -> Scope {
    web::scope("/friendrequests")
        .route("/", web::get().to(get_friendrequests))
        .route("/", web::post().to(send_friendrequest))
        .route("/{friendreq_id}/", web::get().to(get_friendrequest))
        .route("/{friendreq_id}/", web::delete().to(delete_friendrequest))
        .route(
            "/{friendreq_id}/accept/",
            web::patch().to(accept_friendrequest),
        )
}

#[derive(Serialize, Deserialize, Validate, IntoParams)]
struct QueryGetFriendrequests {
    #[validate(range(min = 1, message = "'page' param has to be at least 1"))]
    page: Option<usize>,
    #[validate(range(
        min = 1,
        max = 50,
        message = "'page_size' param has to be in the range of 1 and 50"
    ))]
    page_size: Option<usize>,
}

/// Get a list of your pending friendrequests
#[utoipa::path(
    get,
    path = "/friendrequests/",
    params(QueryGetFriendrequests),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = PaginatedResponseClientFriendrequest)),
)]
async fn get_friendrequests(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    query: web::Query<QueryGetFriendrequests>,
) -> HttpResponse {
    let query: QueryGetFriendrequests = query.into_inner();
    let user_id: usize = auth_token.id;
    let blocked_user_ids: Vec<u64> = app_state
        .dbclient
        .get_blocked_users(user_id, None)
        .await
        .into_iter()
        .map(|b| b.blocked_user_id)
        .collect();
    let friendrequests: Vec<ClientFriendrequest> = app_state
        .dbclient
        .get_friendrequests(user_id, None)
        .await
        .into_iter()
        .filter(|req: &DbFriendrequest| {
            !blocked_user_ids.contains(&req.from_user) && !blocked_user_ids.contains(&req.to_user)
        })
        .map(|req: DbFriendrequest| req.into_client(app_state.config.domain()))
        .collect();

    HttpResponse::Ok().json(
        Paginator::<ClientFriendrequest>::new()
            .page(query.page)
            .page_size(query.page_size)
            .url(app_state.config.domain())
            .pathname("/friendrequests")
            .data(friendrequests)
            .into_paginated_response(),
    )
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct SendFriendrequestBody {
    to_user: usize,
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully sent a new friendrequest")]
pub struct SendFriendrequestResponse {
    pub message: String,
    pub result: ClientFriendrequest,
}

/// Send a friendrequest to a user
#[utoipa::path(
    post,
    path = "/friendrequests/",
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    request_body=SendFriendrequestBody,
    responses((status = 201, response = SendFriendrequestResponse)),
)]
async fn send_friendrequest(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    body: web::Json<SendFriendrequestBody>,
) -> Result<HttpResponse, HttpError> {
    let body: SendFriendrequestBody = body.into_inner();
    let user_id: usize = auth_token.id;
    let to_user_id: usize = body.to_user;
    /* Check if you try to send yourself a friendrequest */
    if body.to_user == user_id {
        return Err(HttpError::bad_request(
            ErrorMessage::CantSendYourselfFriendrequest,
        ));
    }
    /* Check if there is already a pending friendrequest */
    if app_state
        .dbclient
        .get_friendrequest(user_id, to_user_id, None)
        .await
        .is_some()
    {
        return Err(HttpError::bad_request(
            ErrorMessage::AlreadyPendingFriendrequest,
        ));
    }
    /* Check if the user is blocked */
    if app_state
        .dbclient
        .get_blocked_user(user_id, to_user_id, None)
        .await
        .is_some()
    {
        return Err(HttpError::bad_request(ErrorMessage::YouHaveBlockedThisUser));
    }
    /* Check if the to_user_id exists */
    app_state
        .dbclient
        .get_user(to_user_id, None)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?;
    /* Save friendrequest */
    app_state
        .dbclient
        .save_friendrequest(user_id, to_user_id, None)
        .await?;

    let friendrequest: ClientFriendrequest = app_state
        .dbclient
        .get_friendrequest(user_id, to_user_id, None)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(app_state.config.domain());

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::SendFriendrequest,
                payload: friendrequest.to_payload()?,
                message: "You've send a friendrequest".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::SendFriendrequest,
                payload: friendrequest.to_payload()?,
                message: "You've received a friendrequest".into(),
            },
            &[to_user_id],
        ),
    ]);

    Ok(HttpResponse::Ok().json(SendFriendrequestResponse {
        message: SuccessMessage::SentFriendrequest.into(),
        result: friendrequest,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully got friendrequest")]
pub struct GetFriendrequestResponse {
    pub message: String,
    pub result: ClientFriendrequest,
}

/// Get a specific friendrequest
#[utoipa::path(
    get,
    path = "/friendrequests/{friendreq_id}/",
    params(("friendreq_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = GetFriendrequestResponse)),
)]
async fn get_friendrequest(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let friendrequest_id: usize = path.into_inner();

    let friendrequest: ClientFriendrequest = app_state
        .dbclient
        .get_friendrequest_by_id(friendrequest_id, None)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoFriendrequestFound))
        .and_then(|req: DbFriendrequest| {
            let is_users_friendrequest: bool =
                req.from_user as usize == user_id || req.to_user as usize == user_id;
            match is_users_friendrequest {
                true => Ok(req),
                false => Err(HttpError::bad_request(ErrorMessage::NotYourFriendrequest)),
            }
        })?
        .into_client(app_state.config.domain());

    Ok(HttpResponse::Ok().json(GetFriendrequestResponse {
        message: SuccessMessage::GotFriendrequest.to_string(),
        result: friendrequest,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Accepted the friendrequest")]
pub struct AcceptFriendrequestResponse {
    pub message: String,
    pub result: ClientUser,
}

/// Accept a pending friendrequest
#[utoipa::path(
    patch,
    path = "/friendrequests/{friendreq_id}/accept/",
    params(("friendreq_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = AcceptFriendrequestResponse)),
)]
async fn accept_friendrequest(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let friendrequest_id: usize = path.into_inner();

    let mut transaction = app_state.dbclient.begin_transaction().await?;

    /* Check if its legal to accept */
    let user_id_of_friend: usize = app_state
        .dbclient
        .get_friendrequest_by_id(friendrequest_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoFriendrequestFound))
        .and_then(|req: DbFriendrequest| {
            let can_user_accept: bool =
                req.from_user as usize != user_id && req.to_user as usize == user_id;
            match can_user_accept {
                true => Ok(req.from_user as usize),
                false => Err(HttpError::bad_request(
                    ErrorMessage::CantAcceptFriendrequest,
                )),
            }
        })?;
    /* Check if the user is blocked */
    if app_state
        .dbclient
        .get_blocked_user(user_id, user_id_of_friend, None)
        .await
        .is_some()
    {
        return Err(HttpError::bad_request(ErrorMessage::YouHaveBlockedThisUser));
    }
    /* Create new friend */
    app_state
        .dbclient
        .save_friend(user_id, user_id_of_friend, Some(&mut transaction))
        .await?;
    /* Remove obsolete friendrequest */
    app_state
        .dbclient
        .delete_friendrequest(friendrequest_id, Some(&mut transaction))
        .await?;
    /* Get the new friend */
    let friend: ClientUser = app_state
        .dbclient
        .get_user(user_id_of_friend, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;
    /* Get the myself to send as socket event */
    let user: ClientUser = app_state
        .dbclient
        .get_user(user_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::AcceptFriendrequest,
                payload: friend.to_payload()?,
                message: "You've accepted a friendrequest".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::AcceptFriendrequest,
                payload: user.to_payload()?,
                message: "A friendrequest of yours was accepted".into(),
            },
            &[user_id_of_friend],
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(AcceptFriendrequestResponse {
        message: SuccessMessage::AcceptedFriendrequest.into(),
        result: friend,
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Declined and deleted the friendrequest")]
pub struct DeleteFriendrequestResponse {
    pub message: String,
    pub result: ClientFriendrequest,
}

/// Delete aka. decline a pending friendrequest
#[utoipa::path(
    delete,
    path = "/friendrequests/{friendreq_id}/",
    params(("friendreq_id" = u64, Path,)),
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = DeleteFriendrequestResponse)),
)]
async fn delete_friendrequest(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let friendrequest_id: usize = path.into_inner();
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    let friendrequest: DbFriendrequest = app_state
        .dbclient
        .get_friendrequest_by_id(friendrequest_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoFriendrequestFound))
        .and_then(|req: DbFriendrequest| {
            /* Check if its legal to decline */
            let can_user_decline: bool =
                req.from_user as usize == user_id || req.to_user as usize == user_id;
            match can_user_decline {
                true => Ok(req),
                false => Err(HttpError::bad_request(
                    ErrorMessage::CantDeclineFriendrequest,
                )),
            }
        })?;

    let socket_to_user_id: usize = if friendrequest.from_user as usize == user_id {
        friendrequest.to_user as usize
    } else {
        friendrequest.from_user as usize
    };

    let friendrequest: ClientFriendrequest = friendrequest.into_client(app_state.config.domain());

    /* Delete the friendrequest */
    app_state
        .dbclient
        .delete_friendrequest(friendrequest_id, Some(&mut transaction))
        .await?;

    app_state.websocket_server_handle.emit_socket_events(&[
        (
            SocketEvent {
                action: SocketAction::DeclineFriendrequest,
                payload: friendrequest.to_payload()?,
                message: "You've declined a friendrequest".into(),
            },
            &[user_id],
        ),
        (
            SocketEvent {
                action: SocketAction::DeclineFriendrequest,
                payload: friendrequest.to_payload()?,
                message: "A friendrequest got declined".into(),
            },
            &[socket_to_user_id],
        ),
    ]);

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(DeleteFriendrequestResponse {
        message: SuccessMessage::DeletedFriendrequest.into(),
        result: friendrequest,
    }))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::VERSION;
    use crate::server::Server;
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils;
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, dbclient::DbClient, AppState};
    use actix_web::{body::MessageBody, http, test, web, App};
    use futures::{SinkExt, StreamExt};
    use pretty_assertions::assert_eq;
    use reqwest::Method;
    use serde_json::json;
    use sqlx::mysql::MySqlPool;
    use tokio::time::{sleep, Duration};
    use tokio_tungstenite::tungstenite::Message;

    #[sqlx::test]
    fn test_get_friendrequests(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_get_friendrequests_paginated_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let friendreq: ClientFriendrequest = dbclient
            .get_friendrequest(1, 2, None)
            .await
            .unwrap()
            .into_client(config.domain());
        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![friendreq],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friendrequests_previous_url(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("martin", "martin@mail.de", "1234asdf", None)
            .await
            .unwrap();

        dbclient.save_friendrequest(1, 2, None).await.unwrap();
        dbclient.save_friendrequest(1, 3, None).await.unwrap();
        let friendrequest: ClientFriendrequest = dbclient
            .get_friendrequest(1, 3, None)
            .await
            .unwrap()
            .into_client(config.domain());

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some(
                "http://localhost:4000/v1/friendrequests/?page=1&page_size=1".to_string(),
            ),
            result: vec![friendrequest],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page_size=1&page=2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friendrequests_next_url(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("martin", "martin@mail.de", "1234asdf", None)
            .await
            .unwrap();

        dbclient.save_friendrequest(1, 2, None).await.unwrap();
        dbclient.save_friendrequest(1, 3, None).await.unwrap();
        let friendrequest: ClientFriendrequest = dbclient
            .get_friendrequest(1, 2, None)
            .await
            .unwrap()
            .into_client(config.domain());

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/v1/friendrequests/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![friendrequest],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page_size=1&page=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friendrequests_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("thomas", "thomas@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friendrequest(1, 2, None).await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient.save_friendrequest(1, 3, None).await.unwrap();
        let friendrequests: Vec<ClientFriendrequest> = vec![
            dbclient
                .get_friendrequest(1, 2, None)
                .await
                .unwrap()
                .into_client(config.domain()),
            dbclient
                .get_friendrequest(1, 3, None)
                .await
                .unwrap()
                .into_client(config.domain()),
        ];
        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: friendrequests,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_get_friendrequests_filters_blocked_users(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_friendrequest(1, 2, None).await.unwrap();
        dbclient.save_friendrequest(1, 3, None).await.unwrap();
        dbclient.block_user(1, 3, None).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![ClientFriendrequest {
                id: 1,
                from_user: "http://localhost:4000/v1/users/1/".to_string(),
                to_user: "http://localhost:4000/v1/users/2/".to_string(),
                accept: "http://localhost:4000/v1/friendrequests/1/accept/".to_string(),
                decline: "http://localhost:4000/v1/friendrequests/1/".to_string(),
            }],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_get_friendrequests_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_send_friendrequest(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_send_friendrequest_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&SendFriendrequestResponse {
            message: SuccessMessage::SentFriendrequest.into(),
            result: ClientFriendrequest {
                id: 1,
                from_user: "http://localhost:4000/v1/users/1/".to_string(),
                to_user: "http://localhost:4000/v1/users/2/".to_string(),
                accept: "http://localhost:4000/v1/friendrequests/1/accept/".to_string(),
                decline: "http://localhost:4000/v1/friendrequests/1/".to_string(),
            },
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_send_friendrequest_to_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_send_friendrequest_but_already_pending(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_send_friendrequest_but_user_does_not_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_send_friendrequest_created_friendrequest(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(
            dbclient_clone
                .get_friendrequest_by_id(1, None)
                .await
                .is_some(),
            true
        );
    }

    #[sqlx::test]
    fn test_send_friendrequest_to_yourself(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 1 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_send_friendrequest_to_a_blocked_user(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.block_user(1, 2, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_send_friendrequest_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 99 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_send_friendrequest_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: SendFriendrequestResponse = test_utils::do_http_request(
            1,
            Method::POST,
            format!("http://127.0.0.1:{port}/{VERSION}/friendrequests"),
            Some(json!({
                "to_user": 2,
            })),
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::SendFriendrequest {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientFriendrequest =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::SendFriendrequest {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientFriendrequest =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
    }

    #[sqlx::test]
    fn test_get_friendrequest(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::get().to(get_friendrequest)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_get_friendrequest_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&GetFriendrequestResponse {
            message: SuccessMessage::GotFriendrequest.into(),
            result: ClientFriendrequest {
                id: 1,
                from_user: "http://localhost:4000/v1/users/1/".to_string(),
                to_user: "http://localhost:4000/v1/users/2/".to_string(),
                accept: "http://localhost:4000/v1/friendrequests/1/accept/".to_string(),
                decline: "http://localhost:4000/v1/friendrequests/1/".to_string(),
            },
        })
        .unwrap();
        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::get().to(get_friendrequest)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_get_friendrequest_that_doesnt_exist(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::get().to(get_friendrequest)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_get_friendrequest_which_isnt_mine(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::get().to(get_friendrequest)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_get_friendrequest_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::get().to(get_friendrequest)),
        )
        .await;

        let req = test::TestRequest::default().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_accept_friendrequest(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&AcceptFriendrequestResponse {
            message: SuccessMessage::AcceptedFriendrequest.into(),
            result: dbclient
                .get_user(1, None)
                .await
                .unwrap()
                .into_client(config.domain(), websocket_server_handle.clone())
                .await,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_which_doesnt_exist(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_which_isnt_mine(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_which_i_sent(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_removed_friendrequest(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(
            dbclient_clone
                .get_friendrequest_by_id(1, None)
                .await
                .is_none(),
            true
        );
    }

    #[sqlx::test]
    fn test_accept_friendrequest_created_new_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(dbclient_clone.get_friend(2, 1, None).await.is_some(), true);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_of_a_blocked_user(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_friendrequest(2, 1, None).await.unwrap();
        dbclient.block_user(1, 2, None).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friendrequest(2, 1, None).await.unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: AcceptFriendrequestResponse = test_utils::do_http_request(
            1,
            Method::PATCH,
            format!("http://127.0.0.1:{port}/{VERSION}/friendrequests/1/accept"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::AcceptFriendrequest {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientUser =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::AcceptFriendrequest {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientUser =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_ne!(socket_event_client_two_payload, response.result);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_delete_friendrequest(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_delete_friendrequest_response(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&DeleteFriendrequestResponse {
            message: SuccessMessage::DeletedFriendrequest.into(),
            result: dbclient
                .get_friendrequest_by_id(1, None)
                .await
                .unwrap()
                .into_client(config.domain()),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_delete_friendrequest_which_doesnt_exist(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/9")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_delete_friendrequest_which_isnt_mine(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_delete_friendrequest_i_sent(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let _ = dbclient.save_friendrequest(1, 2, None).await;

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert!(dbclient.get_friendrequest_by_id(1, None).await.is_none());
    }

    #[sqlx::test]
    fn test_delete_friendrequest_i_received(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let _ = dbclient.save_friendrequest(2, 1, None).await;

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert!(dbclient.get_friendrequest_by_id(1, None).await.is_none());
    }

    #[sqlx::test]
    fn test_delete_friendrequest_removed_friendrequest(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert!(dbclient_clone
            .get_friendrequest_by_id(1, None)
            .await
            .is_none());
    }

    #[sqlx::test]
    fn test_delete_friendrequest_didnt_create_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert!(dbclient_clone.get_friend(2, 1, None).await.is_none());
    }

    #[sqlx::test]
    fn test_delete_friendrequest_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friendrequest(1, 2, None).await.unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let response: DeleteFriendrequestResponse = test_utils::do_http_request(
            1,
            Method::DELETE,
            format!("http://127.0.0.1:{port}/{VERSION}/friendrequests/1"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::DeclineFriendrequest {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientFriendrequest =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;

        let socket_event_client_two: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::DeclineFriendrequest {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_two_payload: ClientFriendrequest =
            serde_json::from_str(&socket_event_client_two.payload).unwrap();
        let _ = client_two_write.close().await;

        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload, response.result);
        assert_eq!(socket_event_client_two_payload, response.result);
    }
}
