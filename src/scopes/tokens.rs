use crate::dbclient::DbClient;
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{ClientUser, DbUser, ToPayload};
use crate::success::SuccessMessage;
use crate::utils::password_utils;
use crate::ws::events::{SocketAction, SocketEvent};
use crate::AppState;
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use utoipa::{ToResponse, ToSchema};
use validator::Validate;

pub fn tokens_scope() -> Scope {
    web::scope("/tokens")
        .route("/", web::post().to(create_tokens))
        .route("/refresh/", web::post().to(refresh_tokens))
        .route("/reset/", web::patch().to(reset_tokens))
        .route("/validate/", web::patch().to(validate_token))
}

#[derive(Serialize, Deserialize, Validate, ToSchema)]
pub struct CreateTokensBody {
    #[validate(email(message = "Please type in a valid email"))]
    email: String,
    #[validate(length(
        min = 8,
        max = 2000,
        message = "Password has to be at least 8 characters long"
    ))]
    password: String,
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully new token pair")]
pub struct CreateTokensResponse {
    access: String,
    refresh: String,
}

/// Create auth tokens aka. login
/// Create a new authentication token pair (bearer and refresh token)
#[utoipa::path(
    post,
    path = "/tokens/",
    request_body=CreateTokensBody,
    responses((status = 200, response = CreateTokensResponse)),
)]
async fn create_tokens(
    app_state: web::Data<AppState>,
    body: web::Json<CreateTokensBody>,
) -> Result<HttpResponse, HttpError> {
    let body: CreateTokensBody = body.into_inner();

    body.validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let user_id: usize = app_state
        .dbclient
        .get_user_by_email(&body.email, None)
        .await
        .ok_or(HttpError::unauthorized(ErrorMessage::WrongCredentials))
        .and_then(|user: DbUser| {
            let is_password_correct: bool = password_utils::compare(&body.password, &user.password);
            match is_password_correct {
                true => Ok(user.id as usize),
                false => Err(HttpError::unauthorized(ErrorMessage::WrongCredentials)),
            }
        })?;
    /* Create new token pair */
    let access: String =
        AuthenticationToken::create_access_token(user_id, &app_state.config.jwt_secret_token);
    let refresh: String =
        AuthenticationToken::create_refresh_token(user_id, &app_state.config.jwt_secret_token);
    app_state
        .dbclient
        .save_refresh_token(user_id, &refresh, None)
        .await?;
    Ok(HttpResponse::Ok().json(CreateTokensResponse { access, refresh }))
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct RefreshTokensBody {
    refresh: String,
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully created new access and refresh token pair")]
pub struct RefreshTokensResponse {
    access: String,
    refresh: String,
}

/// Refresh auth tokens
/// Create a new authentication token pair and also unvalidating the old one
#[utoipa::path(
    post,
    path = "/tokens/refresh/",
    request_body=RefreshTokensBody,
    responses((status = 200, response = RefreshTokensResponse)),
)]
async fn refresh_tokens(
    body: web::Json<RefreshTokensBody>,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let body: RefreshTokensBody = body.into_inner();
    let secret: &String = &app_state.config.jwt_secret_token;
    let user_id: usize = AuthenticationToken::decode_token(&body.refresh, secret)?;
    let mut transaction = app_state.dbclient.begin_transaction().await?;
    /* Check if refresh token is in db */
    app_state
        .dbclient
        .get_refresh_token(&body.refresh, Some(&mut transaction))
        .await
        .ok_or(HttpError::unauthorized(ErrorMessage::Unauthorized))?;
    /* Invalid this refresh token */
    app_state
        .dbclient
        .delete_refresh_token(&body.refresh, Some(&mut transaction))
        .await?;
    /* Create new token pair */
    let access: String = AuthenticationToken::create_access_token(user_id, secret);
    let refresh: String = AuthenticationToken::create_refresh_token(user_id, secret);
    app_state
        .dbclient
        .save_refresh_token(user_id, &refresh, Some(&mut transaction))
        .await?;

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(RefreshTokensResponse { access, refresh }))
}

#[derive(Serialize, Deserialize, ToResponse)]
#[response(description = "Successfully reset your tokens")]
pub struct ResetTokensResponse {
    message: String,
}

/// Reset all refresh tokens
/// Logout out the user from all devices
#[utoipa::path(
    patch,
    path = "/tokens/reset/",
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200, response = ResetTokensResponse)),
)]
async fn reset_tokens(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let mut transaction = app_state.dbclient.begin_transaction().await?;

    app_state
        .dbclient
        .delete_refresh_tokens(user_id, Some(&mut transaction))
        .await?;

    let user: ClientUser = app_state
        .dbclient
        .get_user(user_id, Some(&mut transaction))
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            app_state.config.domain(),
            app_state.websocket_server_handle.clone(),
        )
        .await;

    app_state.websocket_server_handle.emit_socket_event(
        SocketEvent {
            action: SocketAction::ResetTokens,
            payload: user.to_payload()?,
            message: "You've logged out from all devices".to_string(),
        },
        &[user_id],
    );

    DbClient::commit_transaction(transaction).await?;

    Ok(HttpResponse::Ok().json(ResetTokensResponse {
        message: SuccessMessage::ResettedTokens.into(),
    }))
}

#[derive(Serialize, Deserialize, ToResponse)]
pub struct ValidateTokenResponse {
    message: String, 
}

/// Validate bearer authentication token
/// Validate if a token is still valid/expired or anything
#[utoipa::path(
    patch,
    path = "/tokens/validate/",
    security(("bearerAuth"=[]), ("cookieAuth"=[])),
    responses((status = 200)),
)]
async fn validate_token(
    _auth_token: AuthenticationToken,
) -> HttpResponse {
    HttpResponse::Ok().json(&ValidateTokenResponse {
        message: SuccessMessage::ValidatedToken.into(),
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::VERSION;
    use crate::server::Server;
    use crate::utils::test_utils;
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, dbclient::DbClient, AppState};
    use actix_web::{http, test, web, App};
    use futures::{SinkExt, StreamExt};
    use pretty_assertions::assert_eq;
    use reqwest::Method;
    use sqlx::mysql::MySqlPool;
    use tokio::time::{sleep, Duration};
    use tokio_tungstenite::tungstenite::Message;

    #[sqlx::test]
    async fn test_create_tokens(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(create_tokens)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(CreateTokensBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_create_tokens_but_user_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(create_tokens)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(CreateTokensBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_create_tokens_but_with_wrong_credentials(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "asdf1234", None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(create_tokens)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(CreateTokensBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_create_tokens_validation(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(create_tokens)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(CreateTokensBody {
                email: "marc.mail".to_string(),
                password: "1234".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_refresh_tokens(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let refresh: String = test_utils::get_test_token(1, "shhh!");
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_refresh_token(1, &refresh, None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(refresh_tokens)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RefreshTokensBody { refresh })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_refresh_tokens_with_expired_token(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let refresh: String = test_utils::get_expired_test_token(1, "shhh!");
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_refresh_token(1, &refresh, None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(refresh_tokens)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RefreshTokensBody { refresh })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_refresh_tokens_with_invalid_token(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(refresh_tokens)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RefreshTokensBody {
                refresh: "invalid_token".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_reset_tokens(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(reset_tokens)),
        )
        .await;

        let req = test::TestRequest::patch()
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_reset_tokens_removes_all_refresh_tokens(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_refresh_token(1, test_utils::get_test_token(1, "shhh!"), None)
            .await
            .unwrap();
        dbclient
            .save_refresh_token(1, test_utils::get_test_token(1, "shhh!"), None)
            .await
            .unwrap();
        dbclient
            .save_refresh_token(1, test_utils::get_test_token(1, "shhh!"), None)
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(reset_tokens)),
        )
        .await;

        test::call_service(
            &app,
            test::TestRequest::patch()
                .insert_header((
                    http::header::AUTHORIZATION,
                    format!("Bearer {}", test_utils::get_test_token(1, "shhh!")),
                ))
                .to_request(),
        )
        .await;
        let refresh_tokens = dbclient.get_refresh_tokens(1, None).await;
        assert_eq!(refresh_tokens.len(), 0);
    }

    #[sqlx::test]
    fn test_reset_tokens_with_invalid_access_token(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(reset_tokens)),
        )
        .await;

        let req = test::TestRequest::patch()
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer some_garbage_token"),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_reset_tokens_with_expired_access_token(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(reset_tokens)),
        )
        .await;

        let req = test::TestRequest::patch()
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", test_utils::get_expired_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_reset_tokens_emits_socket_event(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient, port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;

        let _response: ResetTokensResponse = test_utils::do_http_request(
            1,
            Method::PATCH,
            format!("http://127.0.0.1:{port}/{VERSION}/tokens/reset/"),
            None,
            None,
        )
        .await;

        let socket_event_client_one: SocketEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::ResetTokens {
                            break (socket_event);
                        } else {
                            continue;
                        }
                    }
                }
                _ => continue,
            }
        };
        let socket_event_client_one_payload: ClientUser =
            serde_json::from_str(&socket_event_client_one.payload).unwrap();
        let _ = client_one_write.close().await;
        server.stop(false).await;
        drop(server);
        assert_eq!(socket_event_client_one_payload.id, 1);
    }

    #[sqlx::test]
    fn test_reset_tokens_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(reset_tokens)),
        )
        .await;

        let req = test::TestRequest::patch().to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }
}
