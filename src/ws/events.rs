use crate::ws::server::WebSocketConnection;
use serde::{Deserialize, Serialize};
use tokio::sync::oneshot;

/// Command (or SocketEvent) from Handle to Server
pub enum Command {
    Connect {
        user_id: usize,
        new_conn: WebSocketConnection,
    },
    Disconnect {
        conn: WebSocketConnection,
    },
    SocketEvent {
        event: SocketEvent,
        user_ids: Vec<usize>,
    },
    WebRtcEvent {
        event: WebRtcEvent,
        sender_connection: WebSocketConnection,
    },
    GetIsConnected {
        user_id: usize,
        sender: oneshot::Sender<bool>,
    },
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct SocketEvent {
    pub action: SocketAction,
    pub payload: String,
    pub message: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Copy, Clone)]
pub enum SocketAction {
    // Users
    DeleteUser,
    UpdateUser,
    UpdateUserAvatar,
    UserWentOnline,
    UserWentOffline,
    // Users - Friends
    RemoveFriend,
    // Users - BlockedUsers
    BlockUser,
    UnblockUser,
    // Tokens
    ResetTokens,
    // Friendrequests
    SendFriendrequest,
    AcceptFriendrequest,
    DeclineFriendrequest,
    // Posts
    CreatePost,
    RemovePost,
    LikePost,
    UnlikePost,
    CommentPost,
    RemoveCommentPost,
    // Message
    SendMessage,
    UpdateMessage,
    DeleteMessage,
    DeleteMessagePrivately,
    DeleteChat,
}

/// sender_socket_id gets overwritten by the server
/// on SignalingOffer and -Answer only
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct WebRtcEvent {
    pub action: WebRtcAction,
    pub payload: String,
    pub sender_user_id: usize,
    pub sender_socket_id: Option<String>,
    pub receiver_user_id: usize,
    pub receiver_socket_id: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum WebRtcAction {
    SignalingOffer,
    SignalingAnswer,
    Ice,
    RenegotiateOffer,
    RenegotiateAnswer,
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_deserialize_socket_event() {
        let socket_event = SocketEvent {
            action: SocketAction::SendMessage,
            payload: "test_payload".to_string(),
            message: "test_message".to_string(),
        };
        let should_be = "{\"action\":\"SendMessage\",\"payload\":\"test_payload\",\"message\":\"test_message\"}";
        assert_eq!(should_be, serde_json::to_string(&socket_event).unwrap());
    }

    #[test]
    fn test_serialize_socket_event() {
        let socket_event = "{\"action\":\"SendMessage\",\"payload\":\"test_payload\",\"message\":\"test_message\"}";
        let should_be = SocketEvent {
            action: SocketAction::SendMessage,
            payload: "test_payload".to_string(),
            message: "test_message".to_string(),
        };
        assert_eq!(
            should_be,
            serde_json::from_str::<SocketEvent>(&socket_event).unwrap()
        );
    }

    #[test]
    fn test_deserialize_webrtc_event() {
        let webrtc_event = WebRtcEvent {
            action: WebRtcAction::Ice,
            payload: "this is a legit ice candidate".to_string(),
            sender_user_id: 1,
            sender_socket_id: Some("xyz".to_string()),
            receiver_user_id: 2,
            receiver_socket_id: Some("abc".to_string()),
        };
        let should_be = "{\"action\":\"Ice\",\"payload\":\"this is a legit ice candidate\",\"sender_user_id\":1,\"sender_socket_id\":\"xyz\",\"receiver_user_id\":2,\"receiver_socket_id\":\"abc\"}";
        assert_eq!(should_be, serde_json::to_string(&webrtc_event).unwrap());
    }

    #[test]
    fn test_serialize_webrtc_event() {
        let webrtc_event = "{\"action\":\"Ice\",\"payload\":\"this is a legit ice candidate\",\"sender_user_id\":1,\"sender_socket_id\":\"xyz\",\"receiver_user_id\":2,\"receiver_socket_id\":\"abc\"}";
        let should_be = WebRtcEvent {
            action: WebRtcAction::Ice,
            payload: "this is a legit ice candidate".to_string(),
            sender_user_id: 1,
            sender_socket_id: Some("xyz".to_string()),
            receiver_user_id: 2,
            receiver_socket_id: Some("abc".to_string()),
        };
        assert_eq!(
            should_be,
            serde_json::from_str::<WebRtcEvent>(&webrtc_event).unwrap()
        );
    }
}
