use crate::config::{use_config, Config};
use crate::dbclient::DbClient;
use crate::models::ToPayload;
use crate::ws::events::{Command, SocketAction, SocketEvent, WebRtcAction, WebRtcEvent};
use actix_ws::{Message, MessageStream, Session};
use futures_util::StreamExt;
use log::{debug, error, info};
use std::cmp::{Eq, PartialEq};
use std::collections::{HashMap, HashSet};
use std::hash::{Hash, Hasher};
use tokio::sync::mpsc::{self, UnboundedReceiver, UnboundedSender};
use tokio::sync::oneshot;
use tokio::task::{spawn, spawn_local};
use tokio::time::{sleep, timeout, Duration};
use uuid::Uuid;

// ----------------------------------------------- Server Instance
pub struct WebSocketServer {
    connections: HashMap<usize, HashSet<WebSocketConnection>>,
    dbclient: DbClient,
    config: Config,
    server_handle: WebSocketServerHandle,
    receiver: UnboundedReceiver<Command>,
}

impl WebSocketServer {
    /// Create a new websocket server
    pub fn new(dbclient: DbClient) -> (Self, WebSocketServerHandle) {
        // The websocket server also has a handle to itself, dbclient and config
        // so its also able to create client models for socket events and etc.
        let (sender, receiver): (UnboundedSender<Command>, UnboundedReceiver<Command>) =
            mpsc::unbounded_channel();
        let connections: HashMap<usize, HashSet<WebSocketConnection>> = HashMap::new();
        let config = use_config();
        let websocket_server_handle = WebSocketServerHandle { sender };

        (
            Self {
                connections,
                dbclient,
                config,
                server_handle: websocket_server_handle.clone(),
                receiver,
            },
            websocket_server_handle,
        )
    }

    /// Start the websocket server
    pub fn run(websocket_server: Self) {
        spawn(websocket_server.receive_loop());
    }

    async fn receive_loop(mut self) {
        while let Some(command) = self.receiver.recv().await {
            match command {
                Command::Connect { user_id, new_conn } => {
                    debug!("Command::Connect from user_id: {user_id}");
                    self.connect(user_id, new_conn).await;
                }
                Command::Disconnect { conn } => {
                    debug!(
                        "Command::Disconnect from user_id: {} with the socket_id: {}",
                        conn.user_id, conn.socket_id
                    );
                    self.disconnect(conn).await;
                }
                Command::SocketEvent {
                    event: socket_event,
                    user_ids,
                } => {
                    debug!("Command::SocketEvent {:#?}", socket_event);
                    self.emit_socket_event(socket_event, &user_ids).await;
                }
                Command::WebRtcEvent {
                    event: webrtc_event,
                    sender_connection,
                } => {
                    debug!("Command::WebRtcEvent {:#?}", webrtc_event);
                    self.emit_webrtc_event(webrtc_event, sender_connection)
                        .await;
                }
                Command::GetIsConnected { user_id, sender } => {
                    debug!("Command::GetIsConnected for user_id: {user_id}");
                    self.is_user_online(user_id, sender);
                }
            }
        }
    }

    /// Save a new websocket connection and tie it to the user_id to know which connection is who
    async fn connect(&mut self, user_id: usize, mut new_conn: WebSocketConnection) {
        // Start ping-pong-ing
        let _ = new_conn.session.ping(&[]).await;

        // Save the new connection
        self.connections
            .entry(user_id)
            .and_modify(|set: &mut HashSet<WebSocketConnection>| {
                // add new socket session to existing hashset
                set.insert(new_conn.clone());
            })
            .or_insert_with(|| {
                // Create new entry with new hashset
                let mut new_hashset: HashSet<WebSocketConnection> = HashSet::new();
                new_hashset.insert(new_conn.clone());
                return new_hashset;
            });

        // Emit "user went online" event
        {
            let config = self.config.clone();
            let dbclient = self.dbclient.clone();
            let websocket_server_handle = self.server_handle.clone();

            let all_the_users_friends: Vec<usize> = dbclient
                .get_friends_as_user(user_id, None)
                .await
                .into_iter()
                .map(|u| u.id as usize)
                .collect();
            tokio::task::spawn(async move {
                let user_payload = dbclient
                    .get_user(user_id, None)
                    .await
                    .unwrap()
                    .into_client(config.domain(), websocket_server_handle.clone())
                    .await
                    .to_payload()
                    .unwrap();

                websocket_server_handle.emit_socket_events(&[
                    (
                        SocketEvent {
                            action: SocketAction::UserWentOnline,
                            payload: user_payload.clone(),
                            message: "You went online".to_string(),
                        },
                        &[user_id],
                    ),
                    (
                        SocketEvent {
                            action: SocketAction::UserWentOnline,
                            payload: user_payload.clone(),
                            message: "A friend of yours went online".to_string(),
                        },
                        &all_the_users_friends,
                    ),
                ]);
            });
        }
    }

    /// Remove a websocket connection
    async fn disconnect(&mut self, conn: WebSocketConnection) {
        // // Remove sockets connections from set
        self.connections.entry(conn.user_id).and_modify(
            |set: &mut HashSet<WebSocketConnection>| {
                set.remove(&conn);
            },
        );

        // Remove user_id from map when empty
        if let Some(connections_set) = self.connections.get(&conn.user_id) {
            if connections_set.is_empty() {
                self.connections.remove_entry(&conn.user_id);
            }
        }

        // Emit "user went offline" event
        {
            let config = self.config.clone();
            let dbclient = self.dbclient.clone();
            let websocket_server_handle = self.server_handle.clone();

            let all_the_users_friends: Vec<usize> = dbclient
                .get_friends_as_user(conn.user_id, None)
                .await
                .into_iter()
                .map(|u| u.id as usize)
                .collect();
            tokio::task::spawn(async move {
                let user_payload = dbclient
                    .get_user(conn.user_id, None)
                    .await
                    .unwrap()
                    .into_client(config.domain(), websocket_server_handle.clone())
                    .await
                    .to_payload()
                    .unwrap();

                websocket_server_handle.emit_socket_events(&[
                    (
                        SocketEvent {
                            action: SocketAction::UserWentOffline,
                            payload: user_payload.clone(),
                            message: "You went offline".to_string(),
                        },
                        &[conn.user_id],
                    ),
                    (
                        SocketEvent {
                            action: SocketAction::UserWentOffline,
                            payload: user_payload.clone(),
                            message: "A friend of yours went offline".to_string(),
                        },
                        &all_the_users_friends,
                    ),
                ]);
            });
        }
    }

    async fn emit_socket_event(&self, socket_event: SocketEvent, user_ids: &[usize]) {
        let socket_event_as_text: String = serde_json::to_string(&socket_event).unwrap();
        for user_id in user_ids {
            info!("Emit socket event to {}", user_id);
            for connection in self.get_connections_of_user_id(*user_id) {
                let _ = connection.clone().session.text(&socket_event_as_text).await;
            }
        }
    }

    async fn emit_webrtc_event(
        &self,
        webrtc_event: WebRtcEvent,
        sender_connection: WebSocketConnection,
    ) {
        // TODO make this more safe
        // if webrtc_event.sender_user_id == sender_connection.user_id {
        //     // error log and skip
        //     todo!()
        // }

        // update sender_socket_id if offer or answer
        let socket_id: String = sender_connection.socket_id.to_string();
        let webrtc_event = match &webrtc_event.action {
            WebRtcAction::SignalingOffer | WebRtcAction::SignalingAnswer => WebRtcEvent {
                sender_socket_id: Some(socket_id),
                ..webrtc_event.clone()
            },
            _ => webrtc_event,
        };
        let webrtc_event_as_text: String = serde_json::to_string(&webrtc_event).unwrap();

        match &webrtc_event.action {
            // Emit to all clients of user_id
            WebRtcAction::SignalingOffer => {
                for connection in self.get_connections_of_user_id(webrtc_event.receiver_user_id) {
                    let _ = connection.clone().session.text(&webrtc_event_as_text).await;
                }
            }
            // Emit to client with socket_id
            WebRtcAction::SignalingAnswer => {
                if let Some(receiver_socket_id) = &webrtc_event.receiver_socket_id {
                    let receiver_connection = self.get_connection_of_socket_id(
                        webrtc_event.receiver_user_id,
                        receiver_socket_id,
                    );
                    if let Some(connection) = receiver_connection {
                        let _ = connection.clone().session.text(webrtc_event_as_text).await;
                    }
                }
            }
            // Emit to client with socket_id
            _ => {
                let has_sender_and_receiver_socket_id: bool =
                    webrtc_event.sender_socket_id.is_some()
                        && webrtc_event.receiver_socket_id.is_some();

                if has_sender_and_receiver_socket_id {
                    let receiver_connection = self.get_connection_of_socket_id(
                        webrtc_event.receiver_user_id,
                        &webrtc_event.receiver_socket_id.unwrap(),
                    );
                    if let Some(connection) = receiver_connection {
                        let _ = connection.clone().session.text(webrtc_event_as_text).await;
                    }
                }
            }
        }
    }

    fn get_connections_of_user_id(&self, user_id: usize) -> Vec<WebSocketConnection> {
        let user_connections: Option<&HashSet<WebSocketConnection>> =
            self.connections.get(&user_id);
        if let Some(user_connections) = user_connections {
            return user_connections
                .clone()
                .into_iter()
                .collect::<Vec<WebSocketConnection>>();
        }
        Vec::new()
    }

    fn get_connection_of_socket_id(
        &self,
        user_id: usize,
        socket_id: &str,
    ) -> Option<WebSocketConnection> {
        for connection in self.get_connections_of_user_id(user_id) {
            if connection.socket_id.to_string() == socket_id {
                return Some(connection);
            }
        }
        None
    }

    /// used by DbUser.into_client
    fn is_user_online(&self, user_id: usize, sender: oneshot::Sender<bool>) {
        let _ = sender.send(self.connections.contains_key(&user_id));
    }
}

// ----------------------------------------------- Sender to WebSocket Server Instance
#[derive(Clone)]
pub struct WebSocketServerHandle {
    pub sender: UnboundedSender<Command>,
}

impl WebSocketServerHandle {
    /// Start the traditional client-server-socket connection
    /// The client takes over sending and receiving socket events
    pub async fn connect(
        self,
        user_id: usize,
        new_conn: WebSocketConnection,
        msg_stream: MessageStream,
    ) {
        // Save new connection in the server
        let _ = self.sender.send(Command::Connect {
            user_id,
            new_conn: new_conn.clone(),
        });

        self.socket_event_loop(new_conn, msg_stream).await;
    }

    /// Disconnect from the websocket server
    async fn disconnect(&self, conn: WebSocketConnection) {
        let _ = conn.session.clone().close(None).await;
        let _ = self.sender.send(Command::Disconnect { conn });
    }

    /// Start the socket event loop aka.
    /// Continiousely wait for messages from the client and act accordingely
    async fn socket_event_loop(self, mut conn: WebSocketConnection, mut msg_stream: MessageStream) {
        spawn_local(async move {
            loop {
                match timeout(Duration::from_secs(20), msg_stream.next()).await {
                    Ok(Some(Ok(msg))) => match msg {
                        Message::Pong(bytes) => {
                            sleep(Duration::from_secs(10)).await;
                            let _ = conn.session.ping(&bytes).await;
                        }
                        Message::Text(text) => self.handle_text_message(text, &conn),
                        _ => break,
                    },
                    Ok(Some(Err(_))) | Ok(None) => break,
                    Err(_) => {
                        debug!(
                            "No Pong received within 20 seconds. Disconnecting user_id {}...",
                            conn.user_id
                        );
                        break;
                    }
                }
            }
            self.disconnect(conn).await;
        });
    }

    pub fn handle_text_message(&self, text: impl Into<String>, conn: &WebSocketConnection) {
        let text: String = text.into();
        let webrtc_event = serde_json::from_str::<WebRtcEvent>(&text);
        if let Ok(webrtc_event) = webrtc_event {
            let _ = self.sender.send(Command::WebRtcEvent {
                event: webrtc_event,
                sender_connection: conn.clone(),
            });
        }
    }

    /// Send socket event to websocket server to emit to all clients of user_ids
    pub fn emit_socket_event(&self, socket_event: SocketEvent, user_ids: &[usize]) {
        // TODO maybe move this to the server side
        // Remove duplicates to prevent spamming a user
        let mut user_ids: Vec<usize> = user_ids.to_vec();
        user_ids.sort();
        user_ids.dedup();

        let _ = self.sender.send(Command::SocketEvent {
            event: socket_event,
            user_ids,
        });
    }

    pub fn emit_socket_events(&self, socket_events: &[(SocketEvent, &[usize])]) {
        socket_events
            .into_iter()
            .for_each(|(socket_event, user_ids)| {
                self.emit_socket_event(socket_event.clone(), user_ids);
            });
    }

    /// Get if a user is currently connected with a client
    /// used by DbUser.into_client
    pub async fn is_user_connected(&self, user_id: usize) -> bool {
        let (sender, receiver) = oneshot::channel();

        if let Err(e) = self
            .sender
            .send(Command::GetIsConnected { user_id, sender })
        {
            error!("{e}");
            return false;
        }

        match receiver.await {
            Ok(is_connected) => is_connected,
            Err(e) => {
                error!("{e}");
                false
            }
        }
    }
}

// ----------------------------------------------- Single Connection
#[derive(Clone)]
pub struct WebSocketConnection {
    pub user_id: usize,
    pub socket_id: Uuid,
    pub session: Session,
}

impl WebSocketConnection {
    pub fn new(user_id: usize, session: Session) -> Self {
        Self {
            user_id,
            socket_id: Uuid::new_v4(),
            session,
        }
    }
}

impl PartialEq for WebSocketConnection {
    fn eq(&self, other: &Self) -> bool {
        let socket_id_eq: bool = self.socket_id == other.socket_id;
        let user_id_eq: bool = self.user_id == other.user_id;
        socket_id_eq && user_id_eq
    }
}

impl Eq for WebSocketConnection {}

impl Hash for WebSocketConnection {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.socket_id.hash(state);
        self.user_id.hash(state);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::config::Config;
    use crate::models::ClientUser;
    use crate::scopes::users::GetUserResponse;
    use crate::server::Server;
    use crate::utils::test_utils;
    use crate::{app::VERSION, ws::events::SocketAction};
    use futures_util::SinkExt;
    use pretty_assertions::{assert_eq, assert_ne};
    use reqwest::Method;
    use sqlx::MySqlPool;
    use tokio::time::sleep;
    use tokio_tungstenite::tungstenite::protocol::Message;

    #[sqlx::test]
    async fn test_websocketserver_create_and_run(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let (server, _) = WebSocketServer::new(dbclient);
        WebSocketServer::run(server);
    }

    #[sqlx::test]
    fn test_websocket_server_connect_and_disconnect(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let port = test_utils::get_random_port();
        let server = Server::run_test(config, dbclient.clone(), port);

        sleep(Duration::from_secs(1)).await;

        let (mut write, _) = test_utils::create_ws_client(1, port).await;
        let _ = write.close().await;
        server.stop(false).await;
    }

    #[sqlx::test]
    fn test_websocket_server_once_connected_the_user_appears_online(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient.clone(), port);
        sleep(Duration::from_secs(1)).await;
        let (mut write, _) = test_utils::create_ws_client(1, port).await;

        let response: GetUserResponse = test_utils::do_http_request(
            1,
            Method::GET,
            format!("http://127.0.0.1:{port}/{VERSION}/users/1/"),
            None,
            None,
        )
        .await;

        let _ = write.close().await;
        server.stop(false).await;
        assert_eq!("on", response.result.status);
    }

    #[sqlx::test]
    fn test_websocket_server_once_disconnected_the_user_appears_offline(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient.clone(), port);
        sleep(Duration::from_secs(1)).await;
        let (mut write, _) = test_utils::create_ws_client(1, port).await;
        let _ = write.close().await;

        let response: GetUserResponse = test_utils::do_http_request(
            1,
            Method::GET,
            format!("http://127.0.0.1:{port}/{VERSION}/users/1/"),
            None,
            None,
        )
        .await;
        server.stop(false).await;
        assert_eq!("off", response.result.status);
    }

    #[sqlx::test]
    fn test_websocket_server_automatically_disconnects_user_after_20secs_of_no_pong(
        pool: MySqlPool,
    ) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient.clone(), port);
        sleep(Duration::from_secs(1)).await;
        let (mut _write, _) = test_utils::create_ws_client(1, port).await;

        drop(_write);
        sleep(Duration::from_secs(1)).await;

        let response: GetUserResponse = test_utils::do_http_request(
            1,
            Method::GET,
            format!("http://127.0.0.1:{port}/{VERSION}/users/1/"),
            None,
            None,
        )
        .await;
        assert_eq!("on", response.result.status);

        sleep(Duration::from_secs(21)).await;

        let response: GetUserResponse = test_utils::do_http_request(
            1,
            Method::GET,
            format!("http://127.0.0.1:{port}/{VERSION}/users/1/"),
            None,
            None,
        )
        .await;
        server.stop(false).await;
        assert_eq!("off", response.result.status);
    }

    #[sqlx::test]
    fn test_websocket_server_emits_socket_event_when_user_goes_online(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient.clone(), port);
        sleep(Duration::from_secs(1)).await;
        let (mut write, mut read) = test_utils::create_ws_client(1, port).await;

        let socket_event: SocketEvent = loop {
            match read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UserWentOnline {
                            break (socket_event);
                        }
                    }
                }
                _ => continue,
            }
        };
        let user: ClientUser = serde_json::from_str(&socket_event.payload).unwrap();

        let _ = write.close().await;
        server.stop(false).await;
        assert_eq!("on", user.status);
    }

    #[sqlx::test]
    fn test_websocket_server_emits_socket_event_when_user_goes_offline(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        let server = Server::run_test(config, dbclient.clone(), port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, _) = test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;
        let _ = client_one_write.close().await;

        let socket_event: SocketEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
                        if socket_event.action == SocketAction::UserWentOffline {
                            break (socket_event);
                        }
                    }
                }
                _ => continue,
            }
        };
        let user: ClientUser = serde_json::from_str(&socket_event.payload).unwrap();

        let _ = client_two_write.close().await;
        server.stop(false).await;
        assert_eq!("off", user.status);
    }

    #[sqlx::test]
    fn test_webrtc_send_offer(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient.clone(), port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, _client_one_read) = test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let offer = WebRtcEvent {
            action: WebRtcAction::SignalingOffer,
            payload: "this is an legit offer".into(),
            sender_user_id: 1,
            sender_socket_id: None,
            receiver_user_id: 2,
            receiver_socket_id: None,
        };

        let _ = client_one_write
            .send(Message::text(serde_json::to_string(&offer).unwrap()))
            .await;
        let received_offer: WebRtcEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::SignalingOffer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let _ = client_one_write.close().await;
        let _ = client_two_write.close().await;
        server.stop(false).await;

        assert_eq!(offer.action, received_offer.action);
        assert_eq!(offer.payload, received_offer.payload);
        assert_eq!(offer.sender_user_id, received_offer.sender_user_id);
        assert_ne!(offer.sender_socket_id, received_offer.sender_socket_id);
        assert_eq!(offer.receiver_user_id, received_offer.receiver_user_id);
        assert_eq!(offer.receiver_socket_id, received_offer.receiver_socket_id);
    }

    #[sqlx::test]
    fn test_webrtc_send_offer_emits_to_all_clients_of_user_id(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient.clone(), port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, _) = test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;
        let (mut client_three_write, mut client_three_read) =
            test_utils::create_ws_client(2, port).await;

        let offer = WebRtcEvent {
            action: WebRtcAction::SignalingOffer,
            payload: "this is an legit offer".into(),
            sender_user_id: 1,
            sender_socket_id: None,
            receiver_user_id: 2,
            receiver_socket_id: None,
        };

        let _ = client_one_write
            .send(Message::text(serde_json::to_string(&offer).unwrap()))
            .await;

        let _received_offer: WebRtcEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::SignalingOffer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };
        let _received_offer: WebRtcEvent = loop {
            match client_three_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::SignalingOffer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let _ = client_one_write.close().await;
        let _ = client_two_write.close().await;
        let _ = client_three_write.close().await;
        server.stop(false).await;
    }

    #[sqlx::test]
    fn test_webrtc_send_answer(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient.clone(), port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let offer = WebRtcEvent {
            action: WebRtcAction::SignalingOffer,
            payload: "this is an legit offer".into(),
            sender_user_id: 1,
            sender_socket_id: None,
            receiver_user_id: 2,
            receiver_socket_id: None,
        };
        let _ = client_one_write
            .send(Message::text(serde_json::to_string(&offer).unwrap()))
            .await;
        let received_offer: WebRtcEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::SignalingOffer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let answer = WebRtcEvent {
            action: WebRtcAction::SignalingAnswer,
            payload: "this is an legit answer".into(),
            sender_user_id: 2,
            sender_socket_id: None,
            receiver_user_id: 1,
            receiver_socket_id: received_offer.sender_socket_id,
        };
        let _ = client_two_write
            .send(Message::text(serde_json::to_string(&answer).unwrap()))
            .await;
        let received_answer: WebRtcEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::SignalingAnswer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let _ = client_one_write.close().await;
        let _ = client_two_write.close().await;
        server.stop(false).await;

        assert_eq!(answer.action, received_answer.action);
        assert_eq!(answer.payload, received_answer.payload);
        assert_eq!(answer.sender_user_id, received_answer.sender_user_id);
        assert_ne!(answer.sender_socket_id, received_answer.sender_socket_id);
        assert_eq!(answer.receiver_user_id, received_answer.receiver_user_id);
        assert_eq!(
            answer.receiver_socket_id,
            received_answer.receiver_socket_id
        );
    }

    #[sqlx::test]
    fn test_webrtc_send_ice(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient.clone(), port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let _ = client_one_write
            .send(Message::text(
                serde_json::to_string(&WebRtcEvent {
                    action: WebRtcAction::SignalingOffer,
                    payload: "this is an legit offer".into(),
                    sender_user_id: 1,
                    sender_socket_id: None,
                    receiver_user_id: 2,
                    receiver_socket_id: None,
                })
                .unwrap(),
            ))
            .await;

        let received_offer: WebRtcEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::SignalingOffer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let _ = client_two_write
            .send(Message::text(
                serde_json::to_string(&WebRtcEvent {
                    action: WebRtcAction::SignalingAnswer,
                    payload: "this is an legit answer".into(),
                    sender_user_id: 2,
                    sender_socket_id: None,
                    receiver_user_id: 1,
                    receiver_socket_id: received_offer.sender_socket_id.clone(),
                })
                .unwrap(),
            ))
            .await;

        let received_answer: WebRtcEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::SignalingAnswer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let client_one_socket_id: String = received_offer.sender_socket_id.unwrap();
        let client_two_socket_id: String = received_answer.sender_socket_id.unwrap();
        assert_ne!(client_one_socket_id, client_two_socket_id);

        let ice = WebRtcEvent {
            action: WebRtcAction::Ice,
            payload: "this is a legit ice candidate event".into(),
            sender_user_id: 1,
            sender_socket_id: Some(client_one_socket_id.clone()),
            receiver_user_id: 2,
            receiver_socket_id: Some(client_two_socket_id.clone()),
        };
        let _ = client_one_write
            .send(Message::text(serde_json::to_string(&ice).unwrap()))
            .await;

        let received_ice: WebRtcEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::Ice {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let _ = client_one_write.close().await;
        let _ = client_two_write.close().await;
        server.stop(false).await;

        assert_eq!(received_ice, ice);
    }

    #[sqlx::test]
    fn test_webrtc_send_renegotiate_offer(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient.clone(), port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let _ = client_one_write
            .send(Message::text(
                serde_json::to_string(&WebRtcEvent {
                    action: WebRtcAction::SignalingOffer,
                    payload: "this is an legit offer".into(),
                    sender_user_id: 1,
                    sender_socket_id: None,
                    receiver_user_id: 2,
                    receiver_socket_id: None,
                })
                .unwrap(),
            ))
            .await;
        let received_offer: WebRtcEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::SignalingOffer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let _ = client_two_write
            .send(Message::text(
                serde_json::to_string(&WebRtcEvent {
                    action: WebRtcAction::SignalingAnswer,
                    payload: "this is an legit answer".into(),
                    sender_user_id: 2,
                    sender_socket_id: None,
                    receiver_user_id: 1,
                    receiver_socket_id: received_offer.sender_socket_id.clone(),
                })
                .unwrap(),
            ))
            .await;
        let received_answer: WebRtcEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::SignalingAnswer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let client_one_socket_id: String = received_offer.sender_socket_id.unwrap();
        let client_two_socket_id: String = received_answer.sender_socket_id.unwrap();
        assert_ne!(client_one_socket_id, client_two_socket_id);

        let reoffer = WebRtcEvent {
            action: WebRtcAction::RenegotiateOffer,
            payload: "this is a legit renegotiate offer".into(),
            sender_user_id: 1,
            sender_socket_id: Some(client_one_socket_id.clone()),
            receiver_user_id: 2,
            receiver_socket_id: Some(client_two_socket_id.clone()),
        };
        let _ = client_one_write
            .send(Message::text(serde_json::to_string(&reoffer).unwrap()))
            .await;
        let received_reoffer: WebRtcEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::RenegotiateOffer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let _ = client_one_write.close().await;
        let _ = client_two_write.close().await;
        server.stop(false).await;

        assert_eq!(received_reoffer, reoffer);
    }

    #[sqlx::test]
    fn test_webrtc_send_renegotiate_answer(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let server = Server::run_test(config, dbclient.clone(), port);
        sleep(Duration::from_secs(1)).await;
        let (mut client_one_write, mut client_one_read) =
            test_utils::create_ws_client(1, port).await;
        let (mut client_two_write, mut client_two_read) =
            test_utils::create_ws_client(2, port).await;

        let _ = client_one_write
            .send(Message::text(
                serde_json::to_string(&WebRtcEvent {
                    action: WebRtcAction::SignalingOffer,
                    payload: "this is an legit offer".into(),
                    sender_user_id: 1,
                    sender_socket_id: None,
                    receiver_user_id: 2,
                    receiver_socket_id: None,
                })
                .unwrap(),
            ))
            .await;
        let received_offer: WebRtcEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::SignalingOffer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let _ = client_two_write
            .send(Message::text(
                serde_json::to_string(&WebRtcEvent {
                    action: WebRtcAction::SignalingAnswer,
                    payload: "this is an legit answer".into(),
                    sender_user_id: 2,
                    sender_socket_id: None,
                    receiver_user_id: 1,
                    receiver_socket_id: received_offer.sender_socket_id.clone(),
                })
                .unwrap(),
            ))
            .await;
        let received_answer: WebRtcEvent = loop {
            match client_one_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::SignalingAnswer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let client_one_socket_id: String = received_offer.sender_socket_id.unwrap();
        let client_two_socket_id: String = received_answer.sender_socket_id.unwrap();
        assert_ne!(client_one_socket_id, client_two_socket_id);

        let reanswer = WebRtcEvent {
            action: WebRtcAction::RenegotiateAnswer,
            payload: "this is a legit renegotiate answer".into(),
            sender_user_id: 1,
            sender_socket_id: Some(client_one_socket_id.clone()),
            receiver_user_id: 2,
            receiver_socket_id: Some(client_two_socket_id.clone()),
        };
        let _ = client_one_write
            .send(Message::text(serde_json::to_string(&reanswer).unwrap()))
            .await;
        let received_reanswer: WebRtcEvent = loop {
            match client_two_read.next().await {
                Some(Ok(Message::Text(msg))) => {
                    if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
                        if webrtc_event.action == WebRtcAction::RenegotiateAnswer {
                            break (webrtc_event);
                        }
                    }
                }
                _ => continue,
            }
        };

        let _ = client_one_write.close().await;
        let _ = client_two_write.close().await;
        server.stop(false).await;
        assert_eq!(received_reanswer, reanswer);
    }
}
