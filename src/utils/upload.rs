use crate::app::{AVATAR_DIRECTORY, POSTS_DIRECTORY};
use crate::error::{ErrorMessage, HttpError};
use crate::scopes::posts::PostUpload;
use crate::scopes::users::AvatarUpload;
use actix_web::web;
use image::{imageops::FilterType, DynamicImage};
use log::{debug, error};
use mime::{Mime, IMAGE_GIF, IMAGE_JPEG, IMAGE_PNG};

pub async fn save_avatar(form: AvatarUpload, user_id: usize) -> Result<(), HttpError> {
    let legal_mime_types: [Mime; 3] = [IMAGE_PNG, IMAGE_JPEG, IMAGE_GIF];
    form.file
        .content_type
        .ok_or(HttpError::server_error())
        .and_then(
            |content_type: Mime| match legal_mime_types.contains(&content_type) {
                true => Ok(()),
                false => {
                    debug!("File is not a legal mime type");
                    Err(HttpError::server_error())
                }
            },
        )?;

    web::block(move || {
        let file: DynamicImage = image::io::Reader::open(&form.file.file.path())
            .map_err(|e| {
                error!("{e}");
                HttpError::bad_request(ErrorMessage::UploadError)
            })?
            .with_guessed_format()
            .map_err(|e| {
                error!("{e}");
                HttpError::bad_request(ErrorMessage::UploadError)
            })?
            .decode()
            .map_err(|e| {
                error!("{e}");
                HttpError::bad_request(ErrorMessage::UploadError)
            })?;

        let path = AVATAR_DIRECTORY.as_path().join(&format!("{user_id}.jpeg"));

        file.resize_exact(200, 200, FilterType::Nearest)
            .save(path)
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?;
        debug!("Updated avatar of user {user_id}");
        Ok(())
    })
    .await
    .unwrap()
}

pub async fn save_posts(form: PostUpload, post_id: usize) -> Result<(), HttpError> {
    for (index, file) in form.files.into_iter().enumerate() {
        let legal_mime_types: [Mime; 3] = [IMAGE_PNG, IMAGE_JPEG, IMAGE_GIF];
        file.content_type
            .ok_or(HttpError::server_error())
            .and_then(
                |content_type: Mime| match legal_mime_types.contains(&content_type) {
                    true => Ok(()),
                    false => {
                        debug!("File is not a legal mime type");
                        Err(HttpError::server_error())
                    }
                },
            )?;

        web::block(move || {
            let file: DynamicImage = image::io::Reader::open(file.file.path())
                .map_err(|e| {
                    error!("{e}");
                    HttpError::bad_request(ErrorMessage::UploadError)
                })?
                .with_guessed_format()
                .map_err(|e| {
                    error!("{e}");
                    HttpError::bad_request(ErrorMessage::UploadError)
                })?
                .decode()
                .map_err(|e| {
                    error!("{e}");
                    HttpError::bad_request(ErrorMessage::UploadError)
                })?;

            let path = POSTS_DIRECTORY
                .as_path()
                .join(&format!("{post_id}_{index}.jpeg"));

            file.save(path).map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?;

            debug!("Saved post picture for post {post_id}");
            Ok(())
        })
        .await
        .unwrap()?;
    }
    Ok(())
}
