use crate::app::VERSION;
use crate::dbclient::DbClient;
use crate::extractors::authentication_token::Claims;
use futures_util::stream::{SplitSink, SplitStream};
use futures_util::StreamExt;
use jsonwebtoken::{encode, errors::Error as JwtError, EncodingKey, Header};
use reqwest::{ClientBuilder, Method, RequestBuilder};
use serde::{de::DeserializeOwned, Serialize};
use sqlx::mysql::MySqlPool;
use std::net::TcpListener;
use tokio::net::TcpStream;
use tokio_tungstenite::tungstenite::http::{header, Request};
use tokio_tungstenite::tungstenite::protocol::Message;
use tokio_tungstenite::{connect_async, MaybeTlsStream, WebSocketStream};

pub fn get_test_token(user_id: usize, secret: &str) -> String {
    let token: Result<String, JwtError> = encode(
        &Header::default(),
        &Claims::access(user_id),
        &EncodingKey::from_secret(secret.as_ref()),
    );
    token.unwrap()
}

pub fn get_expired_test_token(user_id: usize, secret: &str) -> String {
    let token: Result<String, JwtError> = encode(
        &Header::default(),
        &Claims::expired(user_id),
        &EncodingKey::from_secret(secret.as_ref()),
    );
    token.unwrap()
}

pub async fn init_test_users(pool: &MySqlPool) {
    let dbclient = DbClient::from_pool(pool.clone());
    dbclient
        .save_user("marc", "marc@mail.de", "1234asdf", None)
        .await
        .unwrap();
    dbclient
        .save_user("nico", "nico@mail.de", "1234asdf", None)
        .await
        .unwrap();
    dbclient
        .save_user("michelle", "michelle@mail.de", "1234asdf", None)
        .await
        .unwrap();
}

pub async fn init_test_friends(pool: &MySqlPool) {
    let dbclient = DbClient::from_pool(pool.clone());
    dbclient.save_friend(1, 2, None).await.unwrap();
}

pub async fn init_test_friendrequests(pool: &MySqlPool) {
    let dbclient = DbClient::from_pool(pool.clone());
    dbclient.save_friendrequest(1, 2, None).await.unwrap();
}

pub async fn init_test_refresh_tokens(pool: &MySqlPool) {
    let dbclient = DbClient::from_pool(pool.clone());
    dbclient
        .save_refresh_token(1, "super_secret_refresh_token", None)
        .await
        .unwrap();
}

pub async fn init_test_messages(pool: &MySqlPool) {
    let dbclient = DbClient::from_pool(pool.clone());
    dbclient
        .save_message(1, 2, "Hi what's up?", None)
        .await
        .unwrap();
    dbclient
        .save_message(2, 1, "Nothing, how about you?", None)
        .await
        .unwrap();
    dbclient
        .save_message(1, 2, "I'm trapped in a db!", None)
        .await
        .unwrap();
}

pub async fn init_test_posts(pool: &MySqlPool) {
    let dbclient = DbClient::from_pool(pool.clone());
    dbclient.save_post(1, "First post!", 0, None).await.unwrap();
    dbclient
        .save_post(2, "Second post!", 0, None)
        .await
        .unwrap();
    dbclient.save_post(1, "Third post!", 0, None).await.unwrap();
    dbclient
        .save_post(3, "Fourth post!", 0, None)
        .await
        .unwrap();
}

pub async fn init_test_post_comments(pool: &MySqlPool) {
    let dbclient = DbClient::from_pool(pool.clone());
    dbclient.save_post_comment(1, 2, "lol", None).await.unwrap();
    dbclient
        .save_post_comment(1, 1, "what?", None)
        .await
        .unwrap();
    dbclient
        .save_post_comment(1, 2, "nice pics of your cat", None)
        .await
        .unwrap();
    dbclient
        .save_post_comment(2, 1, "hahah", None)
        .await
        .unwrap();
}

pub async fn init_test_post_likes(pool: &MySqlPool) {
    let dbclient = DbClient::from_pool(pool.clone());
    dbclient.like_post(1, 1, None).await.unwrap();
    dbclient.like_post(1, 2, None).await.unwrap();
    dbclient.like_post(2, 1, None).await.unwrap();
    dbclient.like_post(2, 2, None).await.unwrap();
}

pub async fn init_test_blocked_users(pool: &MySqlPool) {
    let dbclient = DbClient::from_pool(pool.clone());
    dbclient.block_user(1, 2, None).await.unwrap();
}

pub fn get_random_port() -> u16 {
    let listener = TcpListener::bind("127.0.0.1:0").unwrap();
    let local_addr = listener.local_addr().unwrap();
    let port = local_addr.port();
    port
}

pub async fn create_ws_client(
    user_id: usize,
    port: u16,
) -> (
    SplitSink<WebSocketStream<MaybeTlsStream<TcpStream>>, Message>,
    SplitStream<WebSocketStream<MaybeTlsStream<TcpStream>>>,
) {
    let request = Request::builder()
        .uri(format!("ws://127.0.0.1:{port}/{VERSION}/ws/"))
        .header(header::HOST, format!("127.0.0.1:{port}"))
        .header(
            header::AUTHORIZATION,
            &format!("Bearer {}", get_test_token(user_id, "shhh!")),
        )
        .header(header::UPGRADE, "websocket")
        .header(header::CONNECTION, "Upgrade")
        .header(header::SEC_WEBSOCKET_KEY, "pDTDUR/eEFpXHb847ddH1g==")
        .header(header::SEC_WEBSOCKET_VERSION, "13")
        .body(())
        .unwrap();

    // Connect to the WebSocket server
    let (ws_stream, _) = connect_async(request).await.expect("Failed to connect");
    let (write, read) = ws_stream.split();

    (write, read)
}

pub async fn do_http_request<T: Serialize + DeserializeOwned>(
    user_id: usize,
    method: Method,
    route: impl Into<String>,
    body: Option<serde_json::Value>,
    query: Option<Vec<(String, String)>>,
) -> T {
    let route: String = route.into();
    let mut request: RequestBuilder = {
        let client = ClientBuilder::new().build().unwrap();
        match method {
            Method::GET => client.get(route),
            Method::POST => client.post(route),
            Method::PATCH => client.patch(route),
            Method::DELETE => client.delete(route),
            _ => panic!("Unsupported HTTP Method: {}", method),
        }
    };
    request = request.bearer_auth(get_test_token(user_id, "shhh!"));

    if let Some(body) = body {
        request = request.json(&body);
    }
    if let Some(query) = query {
        request = request.json(&query);
    }

    let body = request.send().await.unwrap().bytes().await.unwrap();
    eprintln!("do_http_request::body -> {:?}", body);
    let body = body.to_vec();
    eprintln!("do_http_request::body -> {:?}", body);
    let body: String = String::from_utf8(body).unwrap();
    eprintln!("do_http_request::body -> {:?}", body);
    serde_json::from_str::<T>(&body).unwrap()
}

pub async fn do_multipart_request<T: Serialize + DeserializeOwned>(
    user_id: usize,
    method: Method,
    route: impl Into<String>,
    form: reqwest::multipart::Form,
) -> T {
    let route: String = route.into();
    let mut request: RequestBuilder = {
        let client = ClientBuilder::new().build().unwrap();
        match method {
            Method::GET => client.get(route),
            Method::POST => client.post(route),
            Method::PATCH => client.patch(route),
            Method::DELETE => client.delete(route),
            _ => panic!("Unsupported HTTP Method: {}", method),
        }
    };
    request = request.bearer_auth(get_test_token(user_id, "shhh!"));
    request = request.multipart(form);

    let body = request.send().await.unwrap().bytes().await.unwrap();
    eprintln!("do_http_request::body -> {:?}", body);
    let body = body.to_vec();
    eprintln!("do_http_request::body -> {:?}", body);
    let body: String = String::from_utf8(body).unwrap();
    eprintln!("do_http_request::body -> {:?}", body);
    serde_json::from_str::<T>(&body).unwrap()
}
