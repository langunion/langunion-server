use bcrypt;

pub fn compare(password: impl Into<String>, hashed: impl Into<String>) -> bool {
    let password: String = password.into();
    let hashed: String = hashed.into();
    let compared: bcrypt::BcryptResult<bool> = bcrypt::verify::<&str>(&password, &hashed);
    match compared {
        Ok(t) => t,
        Err(_) => false,
    }
}

pub fn hash(password: impl Into<String>) -> String {
    bcrypt::hash(password.into(), 10).unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_compare_hashed_password_should_return_true() {
        let password: String = "1234asdf".to_string();
        let hashed_password: String = hash(&password);
        assert_eq!(compare(&password, &hashed_password), true);
    }

    #[test]
    fn test_compare_hashed_passwords_should_return_false() {
        let hashed_password: String = hash("asdf1234");
        assert_eq!(compare("1234asdf", &hashed_password), false);
    }
}
