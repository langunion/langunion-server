use crate::app::{AVATAR_DIRECTORY, POSTS_DIRECTORY};
use crate::error::HttpError;
use crate::models::DbPost;
use actix_web::web;
use log::error;
use regex::Regex;
use std::fs;
use std::path::PathBuf;

pub async fn delete_avatar(user_id: usize) -> Result<(), HttpError> {
    web::block(move || {
        let avatar_path = AVATAR_DIRECTORY.as_path().join(format!("{user_id}.jpeg"));

        if avatar_path.exists() {
            fs::remove_file(avatar_path).map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?;
        }
        Ok(())
    })
    .await
    .unwrap()
}

pub async fn delete_post_pics(posts: Vec<DbPost>) -> Result<(), HttpError> {
    let post_ids: Vec<usize> = posts
        .into_iter()
        .map(|post: DbPost| post.id as usize)
        .collect();

    web::block(move || {
        let dir: Vec<PathBuf> = fs::read_dir(POSTS_DIRECTORY.as_path())
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .map(|entry| match entry {
                Ok(entry) => entry.path(),
                Err(_) => PathBuf::new(),
            })
            .collect();

        for post_id in post_ids {
            for path in dir.clone() {
                let regex = Regex::new(&format!("{post_id}_\\d+\\.jpeg")).unwrap();
                if regex.is_match(path.to_str().unwrap_or("")) {
                    if let Err(e) = fs::remove_file(path) {
                        error!("{e}");
                    }
                }
            }
        }
        Ok(())
    })
    .await
    .unwrap()
}
