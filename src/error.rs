use actix_web::http::StatusCode;
use actix_web::{body::BoxBody, HttpResponse, ResponseError};
use log::error;
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::fmt;
use std::string::ToString;

// pub static PASSWORD_VALIDATION_ERROR: &str = "Username has to be at least 4 to 20 characters long and does not contain any spaces";
// pub const USERNAME_VALIDATION_ERROR: &str = "Has to be email";
// pub const EMAIL_VALIDATION_ERROR: &str = "Password has to be at least 8 characters long";

#[derive(Serialize, Deserialize)]
pub struct HttpErrorResponse {
    pub status: u16,
    pub message: String,
}

pub enum ErrorMessage {
    // Generic
    ServerError,
    Unauthorized,
    CouldntConnectToSocketServer,
    MissingAuthToken,
    ExpiredAuthToken,
    // Generic - File Upload
    MissingPicture,
    MissingPictures,
    MissingFile,
    MissingFiles,
    UploadError,
    WrongFileType,
    PayloadTooLarge,
    // Users
    UsernameExist,
    EmailExist,
    WrongCredentials,
    WrongPassword,
    WrongEmail,
    NoUserFound,
    EmailIsTaken,
    NewEmailIsTheSame,
    NewUsernameIsTheSame,
    // Tokens
    InvalidToken,
    // Friends
    NoFriendFound,
    // Friendrequests
    NoFriendrequestFound,
    NotYourFriendrequest,
    AlreadyPendingFriendrequest,
    AlreadyFriends,
    CantAcceptFriendrequest,
    CantDeclineFriendrequest,
    CantDeleteFriendrequest,
    CantSendYourselfFriendrequest,
    // Blocked Users
    YouHaveBlockedThisUser,
    UserIsntBlocked,
    UserAlreadyBlocked,
    // Messages
    MessageHasNoContent,
    NoMessageFound,
    NotYourMessage,
    CannotDeleteMessage,
    // Posts
    PostTooLong,
    PostCommentTooLong,
    CannotLikePostTwice,
    CannotUnlikePostTwice,
    EmptyPost,
    NoPostFound,
    NoPostCommentFound,
    NoPostPicFound,
    CannotViewThisPost,
    CannotLikeThisPost,
    CannotUnlikeThisPost,
    NotYourPost,
    CannotCommentOnThisPost,
    CannotDeletePostComment,
}

impl ToString for ErrorMessage {
    fn to_string(&self) -> String {
        self.to_str().to_string()
    }
}

impl Into<String> for ErrorMessage {
    fn into(self) -> String {
        self.to_string()
    }
}

impl ErrorMessage {
    fn to_str(&self) -> &str {
        match self {
            // Generic
            ErrorMessage::ServerError => "Server Error. Please try again later.",
            ErrorMessage::Unauthorized => {
                "You are not authorized to do this action. Please try to login with a valid user."
            }
            ErrorMessage::CouldntConnectToSocketServer => "Could not connect to socket server",
            ErrorMessage::MissingAuthToken => "Missing authentication token.",
            ErrorMessage::ExpiredAuthToken => "Authentication token is expired.",
            // Generic - File Upload
            ErrorMessage::MissingPicture => "No picture to upload found.",
            ErrorMessage::MissingPictures => "No pictures to upload found.",
            ErrorMessage::MissingFile => "No picture to upload found.",
            ErrorMessage::MissingFiles => "No pictures to upload found.",
            ErrorMessage::UploadError => "The file you've uploaded seems to be corrupted.",
            ErrorMessage::WrongFileType => "The uploaded file is in a wrong format.",
            ErrorMessage::PayloadTooLarge => "Paylod is too large.",
            // Users
            ErrorMessage::UsernameExist => "An User with this username already exists.",
            ErrorMessage::EmailExist => "An User with this email already exists.",
            ErrorMessage::WrongCredentials => "Email or password is wrong.",
            ErrorMessage::WrongPassword => "Password is wrong.",
            ErrorMessage::WrongEmail => "Email is wrong.",
            ErrorMessage::NoUserFound => "No user has been found.",
            ErrorMessage::EmailIsTaken => "This is email is already taken by another user.",
            ErrorMessage::NewEmailIsTheSame => "New email can't be the same as the old one.",
            ErrorMessage::NewUsernameIsTheSame => "New username can't be the same as the old one.",
            // Tokens
            ErrorMessage::InvalidToken => "Authentication token is invalid or expired.",
            // Friends
            ErrorMessage::NoFriendFound => "You are not friends with this user.",
            // Friendrequests
            ErrorMessage::NoFriendrequestFound => "No pending friendrequest was found.",
            ErrorMessage::NotYourFriendrequest => "You are not allowed to view this friendrequest.",
            ErrorMessage::AlreadyPendingFriendrequest => {
                "There is already a pending friendrequest with this user."
            }
            ErrorMessage::AlreadyFriends => "You are already friends with this user.",
            ErrorMessage::CantAcceptFriendrequest => "You cannot accept this friendrequest.",
            ErrorMessage::CantDeclineFriendrequest => "You cannot decline this friendrequest.",
            ErrorMessage::CantDeleteFriendrequest => "You cannot delete this friendrequest.",
            ErrorMessage::CantSendYourselfFriendrequest => {
                "You cannot send yourself a friendrequest."
            }
            // Blocked Users
            ErrorMessage::YouHaveBlockedThisUser => "You have blocked this user.",
            ErrorMessage::UserIsntBlocked => "This user isn't blocked.",
            ErrorMessage::UserAlreadyBlocked => "You have already blocked this user.",
            // Messages
            ErrorMessage::MessageHasNoContent => "The content of a message cannot be empty.",
            ErrorMessage::NoMessageFound => "No message was found.",
            ErrorMessage::NotYourMessage => "This message does not belong to you.",
            ErrorMessage::CannotDeleteMessage => "You cannot delete this message.",
            // Posts
            ErrorMessage::PostTooLong => "A post is limited to only 255 characters.",
            ErrorMessage::PostCommentTooLong => {
                "A comment to a post is limited to only 255 characters."
            }
            ErrorMessage::CannotLikePostTwice => "You have already liked this post.",
            ErrorMessage::CannotUnlikePostTwice => {
                "You cannot unlike a post that you did not like before."
            }
            ErrorMessage::EmptyPost => "Cannot post a empty post.",
            ErrorMessage::NoPostFound => "The post doesn't exist.",
            ErrorMessage::NoPostCommentFound => "The comment doesn't exist.",
            ErrorMessage::NoPostPicFound => "No picture found.",
            ErrorMessage::CannotViewThisPost => "You aren't allowed to view this post.",
            ErrorMessage::CannotLikeThisPost => "You aren't allowed to like this post.",
            ErrorMessage::CannotUnlikeThisPost => "You aren't allowed to unlike this post.",
            ErrorMessage::NotYourPost => "This post doesn't belong to you.",
            ErrorMessage::CannotCommentOnThisPost => "You cannot post a comment to this post.",
            ErrorMessage::CannotDeletePostComment => "You cannot delete this comment.",
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct HttpError {
    pub status: u16,
    pub message: String,
}

impl HttpError {
    pub fn new(message: impl Into<String>, status: u16) -> Self {
        HttpError {
            message: message.into(),
            status,
        }
    }

    pub fn server_error() -> Self {
        HttpError {
            message: ErrorMessage::ServerError.to_string(),
            status: 500,
        }
    }

    pub fn bad_request(message: impl Into<String>) -> Self {
        HttpError {
            message: message.into(),
            status: 400,
        }
    }

    pub fn unauthorized(message: impl Into<String>) -> Self {
        HttpError {
            message: message.into(),
            status: 401,
        }
    }

    pub fn wrong_credentials() -> Self {
        Self::bad_request(ErrorMessage::WrongCredentials)
    }

    pub fn not_found() -> Self {
        HttpError {
            message: "Not found".into(),
            status: 404,
        }
    }

    pub fn into_http_response(self) -> HttpResponse {
        match self.status {
            400 => HttpResponse::BadRequest().json(HttpErrorResponse {
                status: 400,
                message: self.message.into(),
            }),
            401 => HttpResponse::Unauthorized().json(HttpErrorResponse {
                status: 401,
                message: self.message.into(),
            }),
            404 => HttpResponse::NotFound().json(HttpErrorResponse {
                status: 404,
                message: self.message.into(),
            }),
            413 => HttpResponse::PayloadTooLarge().json(HttpErrorResponse {
                status: 413,
                message: self.message.into(),
            }),
            500 => HttpResponse::InternalServerError().json(HttpErrorResponse {
                status: 500,
                message: self.message.into(),
            }),
            _ => {
                error!(
                    "Missing pattern match. Converted http status code {} to 500.",
                    self.status
                );
                HttpResponse::InternalServerError().json(HttpErrorResponse {
                    status: 500,
                    message: ErrorMessage::ServerError.into(),
                })
            }
        }
    }
}

impl fmt::Display for HttpError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "HttpError: message: {}, status: {}",
            self.message, self.status
        )
    }
}

impl Error for HttpError {}

impl ResponseError for HttpError {
    fn error_response(&self) -> HttpResponse<BoxBody> {
        let cloned = self.clone();
        sentry::capture_error(&cloned);
        cloned.into_http_response()
    }

    fn status_code(&self) -> StatusCode {
        StatusCode::from_u16(self.status).unwrap_or(StatusCode::INTERNAL_SERVER_ERROR)
    }
}
