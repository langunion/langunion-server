use crate::app::{APPNAME, LANGUNION_DIRECTORY, VERSION};
use log::{debug, error, warn};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::env;
use std::fs;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::sync::LazyLock;
use toml;
use uuid::Uuid;

pub static CONFIG: LazyLock<Config> = LazyLock::new(|| {
    debug!("Created Config struct");
    #[cfg(not(test))]
    let config = Config::new();
    #[cfg(test)]
    let config = Config::new_test();
    config
});

pub fn use_config() -> Config {
    CONFIG.clone()
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ConfigToml {
    general: Option<ConfigTomlGeneral>,
    database: Option<ConfigTomlDatabase>,
    jwt: Option<ConfigTomlJwt>,
    sentry: Option<ConfigTomlSentry>,
}

impl Default for ConfigToml {
    fn default() -> Self {
        Self {
            general: None,
            database: None,
            jwt: None,
            sentry: None,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct ConfigTomlGeneral {
    domain: Option<String>,
    port: Option<usize>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct ConfigTomlDatabase {
    host: Option<String>,
    port: Option<usize>,
    user: Option<String>,
    password: Option<String>,
    database_name: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct ConfigTomlJwt {
    secret_token: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct ConfigTomlSentry {
    api_key: Option<String>,
}

#[derive(Debug, Clone)]
pub struct Config {
    pub general_domain: String,
    pub general_port: usize,
    pub database_user: String,
    pub database_password: String,
    pub database_host: String,
    pub database_port: usize,
    pub database_database_name: String,
    pub jwt_secret_token: String,
    pub sentry_api_key: String,
}

impl Config {
    /// Read config file and create a new instance of Config
    pub fn new() -> Self {
        crate::app::App::create_langunion_directory();
        let env_pre_key: String = APPNAME.to_uppercase();
        let config_toml: ConfigToml = Self::get_config();
        let default: Config = Self::default();

        let (general_domain, general_port): (String, usize) = match config_toml.general {
            Some(general) => {
                let domain: String = Self::get_val(
                    format!("{env_pre_key}_GENERAL_DOMAIN"),
                    general.domain,
                    default.general_domain,
                );
                let port: usize = Self::get_val(
                    format!("{env_pre_key}_GENERAL_PORT"),
                    general.port,
                    default.general_port,
                );
                (domain, port)
            }
            None => (default.general_domain, default.general_port),
        };

        let (
            database_user,
            database_password,
            database_host,
            database_port,
            database_database_name,
        ) = match config_toml.database {
            Some(database) => {
                let user = Self::get_val(
                    format!("{env_pre_key}_DATABASE_USER"),
                    database.user,
                    default.database_user,
                );
                let password = Self::get_val(
                    format!("{env_pre_key}_DATABASE_PASSWORD"),
                    database.password,
                    default.database_password,
                );
                let host = Self::get_val(
                    format!("{env_pre_key}_DATABASE_HOST"),
                    database.host,
                    default.database_host,
                );
                let port = Self::get_val(
                    format!("{env_pre_key}_DATABASE_PORT"),
                    database.port,
                    default.database_port,
                );
                let db_name = Self::get_val(
                    format!("{env_pre_key}_DATABASE_DATABASE_NAME"),
                    database.database_name,
                    default.database_database_name,
                );
                (user, password, host, port, db_name)
            }
            None => (
                default.database_user,
                default.database_password,
                default.database_host,
                default.database_port,
                default.database_database_name,
            ),
        };

        let jwt_secret_token: String = match config_toml.jwt {
            Some(jwt) => Self::get_val(
                format!("{env_pre_key}_JWT_SECRET_TOKEN"),
                jwt.secret_token,
                default.jwt_secret_token,
            ),
            None => default.jwt_secret_token,
        };

        let sentry_api_key: String = match config_toml.sentry {
            Some(sentry) => Self::get_val(
                format!("{env_pre_key}_SENTRY_API_KEY"),
                sentry.api_key,
                default.sentry_api_key.clone(),
            ),
            None => default.sentry_api_key,
        };

        let config = Config {
            general_domain,
            general_port,
            database_user,
            database_password,
            database_host,
            database_port,
            database_database_name,
            jwt_secret_token,
            sentry_api_key,
        };
        debug!("{:#?}", config);
        config
    }

    /// Create a test config and the test upload directory
    #[cfg(test)]
    pub fn new_test() -> Self {
        crate::app::App::create_langunion_directory();
        let config = Self {
            jwt_secret_token: "shhh!".to_string(),
            ..Default::default()
        };
        config
    }

    /// Read the config file in one of the many directories it can be stored
    /// Convert the read (raw) string of the config file and convert it into ConfigToml
    fn get_config() -> ConfigToml {
        let config_file: String = {
            let paths: [PathBuf; 3] = [
                // "./{APPNAME}.toml"
                Path::new(".").join(&format!("{APPNAME}.toml")),
                // "~/.{APPNAME}/config.toml"
                LANGUNION_DIRECTORY.as_path().join("config.toml"),
                // "~/.config/{APPNAME}/config.toml"
                Path::new(&dirs::home_dir().unwrap())
                    .join(".config")
                    .join(APPNAME)
                    .join("config.toml"),
            ];

            let mut config_file: Option<String> = None;
            for path in paths {
                if let Ok(content) = fs::read_to_string(&path) {
                    debug!("Config file read from {:?}", path);
                    config_file = Some(Self::untildify(&content));
                    break;
                }
            }

            match config_file {
                Some(config_file) => config_file,
                None => {
                    warn!("No config file was found");
                    String::new()
                }
            }
        };

        toml::from_str(&config_file).unwrap_or_else(|e| {
            error!("Error while parsing config toml file");
            error!("{e}");
            error!("Starting server with default configuration!");
            ConfigToml::default()
        })
    }

    /// Replace ~/ with the path of the home dir
    fn untildify(content: impl Into<String>) -> String {
        let content: String = content.into();
        let regex = Regex::new("=(\\s*)?(\"|')~\\/").unwrap();
        let home: String = dirs::home_dir().unwrap().to_str().unwrap().to_string();
        let new: String = format!("=\"{}/", &home);
        regex.replace_all(&content, new).to_owned().to_string()
    }

    /// Get value from env or config or use default
    fn get_val<T: FromStr + std::fmt::Display + std::fmt::Debug>(
        env_key: impl Into<String>,
        config_part: Option<T>,
        default: T,
    ) -> T {
        let env_key: String = env_key.into();
        debug!(
            "env_key: {env_key}, config_part: {:?}, default: {default}",
            config_part
        );

        env::var(&env_key)
            .ok()
            // Get the env value
            .and_then(|val| match val.is_empty() {
                true => None,
                false => Some(val),
            })
            .and_then(|val| {
                Some(val.parse::<T>().ok().expect(&format!(
                    "Error while parsing environment variable {env_key}"
                )))
            })
            // Get the config or default value
            .unwrap_or_else(|| {
                config_part.unwrap_or_else(|| {
                    warn!("{env_key} is not set. Default value is being used: {default}");
                    default
                })
            })
    }

    /// Get the URL to the database
    pub fn database_url(&self) -> String {
        format!(
            "mysql://{}:{}@{}:{}/{}",
            self.database_user,
            self.database_password,
            self.database_host,
            self.database_port,
            self.database_database_name,
        )
    }

    /// Get the public domain used for hyperlinking resources
    pub fn domain(&self) -> String {
        format!("{}/{VERSION}", self.general_domain,)
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            general_domain: "http://localhost:4000".to_string(),
            general_port: 4000,
            database_user: "".to_string(),
            database_password: "".to_string(),
            database_host: "localhost".to_string(),
            database_port: 3306,
            database_database_name: "languniondb".to_string(),
            jwt_secret_token: Uuid::new_v4().to_string(),
            sentry_api_key: "".to_string(),
        }
    }
}
