use log::info;
use simplelog::{
    format_description, ColorChoice, CombinedLogger, ConfigBuilder, LevelFilter, TermLogger,
    TerminalMode,
};
use std::env;
use std::fs;
use std::path::{Path, PathBuf};
use std::sync::LazyLock;

pub static APPNAME: &str = "langunion";
pub static VERSION: &str = "v1";
pub static LANGUNION_DIRECTORY: LazyLock<PathBuf> = LazyLock::new(|| {
    #[cfg(not(test))]
    return PathBuf::new()
        .join(&dirs::home_dir().unwrap())
        .join(&format!(".{APPNAME}"));

    #[cfg(test)]
    return PathBuf::new()
        .join(".")
        .join(".test_langunion")
        .join(uuid::Uuid::new_v4().to_string());
});
pub static POSTS_DIRECTORY: LazyLock<PathBuf> = LazyLock::new(|| {
    PathBuf::new()
        .join(LANGUNION_DIRECTORY.as_path())
        .join("files")
        .join("posts")
});
pub static AVATAR_DIRECTORY: LazyLock<PathBuf> = LazyLock::new(|| {
    PathBuf::new()
        .join(LANGUNION_DIRECTORY.as_path())
        .join("files")
        .join("avatar")
});

pub static SNAPSHOTS_DIRECTORY: LazyLock<PathBuf> = LazyLock::new(|| {
    PathBuf::new()
        .join(LANGUNION_DIRECTORY.as_path())
        .join("snapshots")
});

pub struct App {}

impl App {
    /// Create the directory structure of .langunion/
    pub fn create_langunion_directory() {
        if !LANGUNION_DIRECTORY.exists() {
            fs::create_dir_all(LANGUNION_DIRECTORY.as_path())
                .expect("Could not create langunion directory");
            info!("Langunion directory created");
        }

        let snapshots_directory = Path::new(&LANGUNION_DIRECTORY.as_path()).join("snapshots");
        if !snapshots_directory.exists() {
            fs::create_dir_all(&snapshots_directory).expect("Could not create snapshots directory");
            info!("Snapshots directory created");
        }

        let upload_directory = Path::new(&LANGUNION_DIRECTORY.as_path()).join("files");
        if !upload_directory.exists() {
            fs::create_dir_all(&upload_directory).expect("Could not create upload directory");
            info!("Upload directory created");
        }

        let avatar_directory = Path::new(&upload_directory).join("avatar");
        if !avatar_directory.exists() {
            fs::create_dir_all(&avatar_directory).expect("Could not create avatar directory");
            info!("Avatar directory created");
        }

        let posts_directory = Path::new(&upload_directory).join("posts");
        if !posts_directory.exists() {
            fs::create_dir_all(&posts_directory).expect("Could not create posts directory");
            info!("Posts directory created");
        }
    }

    pub fn init_logger() {
        let config = ConfigBuilder::new()
            .set_time_format_custom(format_description!(
                version = 2,
                "\\[[year]-[month]-[day] [hour]:[minute]:[second]\\]"
            ))
            .set_thread_level(LevelFilter::Off)
            .build();

        let log_level = Self::log_level();

        CombinedLogger::init(vec![
            TermLogger::new(
                log_level,
                config.clone(),
                TerminalMode::Mixed,
                ColorChoice::Auto,
            ),
            #[cfg(not(debug_assertions))]
            simplelog::WriteLogger::new(
                log_level,
                config.clone(),
                std::fs::File::create(LANGUNION_DIRECTORY.as_path().join("langunion-server.log"))
                    .unwrap(),
            ),
        ])
        .unwrap();
    }

    /// Try to get the log level from env var 'LOG_LEVEL' or use default
    fn log_level() -> LevelFilter {
        #[cfg(debug_assertions)]
        let default = LevelFilter::Debug;
        #[cfg(not(debug_assertions))]
        let default = LevelFilter::Info;
        let log_level: String = env::var("LOG_LEVEL").unwrap_or(String::new());
        log_level.parse::<LevelFilter>().unwrap_or(default)
    }
}
