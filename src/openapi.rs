use crate::app::VERSION;
use crate::models::{
    ClientFriendrequest, ClientMessage, ClientPost, ClientPostComment, ClientUser,
};
use crate::scopes::blocked_users::{
    BlockUserBody, BlockUserResponse, GetBlockedUserResponse, UnblockUserResponse,
};
use crate::scopes::friendrequests::{
    AcceptFriendrequestResponse, DeleteFriendrequestResponse, GetFriendrequestResponse,
    SendFriendrequestBody, SendFriendrequestResponse,
};
use crate::scopes::friends::{GetFriendResponse, RemoveFriendResponse};
use crate::scopes::messages::{
    DeleteChatResponse, DeleteMessageResponse, GetMessageResponse, SendMessageBody,
    SendMessageResponse, UpdateMessageBody, UpdateMessageResponse,
};
use crate::scopes::posts::{
    DeleteCommentResponse, DeletePostResponse, GetPostResponse, LikePostResponse, PostCommentBody,
    PostCommentResponse, PostUpload, SavePostResponse, UnlikePostResponse,
};
use crate::scopes::tokens::{
    CreateTokensBody, CreateTokensResponse, RefreshTokensBody, RefreshTokensResponse,
    ResetTokensResponse, ValidateTokenResponse
};
use crate::scopes::users::{
    AvatarUpload, DeleteUserBody, DeleteUserResponse, GetMeResponse, GetUserResponse,
    RegisterUserBody, RegisterUserResponse, UpdateAvatarResponse, UpdateUserBody,
    UpdateUserResponse,
};
use utoipa::openapi::security::{ApiKey, ApiKeyValue, HttpAuthScheme, HttpBuilder, SecurityScheme};
use utoipa::openapi::{Components, Server};
use utoipa::ToResponse;
use utoipa::{Modify, OpenApi};
use utoipa_swagger_ui::SwaggerUi;

// TODO wait until utoipa fixes this workaround with generics
// https://github.com/juhaku/utoipa/issues/835
#[derive(ToResponse)]
#[response(description = "Successfully got a list of users")]
pub struct PaginatedResponseClientUser {
    pub count: usize,
    pub next: Option<String>,
    pub previous: Option<String>,
    pub result: Vec<ClientUser>,
}

#[derive(ToResponse)]
#[response(description = "Successfully got a list of all your pending friendrequests")]
pub struct PaginatedResponseClientFriendrequest {
    pub count: usize,
    pub next: Option<String>,
    pub previous: Option<String>,
    pub result: Vec<ClientFriendrequest>,
}

#[derive(ToResponse)]
#[response(description = "Successfully got a list of all your messages")]
pub struct PaginatedResponseClientMessage {
    pub count: usize,
    pub next: Option<String>,
    pub previous: Option<String>,
    pub result: Vec<ClientMessage>,
}

#[derive(ToResponse)]
#[response(description = "Successfully got a list of all your posts")]
pub struct PaginatedResponseClientPost {
    pub count: usize,
    pub next: Option<String>,
    pub previous: Option<String>,
    pub result: Vec<ClientPost>,
}

#[derive(OpenApi)]
#[openapi(
    info(
        title="Langunion API",
        description="Backend server for langunion",
    ),
    modifiers(&SecurityAddon, &ServerAddon, &PathsAddon),
    components(
        schemas(
            ClientUser,
            ClientFriendrequest,
            ClientMessage,
            ClientPost,
            ClientPostComment,
            RegisterUserBody,
            DeleteUserBody,
            UpdateUserBody,
            AvatarUpload,
            BlockUserBody,
            CreateTokensBody,
            RefreshTokensBody,
            SendFriendrequestBody,
            PostUpload,
            PostCommentBody,
            SendMessageBody,
            UpdateMessageBody,
        ),
        responses(
            PaginatedResponseClientUser,
            PaginatedResponseClientFriendrequest,
            PaginatedResponseClientMessage,
            PaginatedResponseClientPost,
            RegisterUserResponse,
            DeleteUserResponse,
            UpdateUserResponse,
            GetMeResponse,
            GetUserResponse,
            UpdateAvatarResponse,
            GetFriendResponse,
            RemoveFriendResponse,
            BlockUserResponse,
            GetBlockedUserResponse,
            UnblockUserResponse,
            CreateTokensResponse,
            RefreshTokensResponse,
            ResetTokensResponse,
            ValidateTokenResponse,
            SendFriendrequestResponse,
            GetFriendrequestResponse,
            DeleteFriendrequestResponse,
            AcceptFriendrequestResponse,
            SavePostResponse,
            GetPostResponse,
            DeletePostResponse,
            LikePostResponse,
            UnlikePostResponse,
            PostCommentResponse,
            DeleteCommentResponse,
            SendMessageResponse,
            GetMessageResponse,
            UpdateMessageResponse,
            DeleteMessageResponse,
            DeleteChatResponse,
        ),
    ),
    paths(
        crate::scopes::users::get_users,
        crate::scopes::users::register_user,
        crate::scopes::users::delete_user,
        crate::scopes::users::update_user,
        crate::scopes::users::get_me,
        crate::scopes::users::get_user,
        crate::scopes::users::update_avatar,
        crate::scopes::users::get_avatar,
        crate::scopes::friends::get_friends,
        crate::scopes::friends::get_friend,
        crate::scopes::friends::remove_friend,
        crate::scopes::blocked_users::get_blocked_users,
        crate::scopes::blocked_users::block_user,
        crate::scopes::blocked_users::get_blocked_user,
        crate::scopes::blocked_users::unblock_user,
        crate::scopes::chat_partners::get_chat_partners,
        crate::scopes::tokens::create_tokens,
        crate::scopes::tokens::refresh_tokens,
        crate::scopes::tokens::reset_tokens,
        crate::scopes::tokens::validate_token,
        crate::scopes::friendrequests::get_friendrequests,
        crate::scopes::friendrequests::send_friendrequest,
        crate::scopes::friendrequests::get_friendrequest,
        crate::scopes::friendrequests::delete_friendrequest,
        crate::scopes::friendrequests::accept_friendrequest,
        crate::scopes::posts::get_posts,
        crate::scopes::posts::save_post,
        crate::scopes::posts::get_post,
        crate::scopes::posts::delete_post,
        crate::scopes::posts::like_post,
        crate::scopes::posts::unlike_post,
        crate::scopes::posts::post_comment,
        crate::scopes::posts::delete_comment,
        crate::scopes::posts::get_post_pic,
        crate::scopes::messages::get_messages,
        crate::scopes::messages::send_message,
        crate::scopes::messages::get_message,
        crate::scopes::messages::update_message,
        crate::scopes::messages::delete_message,
        crate::scopes::messages::delete_message_only_for_me,
        crate::scopes::messages::get_chat,
        crate::scopes::messages::delete_chat,
    ),
)]
pub struct ApiDoc {}

impl ApiDoc {
    pub fn swagger_ui() -> SwaggerUi {
        SwaggerUi::new("/swagger-ui/{_:.*}").url("/openapi", ApiDoc::openapi())
    }
}

struct SecurityAddon;

impl Modify for SecurityAddon {
    fn modify(&self, openapi: &mut utoipa::openapi::OpenApi) {
        if openapi.components.is_none() {
            openapi.components = Some(Components::new());
        }

        openapi.components.as_mut().unwrap().add_security_scheme(
            "bearerAuth",
            SecurityScheme::Http(
                HttpBuilder::new()
                    .scheme(HttpAuthScheme::Bearer)
                    .bearer_format("JWT")
                    .build(),
            ),
        );

        openapi.components.as_mut().unwrap().add_security_scheme(
            "cookieAuth",
            SecurityScheme::ApiKey(ApiKey::Cookie(ApiKeyValue::new("Bearer"))),
        );
    }
}

struct ServerAddon;

impl Modify for ServerAddon {
    fn modify(&self, openapi: &mut utoipa::openapi::OpenApi) {
        #[cfg(debug_assertions)] // DEV
        let server: String = {
            use crate::config::use_config;
            format!("http://localhost:{}", use_config().general_port)
        };
        #[cfg(not(debug_assertions))] // PRODUCTION
        let server: String = {
            use crate::app::APPNAME;
            format!("http://server.{APPNAME}.com")
        };

        openapi.servers = Some(vec![Server::new(server)]);
    }
}

struct PathsAddon;

impl Modify for PathsAddon {
    fn modify(&self, openapi: &mut utoipa::openapi::OpenApi) {
        let prefix: String = format!("/{VERSION}");
        let new_paths = openapi
            .paths
            .paths
            .clone()
            .into_iter()
            .map(|(url, path_item)| {
                let new_url: String = format!("{prefix}{url}");
                (new_url, path_item)
            })
            .collect();
        openapi.paths.paths = new_paths;
    }
}
