use crate::app::{APPNAME, VERSION};
use crate::config::Config;
use crate::dbclient::DbClient;
use crate::error::HttpError;
use crate::openapi;
use crate::ws::server::{WebSocketServer, WebSocketServerHandle};
use crate::{scopes, ws};
use actix_cors::Cors;
use actix_multipart::{form::MultipartFormConfig, MultipartError};
use actix_web::{
    get,
    middleware::{Logger, NormalizePath, TrailingSlash},
    web::{self, JsonConfig},
    App, HttpRequest, HttpResponse, HttpServer,
};
use log::info;

/// Struct of the globally available app state
pub struct AppState {
    pub config: Config,
    pub dbclient: DbClient,
    pub websocket_server_handle: WebSocketServerHandle,
}

impl AppState {
    pub fn new(
        config: Config,
        dbclient: DbClient,
        websocket_server_handle: WebSocketServerHandle,
    ) -> Self {
        Self {
            config,
            dbclient,
            websocket_server_handle,
        }
    }
}

pub struct Server {}

impl Server {
    pub fn create_websocket_server(dbclient: DbClient) -> WebSocketServerHandle {
        let (websocket_server, websocket_server_handle): (WebSocketServer, WebSocketServerHandle) =
            WebSocketServer::new(dbclient);
        WebSocketServer::run(websocket_server);
        websocket_server_handle
    }

    pub fn create_app_state(
        config: Config,
        dbclient: DbClient,
        websocket_server_handle: WebSocketServerHandle,
    ) -> AppState {
        AppState {
            config,
            dbclient,
            websocket_server_handle,
        }
    }

    pub fn json_cfg() -> JsonConfig {
        JsonConfig::default().error_handler(
            |err: actix_web::error::JsonPayloadError, _: &HttpRequest| {
                let http_error = HttpError::bad_request(err.to_string());
                actix_web::error::InternalError::from_response(
                    err,
                    HttpResponse::BadRequest().json(http_error),
                )
                .into()
            },
        )
    }

    pub fn multipartform_cfg() -> MultipartFormConfig {
        MultipartFormConfig::default().error_handler(|err: MultipartError, _: &HttpRequest| {
            let http_error = HttpError::bad_request(err.to_string());
            actix_web::error::InternalError::from_response(
                err,
                HttpResponse::BadRequest().json(http_error),
            )
            .into()
        })
    }

    pub async fn run(config: Config) {
        #[cfg(not(debug_assertions))]
        let _guard = match !config.sentry_api_key.is_empty() {
            true => Some(sentry::init((
                config.sentry_api_key.clone(),
                sentry::ClientOptions {
                    release: sentry::release_name!(),
                    ..Default::default()
                },
            ))),
            false => None,
        };

        let mut dbclient: DbClient = DbClient::from_url(config.database_url()).await;
        dbclient.migrate().await;

        let websocket_server_handle: WebSocketServerHandle =
            Server::create_websocket_server(dbclient.clone());
        let port: u16 = config.general_port as u16;
        let app_state = AppState::new(config, dbclient, websocket_server_handle);
        let app_state: web::Data<AppState> = web::Data::new(app_state);

        info!("{}-server is running on port :{} ♥", APPNAME, port);

        HttpServer::new(move || {
            App::new()
                .wrap(Cors::permissive())
                .wrap(Logger::default())
                .wrap(sentry_actix::Sentry::new())
                .app_data(Self::json_cfg())
                .app_data(Self::multipartform_cfg())
                .app_data(app_state.clone())
                .service(root)
                .service(openapi::ApiDoc::swagger_ui())
                .service(
                    web::scope(&format!("/{VERSION}")) // TODO config override version
                        .wrap(NormalizePath::new(TrailingSlash::Always))
                        .service(ws::scope::socket_scope())
                        .service(scopes::users::users_scope())
                        .service(scopes::tokens::tokens_scope())
                        .service(scopes::friendrequests::friendrequests_scope())
                        .service(scopes::messages::messages_scope())
                        .service(scopes::posts::posts_scope()),
                )
                .default_service(web::to(not_found))
        })
        .bind(("0.0.0.0", port))
        .unwrap()
        .run()
        .await
        .unwrap();
    }

    #[cfg(test)]
    pub fn run_test(config: Config, dbclient: DbClient, port: u16) -> actix_web::dev::ServerHandle {
        let websocket_server_handle: WebSocketServerHandle =
            Server::create_websocket_server(dbclient.clone());
        let app_state = AppState::new(config, dbclient, websocket_server_handle);
        let app_state: web::Data<AppState> = web::Data::new(app_state);

        let server = HttpServer::new(move || {
            App::new()
                .wrap(Cors::permissive())
                .app_data(Self::json_cfg())
                .app_data(Self::multipartform_cfg())
                .app_data(app_state.clone())
                .service(
                    web::scope(&format!("/{VERSION}"))
                        .wrap(NormalizePath::new(TrailingSlash::Always))
                        .service(ws::scope::socket_scope())
                        .service(scopes::users::users_scope())
                        .service(scopes::tokens::tokens_scope())
                        .service(scopes::friendrequests::friendrequests_scope())
                        .service(scopes::messages::messages_scope())
                        .service(scopes::posts::posts_scope()),
                )
        })
        .bind(("0.0.0.0", port))
        .unwrap()
        .run();

        let server_handle = server.handle();

        tokio::task::spawn(async {
            let _ = server.await;
        });

        server_handle
    }
}

#[get("/")]
async fn root() -> HttpResponse {
    HttpResponse::Ok().json(serde_json::json!({
        "message": format!("{}-server is up and running ♥", APPNAME),
    }))
}

async fn not_found() -> HttpResponse {
    HttpResponse::NotFound().json(HttpError::not_found())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::utils::test_utils;
    use actix_web::{body::MessageBody, http, test, web::Bytes, App};
    use pretty_assertions::assert_eq;
    use sqlx::mysql::MySqlPool;

    #[actix_web::test]
    async fn test_start_server() {
        let app = test::init_service(App::new().service(root)).await;
        let req = test::TestRequest::default().to_request();
        let resp = test::call_service(&app, req).await;
        assert!(resp.status().is_success());
    }

    #[sqlx::test]
    fn test_start_test_server(pool: MySqlPool) {
        let config = Config::new_test();
        let dbclient = DbClient::from_pool(pool);
        let port = test_utils::get_random_port();
        let server = Server::run_test(config, dbclient.clone(), port);
        server.stop(true).await;
    }

    #[actix_web::test]
    async fn test_root_returns_json() {
        let app = test::init_service(App::new().service(root)).await;
        let req = test::TestRequest::default().to_request();
        let resp = test::call_service(&app, req).await;
        let body: Bytes = resp.into_body().try_into_bytes().unwrap();

        let json_str: String = String::from_utf8_lossy(&body.to_vec()).into_owned();
        let is_json = serde_json::from_str::<serde_json::Value>(&json_str).is_ok();
        assert!(is_json);
    }

    #[actix_web::test]
    async fn test_not_found_returns_json() {
        let app = test::init_service(App::new().default_service(web::to(not_found))).await;
        let req = test::TestRequest::default().uri("/asdf1234").to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.status(), http::StatusCode::NOT_FOUND);

        let body: Bytes = resp.into_body().try_into_bytes().unwrap();
        let expected_json: String = serde_json::to_string(&HttpError::not_found()).unwrap();

        assert_eq!(body, expected_json);
    }

    #[actix_web::test]
    async fn test_not_fount_on_api_route() {
        let app = test::init_service(App::new().default_service(web::to(not_found))).await;
        let req = test::TestRequest::default()
            .uri("/v1/asdf1234")
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.status(), http::StatusCode::NOT_FOUND);

        let body: Bytes = resp.into_body().try_into_bytes().unwrap();
        let expected_json: String = serde_json::to_string(&HttpError::not_found()).unwrap();

        assert_eq!(body, expected_json);
    }

    #[actix_web::test]
    async fn test_deserialize_json_error_returns_json() {
        #[derive(serde::Serialize, serde::Deserialize)]
        struct TestBody {
            a: String,
        }

        #[actix_web::post("/")]
        async fn test_route(_: web::Json<TestBody>) -> String {
            String::new()
        }

        let app =
            test::init_service(App::new().app_data(Server::json_cfg()).service(test_route)).await;
        let req = test::TestRequest::post()
            .uri("/")
            .set_json(serde_json::json!({ "b": "c" }))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);

        let body: Bytes = resp.into_body().try_into_bytes().unwrap();
        let expected_json: String = "{\"status\":400,\"message\":\"Json deserialize error: missing field `a` at line 1 column 9\"}".to_string();

        assert_eq!(body, expected_json);
    }

    #[actix_web::test]
    async fn test_deserialize_mutilpart_error_returns_json() {
        use actix_multipart::form::{text::Text, MultipartForm};

        #[allow(unused)]
        #[derive(MultipartForm)]
        #[multipart(deny_unknown_fields)]
        struct TestMultipartForm {
            a: Text<String>,
        }

        #[actix_web::post("/")]
        async fn test_route(_: MultipartForm<TestMultipartForm>) -> String {
            String::new()
        }

        let payload: Bytes = Bytes::from(format!(
            "testasdadsad\r\n\
                 --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
                 Content-Disposition: form-data; name=\"b\"\r\n\
                 Content-Type: text/plain; charset=utf-8\r\nContent-Length: 256\r\n\r\n\
                 {}\r\n\
                 --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
            "c",
        ));

        let app = test::init_service(
            App::new()
                .app_data(Server::multipartform_cfg())
                .service(test_route),
        )
        .await;
        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .set_payload(payload)
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);

        let body: Bytes = resp.into_body().try_into_bytes().unwrap();
        let expected_json: String =
            "{\"status\":400,\"message\":\"Unsupported field `b`\"}".to_string();

        assert_eq!(body, expected_json);
    }
}
