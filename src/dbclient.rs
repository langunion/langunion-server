use crate::error::{ErrorMessage, HttpError};
use crate::models::{
    DbBlockedUser, DbFriend, DbFriendrequest, DbMessage, DbPost, DbPostComment, DbPostLike,
    DbToken, DbUser, Status,
};
use chrono::Utc;
use crate::utils::password_utils;
use anyhow::Context;
use log::{debug, error};
use sqlx::mysql::{MySql, MySqlPool, MySqlPoolOptions};
use sqlx::Transaction;

#[derive(Debug, Clone)]
pub struct DbClient {
    pool: MySqlPool,
}

impl DbClient {
    pub fn from_pool(pool: MySqlPool) -> Self {
        debug!("Created new dbclient");
        DbClient { pool }
    }

    pub async fn from_url(database_url: impl Into<String>) -> DbClient {
        let database_url: String = database_url.into();
        debug!("Connect to {database_url}");
        let pool = MySqlPoolOptions::new()
            .max_connections(10)
            .connect(&database_url)
            .await
            .unwrap();
        DbClient::from_pool(pool)
    }

    pub async fn migrate(&mut self) {
        // FIXME could fail when db is net setup correctly
        // like wrong username, password or no postgres installed
        // TODO therefore show correct error message what happened wrong
        sqlx::migrate!()
            .run(&self.pool)
            .await
            .context("Migrating db...")
            .unwrap();
    }

    pub async fn begin_transaction(&self) -> Result<Transaction<'_, MySql>, HttpError> {
        self.pool
            .begin()
            .await
            .context("Failed to begin transaction")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })
    }

    pub async fn commit_transaction(tx: Transaction<'_, MySql>) -> Result<(), HttpError> {
        tx.commit()
            .await
            .context("Failed to commit transaction")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })
    }

    pub async fn rollback_transaction(tx: Transaction<'_, MySql>) -> Result<(), HttpError> {
        tx.rollback()
            .await
            .context("Failed to rollback transaction")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })
    }
}

impl DbClient {
    pub async fn execute(
        &self,
        query: impl Into<String>,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        let query_str: String = query.into();
        let query = sqlx::query(&query_str);
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result
            .context(format!("Failed to execute query: {query_str}"))
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?;
        Ok(())
    }

    pub async fn get_user(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Option<DbUser> {
        debug!("Get user with the id: {user_id}");
        let query = sqlx::query_as!(
            DbUser,
            "SELECT * FROM users WHERE id = ? AND is_deleted IS NULL",
            user_id as u64
        );
        let result = match tx {
            Some(tx) => query.fetch_optional(&mut **tx).await,
            None => query.fetch_optional(&self.pool).await,
        };
        result.context("Failed to find a user").unwrap_or(None)
    }

    pub async fn get_user_by_username(
        &self,
        username: impl Into<String>,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Option<DbUser> {
        let username: String = username.into();
        debug!("Get user with the username: {username}");

        let query = sqlx::query_as!(
            DbUser,
            "SELECT * FROM users WHERE username = ? AND is_deleted IS NULL",
            username
        );
        let result = match tx {
            Some(tx) => query.fetch_optional(&mut **tx).await,
            None => query.fetch_optional(&self.pool).await,
        };
        result
            .context("Failed to find a user by username")
            .unwrap_or(None)
    }

    pub async fn get_user_by_email(
        &self,
        email: impl Into<String>,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Option<DbUser> {
        let email: String = email.into();
        debug!("Get user with the email: {email}");

        let query = sqlx::query_as!(
            DbUser,
            "SELECT * FROM users WHERE email = ? AND is_deleted IS NULL",
            email
        );
        let result = match tx {
            Some(tx) => query.fetch_optional(&mut **tx).await,
            None => query.fetch_optional(&self.pool).await,
        };
        result
            .context("Failed to find user by email")
            .unwrap_or(None)
    }

    pub async fn get_users(
        &self,
        query: impl Into<String>,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Vec<DbUser> {
        let query: String = query.into();
        debug!("Get all the users that match the query: {query}");

        let query_fuzzy: String = format!("%{}%", &query);
        let query = sqlx::query_as!(
            DbUser,
            "SELECT * FROM users WHERE ((username LIKE ?) OR (email = ?)) \
             AND is_deleted IS NULL \
             ORDER BY username ASC",
            &query_fuzzy,
            &query,
        );
        let result = match tx {
            Some(tx) => query.fetch_all(&mut **tx).await,
            None => query.fetch_all(&self.pool).await,
        };
        result
            .context("Failed to find users by query")
            .unwrap_or(Vec::new())
    }

    pub async fn save_user(
        &self,
        username: impl Into<String>,
        email: impl Into<String>,
        password: impl Into<String>,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<usize, HttpError> {
        let username: String = username.into();
        let email: String = email.into();
        let password: String = password.into();

        debug!("Insert a new user with the username: {username} and email: {email}");

        let is_email_taken: bool = self
            .get_user_by_email(&email, tx.as_deref_mut())
            .await
            .is_some();
        let is_username_taken: bool = self
            .get_user_by_username(&username, tx.as_deref_mut())
            .await
            .is_some();
        if is_email_taken {
            return Err(HttpError::bad_request(ErrorMessage::EmailExist));
        }
        if is_username_taken {
            return Err(HttpError::bad_request(ErrorMessage::UsernameExist));
        }

        let hashed_password: String = password_utils::hash(&password);

        let query = sqlx::query!(
            "INSERT INTO users(username, display_username, email, password) VALUES (?, ?, ?, ?)",
            &username,
            &username,
            &email,
            hashed_password,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let user_id = result
            .context("Failed to save user")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .last_insert_id();
        Ok(user_id as usize)
    }

    pub async fn delete_user(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete user with the id: {user_id}");
        let query = sqlx::query!(
            "UPDATE users SET is_deleted = CURRENT_TIMESTAMP(3) WHERE id = ?",
            user_id as u64
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result.context("Failed to delete user").map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;
        Ok(())
    }

    pub async fn update_user(
        &self,
        user_id: usize,
        username: Option<&str>,
        display_username: Option<&str>,
        password: Option<&str>,
        email: Option<&str>,
        status: Option<Status>,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        /* Update username */
        if let Some(username) = username {
            debug!("Update username of user with the id: {user_id}");
            let is_username_taken: bool = self
                .get_user_by_username(username, tx.as_deref_mut())
                .await
                .is_some();
            if is_username_taken {
                return Err(HttpError::bad_request(ErrorMessage::UsernameExist));
            }

            let query = sqlx::query!(
                "UPDATE users SET username = ? WHERE id = ? AND is_deleted IS NULL",
                username,
                user_id as u64,
            );
            let result = match tx.as_mut() {
                Some(tx) => query.execute(&mut ***tx).await,
                None => query.execute(&self.pool).await,
            };
            result.context("Failed to update username").map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?;
        }
        /* Update display username */
        if let Some(display_username) = display_username {
            debug!("Update display_username of user with the id: {user_id}");

            let query = sqlx::query!(
                "UPDATE users SET display_username = ? WHERE id = ? AND is_deleted IS NULL",
                display_username,
                user_id as u64,
            );
            let result = match tx.as_mut() {
                Some(tx) => query.execute(&mut ***tx).await,
                None => query.execute(&self.pool).await,
            };
            result
                .context("Failed to update display_username")
                .map_err(|e| {
                    error!("{e}");
                    HttpError::server_error()
                })?;
        }
        /* Update password */
        if let Some(password) = password {
            debug!("Update password of user with the id: {user_id}");
            let hashed: String = password_utils::hash(password);
            let query = sqlx::query!(
                "UPDATE users SET password = ? WHERE id = ? AND is_deleted IS NULL",
                hashed,
                user_id as u64,
            );
            let result = match tx.as_mut() {
                Some(tx) => query.execute(&mut ***tx).await,
                None => query.execute(&self.pool).await,
            };
            result.context("Failed to update password").map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?;
        }
        /* Update email */
        if let Some(email) = email {
            debug!("Update email of user with the id: {user_id}");
            let is_email_taken: bool = self
                .get_user_by_email(email, tx.as_deref_mut())
                .await
                .is_some();
            if is_email_taken {
                return Err(HttpError::bad_request(ErrorMessage::EmailExist));
            }

            let query = sqlx::query!(
                "UPDATE users SET email = ? WHERE id = ? AND is_deleted IS NULL",
                email,
                user_id as u64,
            );
            let result = match tx.as_mut() {
                Some(tx) => query.execute(&mut ***tx).await,
                None => query.execute(&self.pool).await,
            };
            result.context("Failed to update email").map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?;
        }
        /* Update online status */
        if let Some(status) = status {
            debug!("Update online status of user with the id: {user_id}");
            let new_status: String = status.to_string();
            let query = sqlx::query!(
                "UPDATE users SET status = ? WHERE id = ? AND is_deleted IS NULL",
                &new_status,
                user_id as u64,
            );
            let result = match tx.as_mut() {
                Some(tx) => query.execute(&mut ***tx).await,
                None => query.execute(&self.pool).await,
            };
            result
                .context("Failed to update online status")
                .map_err(|e| {
                    error!("{e}");
                    HttpError::server_error()
                })?;
        }

        Ok(())
    }

    pub async fn get_refresh_tokens(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Vec<String> {
        debug!("Get refresh tokens of user with the id: {user_id}");
        let query = sqlx::query_as!(
            DbToken,
            "SELECT * FROM tokens WHERE user_id = ? AND is_deleted IS NULL",
            user_id as u64,
        );
        let tokens = match tx {
            Some(tx) => query.fetch_all(&mut **tx).await,
            None => query.fetch_all(&self.pool).await,
        };

        tokens
            .context("Failed to get refresh tokens")
            .unwrap_or_else(|e| {
                error!("{e}");
                Vec::new()
            })
            .into_iter()
            .map(|t: DbToken| t.refresh)
            .collect::<Vec<String>>()
    }

    pub async fn get_refresh_token(
        &self,
        token: impl Into<String>,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Option<String> {
        debug!("Get refresh token");
        let query = sqlx::query_as!(
            DbToken,
            "SELECT * FROM tokens WHERE refresh = ? AND is_deleted IS NULL",
            token.into(),
        );
        let result = match tx {
            Some(tx) => query.fetch_optional(&mut **tx).await,
            None => query.fetch_optional(&self.pool).await,
        };
        let token = result
            .context("Failed to get refresh token")
            .unwrap_or(None);

        match token {
            Some(token) => Some(token.refresh),
            None => None,
        }
    }

    pub async fn save_refresh_token(
        &self,
        user_id: usize,
        token: impl Into<String>,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<usize, HttpError> {
        debug!("Save a refresh token for user with the id: {user_id}");
        let query = sqlx::query!(
            "INSERT INTO tokens (user_id, refresh) VALUE (?, ?)",
            user_id as u64,
            token.into(),
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let refresh_token_id = result
            .context("Failed to save refresh token")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .last_insert_id();
        Ok(refresh_token_id as usize)
    }

    pub async fn delete_refresh_tokens(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete refresh tokens of user with the id: {user_id}");
        let query = sqlx::query!(
            "UPDATE tokens SET is_deleted = CURRENT_TIMESTAMP(3) WHERE user_id = ?",
            user_id as u64
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result
            .context("Failed to delete refresh tokens")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?;
        Ok(())
    }

    pub async fn delete_refresh_token(
        &self,
        token: impl Into<String>,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete a refresh token");
        let query = sqlx::query!(
            "UPDATE tokens SET is_deleted = CURRENT_TIMESTAMP(3) WHERE refresh = ?",
            token.into()
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result
            .context("Failed to delete refresh token")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?;
        Ok(())
    }

    pub async fn get_friend(
        &self,
        user_id_one: usize,
        user_id_two: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Option<DbFriend> {
        debug!("Get friend with the first user: {user_id_one} and the second user: {user_id_two}");
        let query = sqlx::query_as!(
            DbFriend,
            "SELECT * FROM friends \
             WHERE ((user_one = ? AND user_two = ?) \
             OR (user_one = ? AND user_two = ?)) \
             AND is_deleted IS NULL",
            user_id_one as u64,
            user_id_two as u64,
            user_id_two as u64,
            user_id_one as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_optional(&mut **tx).await,
            None => query.fetch_optional(&self.pool).await,
        };
        result.context("Failed to get friend").unwrap_or(None)
    }

    pub async fn get_friends(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Vec<DbFriend> {
        debug!("Get friends of the user with the id: {user_id}");
        let query = sqlx::query_as!(
            DbFriend,
            "SELECT * FROM friends \
             WHERE (user_one = ? OR user_two = ?) \
             AND is_deleted IS NULL",
            user_id as u64,
            user_id as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_all(&mut **tx).await,
            None => query.fetch_all(&self.pool).await,
        };
        result
            .context("Failed to get friends")
            .unwrap_or(Vec::new())
    }

    pub async fn get_friends_as_user(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Vec<DbUser> {
        debug!("Get friends as users of the user with the id: {user_id}");
        let query = sqlx::query_as!(
            DbUser,
            "SELECT users.* FROM users \
             INNER JOIN friends ON (friends.user_one = users.id OR friends.user_two = users.id) \
             WHERE (friends.user_one = ? OR friends.user_two = ?) \
             AND friends.is_deleted IS NULL \
             AND users.is_deleted IS NULL \
             AND users.id != ?",
            user_id as u64,
            user_id as u64,
            user_id as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_all(&mut **tx).await,
            None => query.fetch_all(&self.pool).await,
        };
        result.unwrap_or(Vec::new())
    }

    pub async fn save_friend(
        &self,
        user_id_one: usize,
        user_id_two: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<usize, HttpError> {
        if user_id_one == user_id_two {
            return Err(HttpError::bad_request(
                ErrorMessage::CantSendYourselfFriendrequest,
            ));
        }

        debug!(
            "Insert friend with the first user: {user_id_one} and the second user: {user_id_two}"
        );
        let query = sqlx::query!(
            "INSERT INTO friends (user_one, user_two) VALUES (?, ?)",
            user_id_one as u64,
            user_id_two as u64,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let friend_id = result
            .context("Failedc to save friend")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .last_insert_id();
        Ok(friend_id as usize)
    }

    pub async fn delete_friend(
        &self,
        user_id_one: usize,
        user_id_two: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!(
            "Delete friend with the first user: {user_id_one} and the second user: {user_id_two}"
        );
        let query = sqlx::query!(
            "UPDATE friends SET is_deleted = CURRENT_TIMESTAMP(3) \
             WHERE (user_one = ? AND user_two = ?) \
             OR (user_one = ? AND user_two = ?)",
            user_id_one as u64,
            user_id_two as u64,
            user_id_two as u64,
            user_id_one as u64,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result.context("Failed to delete friend").map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;
        Ok(())
    }

    pub async fn delete_friends(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete all friends of user with the id: {user_id}");
        let query = sqlx::query!(
            "UPDATE friends SET is_deleted = CURRENT_TIMESTAMP(3) WHERE user_one = ? OR user_two = ?",
            user_id as u64,
            user_id as u64,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result.context("Failed to delete friends").map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;
        Ok(())
    }

    pub async fn get_friendrequest(
        &self,
        user_id: usize,
        friend_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Option<DbFriendrequest> {
        debug!("Get friendrequest with user_id: {user_id} and the friend_id: {friend_id}");
        let query = sqlx::query_as!(
            DbFriendrequest,
            "SELECT * FROM friendrequests \
             WHERE ((from_user = ? AND to_user = ?) \
             OR (from_user = ? AND to_user = ?)) \
             AND is_deleted IS NULL",
            user_id as u64,
            friend_id as u64,
            friend_id as u64,
            user_id as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_optional(&mut **tx).await,
            None => query.fetch_optional(&self.pool).await,
        };
        result
            .context("Failed to get friendrequest")
            .unwrap_or(None)
    }

    pub async fn get_friendrequests(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Vec<DbFriendrequest> {
        debug!("Get all the friendrequests of the user with the id: {user_id}");
        let query = sqlx::query_as!(
            DbFriendrequest,
            "SELECT * FROM friendrequests \
             WHERE ((from_user = ? OR to_user = ?)) \
             AND is_deleted IS NULL \
             ORDER BY id ASC",
            user_id as u64,
            user_id as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_all(&mut **tx).await,
            None => query.fetch_all(&self.pool).await,
        };
        result
            .context("Failed to get friendrequests")
            .unwrap_or(Vec::new())
    }

    pub async fn get_friendrequest_by_id(
        &self,
        req_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Option<DbFriendrequest> {
        debug!("Get friendrequest with the id: {req_id}");
        let query = sqlx::query_as!(
            DbFriendrequest,
            "SELECT * FROM friendrequests WHERE id = ? AND is_deleted IS NULL",
            req_id as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_optional(&mut **tx).await,
            None => query.fetch_optional(&self.pool).await,
        };
        result
            .context("Failed to get friendrequest by id")
            .unwrap_or(None)
    }

    pub async fn save_friendrequest(
        &self,
        user_id: usize,
        friend_id: usize,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<usize, HttpError> {
        debug!("Save friendrequest of user with the id: {user_id} and the friend with the id: {friend_id}");
        // check if the users exist
        // let user: Option<DbUser> = self.get_user(user_id, tx.as_deref_mut()).await;
        let user: Option<DbUser> = self.get_user(user_id, tx.as_deref_mut()).await;
        // let friend: Option<DbUser> = self.get_user(friend_id, tx.as_deref_mut()).await;
        let friend: Option<DbUser> = self.get_user(friend_id, tx.as_deref_mut()).await;
        if user.is_none() || friend.is_none() {
            return Err(HttpError::bad_request(ErrorMessage::NoUserFound));
        }
        // Check if they're not already friends in save_friendrequest
        let friend: Option<DbFriend> = self.get_friend(user_id, friend_id, tx.as_deref_mut()).await;
        if friend.is_some() {
            return Err(HttpError::bad_request(ErrorMessage::AlreadyFriends));
        }
        // check if there is already a pending friendrequest
        let friendrequest: Option<DbFriendrequest> = self
            .get_friendrequest(user_id, friend_id, tx.as_deref_mut())
            .await;
        if friendrequest.is_some() {
            return Err(HttpError::bad_request(
                ErrorMessage::AlreadyPendingFriendrequest,
            ));
        }

        let query = sqlx::query!(
            "INSERT INTO friendrequests (from_user, to_user) VALUES(?, ?)",
            user_id as u64,
            friend_id as u64,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let friendrequest_id = result
            .context("Failed to save friendrequest")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .last_insert_id();
        Ok(friendrequest_id as usize)
    }

    pub async fn delete_friendrequest(
        &self,
        req_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete friendrequest with the id: {req_id}");
        let query = sqlx::query!(
            "UPDATE friendrequests SET is_deleted = CURRENT_TIMESTAMP(3) WHERE id = ?",
            req_id as u64
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result
            .context("Failed to delete friendrequest")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?;
        Ok(())
    }

    pub async fn delete_friendrequests(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete all the friendrequests of the user with the id: {user_id}");
        let query = sqlx::query!(
            "UPDATE friendrequests SET is_deleted = CURRENT_TIMESTAMP(3) WHERE from_user = ? OR to_user = ?",
            user_id as u64,
            user_id as u64,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result
            .context("Failed to delete friendrequests")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?;
        Ok(())
    }

    pub async fn get_blocked_user(
        &self,
        user_id: usize,
        friend_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Option<DbBlockedUser> {
        debug!("Get blocked user with the user_id: {user_id} and the friend_id: {friend_id}");
        let query = sqlx::query_as!(
            DbBlockedUser,
            "SELECT * FROM blocked_users \
             WHERE ((user_id = ? AND blocked_user_id = ?)) \
             AND is_deleted IS NULL",
            user_id as u64,
            friend_id as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_optional(&mut **tx).await,
            None => query.fetch_optional(&self.pool).await,
        };
        result.context("Failed to get blocked user").unwrap_or(None)
    }

    pub async fn get_blocked_users(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Vec<DbBlockedUser> {
        debug!("Get all the blocked users of the user with the id: {user_id}");
        let query = sqlx::query_as!(
            DbBlockedUser,
            "SELECT * FROM blocked_users \
             WHERE (user_id = ?) AND is_deleted IS NULL",
            user_id as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_all(&mut **tx).await,
            None => query.fetch_all(&self.pool).await,
        };
        result
            .context("Failed to get blocked users")
            .unwrap_or(Vec::new())
    }

    pub async fn block_user(
        &self,
        user_id: usize,
        friend_id: usize,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<usize, HttpError> {
        debug!("Block user: {friend_id} by user: {user_id}");
        // Check if the user is already blocked
        let query = sqlx::query_as!(
            DbBlockedUser,
            "SELECT * FROM blocked_users WHERE (user_id = ? AND blocked_user_id = ?) AND is_deleted IS NULL",
            user_id as u64,
            friend_id as u64,
        );
        let result = match tx.as_mut() {
            Some(tx) => query.fetch_one(&mut ***tx).await,
            None => query.fetch_one(&self.pool).await,
        };
        if let Ok(_) = result {
            return Err(HttpError::bad_request(ErrorMessage::UserAlreadyBlocked));
        }

        // Block the user
        let query = sqlx::query!(
            "INSERT INTO blocked_users (user_id, blocked_user_id) VALUES (?, ?)",
            user_id as u64,
            friend_id as u64,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let blocked_user_id = result
            .context("Failed to block user")
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .last_insert_id();
        Ok(blocked_user_id as usize)
    }

    pub async fn unblock_user(
        &self,
        user_id: usize,
        friend_id: usize,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Unblock user: {friend_id} by user: {user_id}");
        // Check if the user is blocked
        let query = sqlx::query_as!(
            DbBlockedUser,
            "SELECT * FROM blocked_users WHERE (user_id = ? AND blocked_user_id = ?) AND is_deleted IS NULL",
            user_id as u64,
            friend_id as u64,
        );
        let result = match tx.as_mut() {
            Some(tx) => query.fetch_one(&mut ***tx).await,
            None => query.fetch_one(&self.pool).await,
        };
        let blocked_user =
            result.map_err(|_| HttpError::bad_request(ErrorMessage::UserIsntBlocked))?;

        let query = sqlx::query!(
            "UPDATE blocked_users SET is_deleted = CURRENT_TIMESTAMP(3) WHERE (id = ?)",
            blocked_user.id
        );
        let result = match tx.as_mut() {
            Some(tx) => query.execute(&mut ***tx).await,
            None => query.execute(&self.pool).await,
        };
        result.context("Failed to unblock user").map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;
        Ok(())
    }

    pub async fn unblock_users(
        &self,
        user_id: usize,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Unblock all users of user with the id: {user_id}");
        let blocked_users: Vec<DbBlockedUser> =
            self.get_blocked_users(user_id, tx.as_deref_mut()).await;
        for blocked_user in blocked_users {
            self.unblock_user(
                user_id,
                blocked_user.blocked_user_id as usize,
                tx.as_deref_mut(),
            )
            .await?;
        }
        Ok(())
    }

    pub async fn get_post(
        &self,
        post_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Option<DbPost> {
        debug!("Get post with the id: {post_id}");
        let query = sqlx::query_as!(
            DbPost,
            "SELECT * FROM posts WHERE id = ? AND is_deleted IS NULL",
            post_id as u64
        );
        let result = match tx {
            Some(tx) => query.fetch_optional(&mut **tx).await,
            None => query.fetch_optional(&self.pool).await,
        };
        result.context("Failed to get post").unwrap_or(None)
    }

    pub async fn get_posts(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Vec<DbPost> {
        debug!("Get all the posts of the user with the id: {user_id}");
        // Get user ids of friends and user_id
        let mut user_ids: Vec<String> = self
            .get_friends(user_id, None)
            .await
            .into_iter()
            .map(|friend_item: DbFriend| {
                friend_item
                    .get_user_id_of_friend(user_id)
                    .unwrap()
                    .to_string()
            })
            .collect();
        user_ids.push(user_id.to_string());

        let query: String = format!(
            "SELECT * FROM posts WHERE author in ({}) \
             AND is_deleted IS NULL \
             ORDER BY created_at DESC",
            user_ids.join(", ")
        );
        // FIXME when sqlx has added support for vec as bind
        // sqlx::query_as!(DbPost, "SELECT * FROM posts WHERE author in (?)", query)
        // but not high-priority, because the binding in this case is not user input
        // so we can be sure that the query is safe to execute
        let query = sqlx::query_as(&query);
        let result = match tx {
            Some(tx) => query.fetch_all(&mut **tx).await,
            None => query.fetch_all(&self.pool).await,
        };
        result.context("Failed to get posts").unwrap_or(Vec::new())
    }

    pub async fn save_post(
        &self,
        user_id: usize,
        content: impl Into<String>,
        image_count: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<usize, HttpError> {
        debug!("Insert post by user with the id: {user_id}");
        let content: String = content.into();

        if content.len() > 255 {
            return Err(HttpError::bad_request(ErrorMessage::PostTooLong));
        }

        let query = sqlx::query!(
            "INSERT INTO posts (author, content, image_count) VALUES (?, ?, ?)",
            user_id as u64,
            content,
            image_count as u64,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let post_id = result
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .last_insert_id();
        Ok(post_id as usize)
    }

    pub async fn delete_post(
        &self,
        post_id: usize,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete post with the id: {post_id}");
        self.delete_post_likes(post_id, tx.as_deref_mut()).await?;
        self.delete_post_comments(post_id, tx.as_deref_mut())
            .await?;

        let query = sqlx::query!(
            "UPDATE posts SET is_deleted = CURRENT_TIMESTAMP(3) WHERE id = ?",
            post_id as u64
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let rows_affected = result
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .rows_affected();

        if rows_affected == 0 {
            return Err(HttpError::bad_request(ErrorMessage::NoPostFound));
        }

        Ok(())
    }

    pub async fn delete_posts(
        &self,
        user_id: usize,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete all the posts of the user with the id: {user_id}");

        // Delete my posts
        let query = sqlx::query!(
            "UPDATE posts SET is_deleted = CURRENT_TIMESTAMP(3) WHERE author = ?",
            user_id as u64
        );
        let result = match tx.as_mut() {
            Some(tx) => query.execute(&mut ***tx).await,
            None => query.execute(&self.pool).await,
        };
        result.map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;

        // Delete my likes
        let query = sqlx::query!(
            "UPDATE post_likes SET is_deleted = CURRENT_TIMESTAMP(3) WHERE user_id = ?",
            user_id as u64
        );
        let result = match tx.as_mut() {
            Some(tx) => query.execute(&mut ***tx).await,
            None => query.execute(&self.pool).await,
        };
        result.map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;

        // delete my comments
        let query = sqlx::query!(
            "UPDATE post_comments SET is_deleted = CURRENT_TIMESTAMP(3) WHERE author = ?",
            user_id as u64
        );
        let result = match tx.as_mut() {
            Some(tx) => query.execute(&mut ***tx).await,
            None => query.execute(&self.pool).await,
        };
        result.map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;

        Ok(())
    }

    pub async fn get_post_likes(
        &self,
        post_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Vec<DbPostLike> {
        debug!("Get all the post likes of the post with the id: {post_id}");
        let query = sqlx::query_as!(
            DbPostLike,
            "SELECT * FROM post_likes WHERE post_id = ? AND is_deleted IS NULL",
            post_id as u64
        );
        let result = match tx {
            Some(tx) => query.fetch_all(&mut **tx).await,
            None => query.fetch_all(&self.pool).await,
        };
        result.unwrap_or(Vec::new())
    }

    pub async fn delete_post_likes(
        &self,
        post_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete all the post likes of the post with the id: {post_id}");
        let query = sqlx::query!(
            "UPDATE post_likes SET is_deleted = CURRENT_TIMESTAMP(3) WHERE post_id = ?",
            post_id as u64
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result.map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;
        Ok(())
    }

    pub async fn like_post(
        &self,
        post_id: usize,
        user_id: usize,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<usize, HttpError> {
        debug!("The user with the id: {user_id} likes a post with the id: {post_id}");
        self.get_post(post_id, tx.as_deref_mut())
            .await
            .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?;

        let liked: bool = self
            .get_post_likes(post_id, tx.as_deref_mut())
            .await
            .into_iter()
            .map(|post_like: DbPostLike| post_like.user_id as usize)
            .collect::<Vec<usize>>()
            .contains(&user_id);

        if liked {
            return Err(HttpError::bad_request(ErrorMessage::CannotLikePostTwice));
        }

        let query = sqlx::query!(
            "INSERT INTO post_likes (post_id, user_id) VALUES (?, ?)",
            post_id as u64,
            user_id as u64
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let post_like_id = result
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .last_insert_id();
        Ok(post_like_id as usize)
    }

    pub async fn unlike_post(
        &self,
        post_id: usize,
        user_id: usize,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("The user with the id: {user_id} unlikes a post with the id: {post_id}");
        self.get_post(post_id, tx.as_deref_mut())
            .await
            .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?;

        let query = sqlx::query!(
            "DELETE FROM post_likes WHERE post_id = ? and user_id = ? AND is_deleted IS NULL",
            post_id as u64,
            user_id as u64
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let rows_affected = result
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .rows_affected();

        if rows_affected == 0 {
            return Err(HttpError::bad_request(ErrorMessage::CannotUnlikePostTwice));
        }
        Ok(())
    }

    pub async fn get_post_comment(
        &self,
        comment_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Option<DbPostComment> {
        debug!("Get a post_comment with the id: {comment_id}");
        let query = sqlx::query_as!(
            DbPostComment,
            "SELECT * FROM post_comments WHERE id = ? AND is_deleted IS NULL",
            comment_id as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_optional(&mut **tx).await,
            None => query.fetch_optional(&self.pool).await,
        };
        result.unwrap_or(None)
    }

    pub async fn get_post_comments(
        &self,
        post_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Vec<DbPostComment> {
        debug!("Get all the post_comments of the post with the id: {post_id}");
        let query = sqlx::query_as!(
            DbPostComment,
            "SELECT * FROM post_comments WHERE post_id = ? AND is_deleted IS NULL \
             ORDER BY created_at DESC",
            post_id as u64
        );
        let result = match tx {
            Some(tx) => query.fetch_all(&mut **tx).await,
            None => query.fetch_all(&self.pool).await,
        };
        result.unwrap_or(Vec::new())
    }

    pub async fn save_post_comment(
        &self,
        post_id: usize,
        user_id: usize,
        content: impl Into<String>,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<usize, HttpError> {
        debug!("Insert a new post_comment by the user: {user_id} to the post: {post_id}");
        let content: String = content.into();
        if content.len() > 255 {
            return Err(HttpError::bad_request(ErrorMessage::PostTooLong));
        }

        // check if post exists
        self.get_post(post_id, tx.as_deref_mut())
            .await
            .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?;

        let query = sqlx::query!(
            "INSERT INTO post_comments (post_id, author, content) VALUES (?, ?, ?)",
            post_id as u64,
            user_id as u64,
            content,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let post_comment_id = result
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .last_insert_id();
        Ok(post_comment_id as usize)
    }

    pub async fn delete_post_comment(
        &self,
        comment_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete post_comment with the id: {comment_id}");
        let query = sqlx::query!(
            "UPDATE post_comments SET is_deleted = CURRENT_TIMESTAMP(3) WHERE id = ?",
            comment_id as u64
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let rows_affected = result
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .rows_affected();

        if rows_affected == 0 {
            return Err(HttpError::bad_request(ErrorMessage::NoPostCommentFound));
        }
        Ok(())
    }

    pub async fn delete_post_comments(
        &self,
        post_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete all the post_comments of the post with the id: {post_id}");
        let query = sqlx::query!(
            "UPDATE post_comments SET is_deleted = CURRENT_TIMESTAMP(3) WHERE post_id = ?",
            post_id as u64
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result.map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;
        Ok(())
    }

    pub async fn get_message(
        &self,
        message_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Option<DbMessage> {
        debug!("Get a message with the id: {message_id}");
        let query = sqlx::query_as!(
            DbMessage,
            "SELECT * FROM messages WHERE id = ? AND is_deleted IS NULL",
            message_id as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_optional(&mut **tx).await,
            None => query.fetch_optional(&self.pool).await,
        };
        result.unwrap_or(None)
    }

    pub async fn get_messages(
        &self,
        user_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Vec<DbMessage> {
        debug!("Get all the messages of the user with the id: {user_id}");
        let query = sqlx::query_as!(
            DbMessage,
            "SELECT * FROM messages \
             WHERE (sender = ? OR receiver = ?) AND is_deleted IS NULL \
             ORDER BY send_at DESC",
            user_id as u64,
            user_id as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_all(&mut **tx).await,
            None => query.fetch_all(&self.pool).await,
        };
        result.unwrap_or(Vec::new())
    }

    pub async fn save_message(
        &self,
        sender: usize,
        receiver: usize,
        content: impl Into<String>,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<usize, HttpError> {
        debug!("Insert a message by the user with the id: {sender}");
        let content: String = content.into();
        if content.is_empty() {
            return Err(HttpError::bad_request(ErrorMessage::MessageHasNoContent));
        }

        let is_receiver_blocked: bool = self
            .get_blocked_user(sender, receiver, tx.as_deref_mut())
            .await
            .is_some();
        if is_receiver_blocked {
            return Err(HttpError::bad_request(ErrorMessage::YouHaveBlockedThisUser));
        }

        let are_friends: bool = self
            .get_friend(sender, receiver, tx.as_deref_mut())
            .await
            .is_some()
            || self
                .get_friend(receiver, sender, tx.as_deref_mut())
                .await
                .is_some()
            || sender == receiver;

        if !are_friends {
            return Err(HttpError::bad_request(ErrorMessage::NoFriendFound));
        }

        let query = sqlx::query!(
            "INSERT INTO messages \
             (sender, receiver, content) \
             VALUES (?, ?, ?)",
            sender as u64,
            receiver as u64,
            &content,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        let message_id = result
            .map_err(|e| {
                error!("{e}");
                HttpError::server_error()
            })?
            .last_insert_id();
        Ok(message_id as usize)
    }

    pub async fn update_message(
        &self,
        message_id: usize,
        new_content: impl Into<String>,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Update message with the id: {message_id}");
        let new_content: String = new_content.into();

        if new_content.is_empty() {
            return Err(HttpError::bad_request(ErrorMessage::MessageHasNoContent));
        }

        let query = sqlx::query!(
            "SELECT * FROM messages WHERE id = ? AND is_deleted IS NULL",
            message_id as u64
        );
        let result = match tx.as_mut() {
            Some(tx) => query.fetch_one(&mut ***tx).await,
            None => query.fetch_one(&self.pool).await,
        };
        result.map_err(|_| HttpError::bad_request(ErrorMessage::NoMessageFound))?;

        let query = sqlx::query!(
            "UPDATE messages SET content = ?, edited_at = ? WHERE id = ? AND is_deleted IS NULL",
            new_content,
            Utc::now(),
            message_id as u64,
        );
        let result = match tx.as_mut() {
            Some(tx) => query.execute(&mut ***tx).await,
            None => query.execute(&self.pool).await,
        };
        result.map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;

        Ok(())
    }

    pub async fn delete_message_for_sender(
        &self,
        message_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete message with the id: {message_id} only for the sender");
        let query = sqlx::query!(
            "UPDATE messages SET is_deleted_by_sender = CURRENT_TIMESTAMP(3) \
             WHERE id = ? AND is_deleted IS NULL",
            message_id as u64,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result.map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;
        Ok(())
    }

    pub async fn delete_message_for_receiver(
        &self,
        message_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete message with the id: {message_id} only for the receiver");
        let query = sqlx::query!(
            "UPDATE messages SET is_deleted_by_receiver = CURRENT_TIMESTAMP(3) \
             WHERE id = ? AND is_deleted IS NULL",
            message_id as u64,
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result.map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;
        Ok(())
    }

    pub async fn delete_message(
        &self,
        message_id: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete the message with the message_id: {message_id}");
        let query = sqlx::query!(
            "UPDATE messages SET is_deleted = CURRENT_TIMESTAMP(3) WHERE id = ?",
            message_id as u64
        );
        let result = match tx {
            Some(tx) => query.execute(&mut **tx).await,
            None => query.execute(&self.pool).await,
        };
        result.map_err(|e| {
            error!("{e}");
            HttpError::server_error()
        })?;
        Ok(())
    }

    pub async fn delete_messages(
        &self,
        user_id: usize,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete all the message of the user with the id: {user_id}");
        let messages: Vec<DbMessage> = self.get_messages(user_id, tx.as_deref_mut()).await;
        for message in messages {
            self.delete_message(message.id as usize, tx.as_deref_mut())
                .await?;
        }

        Ok(())
    }

    pub async fn get_chat(
        &self,
        user_id: usize,
        user_id_of_friend: usize,
        tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Vec<DbMessage> {
        debug!("Get a whole chat of messages of the user: {user_id} with the user: {user_id_of_friend}");
        let query = sqlx::query_as!(
            DbMessage,
            "SELECT * FROM messages \
             WHERE ((sender = ? AND receiver = ?) \
             OR (receiver= ? AND sender = ?)) \
             AND is_deleted IS NULL \
             ORDER BY send_at DESC",
            user_id as u64,
            user_id_of_friend as u64,
            user_id as u64,
            user_id_of_friend as u64,
        );
        let result = match tx {
            Some(tx) => query.fetch_all(&mut **tx).await,
            None => query.fetch_all(&self.pool).await,
        };
        result.unwrap_or(Vec::new())
    }

    pub async fn delete_chat(
        &self,
        user_id: usize,
        user_id_of_friend: usize,
        mut tx: Option<&mut Transaction<'_, MySql>>,
    ) -> Result<(), HttpError> {
        debug!("Delete a whole chat of messages of the user: {user_id} with the user: {user_id_of_friend}");
        let chat: Vec<DbMessage> = self
            .get_chat(user_id, user_id_of_friend, tx.as_deref_mut())
            .await;

        for message in chat {
            let is_user_sender: bool = user_id == message.sender as usize;
            let is_user_receiver: bool = user_id == message.receiver as usize;

            if is_user_sender {
                self.delete_message_for_sender(message.id as usize, tx.as_deref_mut())
                    .await?;
            } else if is_user_receiver {
                self.delete_message_for_receiver(message.id as usize, tx.as_deref_mut())
                    .await?;
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::config::Config;
    use crate::models::ClientPost;
    use crate::utils::test_utils;
    use chrono::{DateTime, Utc};
    use pretty_assertions::{assert_eq, assert_ne};
    use sqlx::mysql::MySqlPool;

    // ------------------------------------------------------------------------- Users
    #[sqlx::test]
    async fn test_get_user_by_id(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::from_pool(pool);
        let user: DbUser = dbclient.get_user(1, None).await.unwrap();
        assert_eq!(user.id, 1);
    }

    #[sqlx::test]
    async fn test_get_user_by_username(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::from_pool(pool);
        let user: DbUser = dbclient.get_user_by_username("marc", None).await.unwrap();
        assert_eq!(user.username, "marc");
    }

    #[sqlx::test]
    async fn test_get_user_by_email(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::from_pool(pool);
        let user: DbUser = dbclient
            .get_user_by_email("marc@mail.de", None)
            .await
            .unwrap();
        assert_eq!(user.email, "marc@mail.de");
    }

    #[sqlx::test]
    async fn test_get_users(pool: MySqlPool) {
        let dbclient: DbClient = DbClient::from_pool(pool);
        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("amarc", "amarc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("bmarc", "bmarc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        let users: Vec<DbUser> = dbclient.get_users("marc", None).await;
        assert_eq!(users.len(), 3);
    }

    #[sqlx::test]
    async fn test_get_users_is_sorted_by_username_alphabetically(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_user("a_user", "a@mail", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("b_user", "b@mail", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("c_user", "c@mail", "1234asdf", None)
            .await
            .unwrap();
        let users: Vec<DbUser> = dbclient.get_users("user", None).await;
        assert_eq!(users[0].username, "a_user");
        assert_eq!(users[1].username, "b_user");
        assert_eq!(users[2].username, "c_user");
    }

    #[sqlx::test]
    async fn test_get_users_email_has_to_exact_to_work(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        assert_eq!(dbclient.get_users("marc@web.de", None).await.len(), 0);
        assert_eq!(dbclient.get_users("marc@mail.de", None).await.len(), 1);
    }

    #[sqlx::test]
    async fn test_save_user(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::from_pool(pool);
        dbclient
            .save_user("test", "test@mail.de", "1234asdf", None)
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user_by_username("test", None).await.unwrap();
        assert_eq!(user.username, "test");
        assert_eq!(user.email, "test@mail.de");
    }

    #[sqlx::test]
    async fn test_save_user_hashed_password(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::from_pool(pool);
        dbclient
            .save_user("test", "test@mail.de", "1234asdf", None)
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user_by_username("test", None).await.unwrap();
        assert_ne!(user.password, "1234asdf");
    }

    #[sqlx::test]
    async fn test_save_user_but_username_is_taken(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::from_pool(pool);
        let save_failed: bool = dbclient
            .save_user("marc", "marc_two@mail.de", "1234asdf", None)
            .await
            .is_err();
        assert_eq!(save_failed, true);
    }

    #[sqlx::test]
    async fn test_save_user_but_email_is_taken(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::from_pool(pool);
        let save_failed: bool = dbclient
            .save_user("marc_two", "marc@mail.de", "1234asdf", None)
            .await
            .is_err();
        assert_eq!(save_failed, true);
    }

    #[sqlx::test]
    async fn test_save_user_uses_username_for_display(pool: MySqlPool) {
        let dbclient: DbClient = DbClient::from_pool(pool);
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        let user = dbclient.get_user(1, None).await.unwrap();
        assert_eq!(user.display_username, user.username);
    }

    #[sqlx::test]
    fn test_save_user_without_email(pool: MySqlPool) {
        let dbclient: DbClient = DbClient::from_pool(pool);
        dbclient
            .save_user("marc", "", "asdfasdf", None)
            .await
            .unwrap();
        let user = dbclient.get_user(1, None).await.unwrap();
        assert_eq!(user.email, "");
    }

    #[sqlx::test]
    async fn test_delete_user(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::from_pool(pool);
        dbclient.delete_user(1, None).await.unwrap();
        let user: Option<DbUser> = dbclient.get_user(1, None).await;
        assert_eq!(user.is_some(), false);
    }

    #[sqlx::test]
    fn test_delete_user_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient
            .save_user("marc", "marc@mail.de", "asdf1234", None)
            .await
            .unwrap();
        assert!(dbclient.get_user(1, None).await.is_some());
        dbclient.delete_user(1, None).await.unwrap();
        let user = sqlx::query_as!(DbUser, "SELECT * FROM users WHERE id = 1")
            .fetch_one(&pool)
            .await
            .unwrap();
        assert!(user.is_deleted.is_some());
    }

    #[sqlx::test]
    async fn test_update_user(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::from_pool(pool);
        let password_before: String = dbclient.get_user(1, None).await.unwrap().password;
        dbclient
            .update_user(
                1,
                Some("leon"),
                Some("leon_the_king"),
                Some("asdf1234"),
                Some("leon@mail.de"),
                Some(Status::Away),
                None,
            )
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user(1, None).await.unwrap();
        assert_eq!(user.username, "leon");
        assert_eq!(user.display_username, "leon_the_king");
        assert_ne!(password_before, user.password);
        assert_eq!(user.email, "leon@mail.de");
        assert_eq!(user.status, "away");
    }

    #[sqlx::test]
    async fn test_update_user_but_username_is_taken(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::from_pool(pool);
        let update_failed: bool = dbclient
            .update_user(1, Some("michelle"), None, None, None, None, None)
            .await
            .is_err();
        assert_eq!(update_failed, true);
    }

    #[sqlx::test]
    async fn test_update_user_but_email_is_taken(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::from_pool(pool);
        let update_failed: bool = dbclient
            .update_user(1, None, None, None, Some("michelle@mail.de"), None, None)
            .await
            .is_err();
        assert_eq!(update_failed, true);
    }

    // ------------------------------------------------------------------------- Tokens
    #[sqlx::test]
    async fn test_get_refresh_tokens(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_refresh_token(1, "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_refresh_token(1, "asdf1234", None)
            .await
            .unwrap();
        let tokens: Vec<String> = dbclient.get_refresh_tokens(1, None).await;
        assert_eq!(tokens.len(), 2);
    }

    #[sqlx::test]
    async fn test_get_refresh_token(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_refresh_token(1, "1234asdf", None)
            .await
            .unwrap();
        let is_token: bool = dbclient.get_refresh_token("1234asdf", None).await.is_some();
        assert_eq!(is_token, true);
    }

    #[sqlx::test]
    async fn test_save_refresh_token(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let is_saved: bool = dbclient
            .save_refresh_token(1, "1234asdf", None)
            .await
            .is_ok();
        assert_eq!(is_saved, true);
    }

    #[sqlx::test]
    async fn test_delete_refresh_tokens(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_refresh_token(1, "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_refresh_token(1, "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_refresh_token(1, "1234asdf", None)
            .await
            .unwrap();
        dbclient.delete_refresh_tokens(1, None).await.unwrap();
        let refresh_tokens: Vec<String> = dbclient.get_refresh_tokens(1, None).await;
        assert_eq!(refresh_tokens.len(), 0);
    }

    #[sqlx::test]
    fn test_delete_refresh_tokens_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient.save_refresh_token(1, "shhh!", None).await.unwrap();
        assert!(dbclient.get_refresh_token("shhh!", None).await.is_some());
        dbclient.delete_refresh_tokens(1, None).await.unwrap();
        let refresh_token = sqlx::query_as!(DbToken, "SELECT * FROM tokens WHERE id = 1")
            .fetch_one(&pool)
            .await
            .unwrap();
        assert!(refresh_token.is_deleted.is_some());
    }

    #[sqlx::test]
    async fn test_delete_refresh_token(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_refresh_token(1, "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .delete_refresh_token("1234asdf", None)
            .await
            .unwrap();
        let is_token: bool = dbclient.get_refresh_token("1234asdf", None).await.is_some();
        assert_eq!(is_token, false);
    }

    #[sqlx::test]
    fn test_delete_refresh_token_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient.save_refresh_token(1, "shhh!", None).await.unwrap();
        assert!(dbclient.get_refresh_token("shhh!", None).await.is_some());
        dbclient.delete_refresh_token("shhh!", None).await.unwrap();
        let refresh_token = sqlx::query_as!(DbToken, "SELECT * FROM tokens WHERE id = 1")
            .fetch_one(&pool)
            .await
            .unwrap();
        assert!(refresh_token.is_deleted.is_some());
    }

    // ------------------------------------------------------------------------- Friends
    #[sqlx::test]
    async fn test_get_friend(pool: MySqlPool) {
        let dbclient: DbClient = DbClient::from_pool(pool);
        dbclient.save_friend(1, 2, None).await.unwrap();
        let friend: Option<DbFriend> = dbclient.get_friend(1, 2, None).await;
        assert_eq!(friend.is_some(), true);
    }

    #[sqlx::test]
    async fn test_get_friend_with_swapped_ids(pool: MySqlPool) {
        let dbclient: DbClient = DbClient::from_pool(pool);
        dbclient.save_friend(1, 2, None).await.unwrap();
        let friend: Option<DbFriend> = dbclient.get_friend(2, 1, None).await;
        assert_eq!(friend.is_some(), true);
    }

    #[sqlx::test]
    async fn test_get_friends(pool: MySqlPool) {
        let dbclient: DbClient = DbClient::from_pool(pool);
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(1, 3, None).await.unwrap();
        dbclient.save_friend(2, 3, None).await.unwrap();
        let friends: Vec<DbFriend> = dbclient.get_friends(1, None).await;
        assert_eq!(friends.len(), 2);
    }

    #[sqlx::test]
    async fn test_get_friends_as_user(pool: MySqlPool) {
        let dbclient: DbClient = DbClient::from_pool(pool);
        dbclient
            .save_user("marc", "marc@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("rena", "rena@langunion.com", "asdfasdf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(1, 3, None).await.unwrap();
        dbclient.save_friend(2, 3, None).await.unwrap();
        let friends: Vec<DbUser> = dbclient.get_friends_as_user(1, None).await;
        assert_eq!(friends.len(), 2);
        assert!(friends.contains(&dbclient.get_user(3, None).await.unwrap()));
        assert!(friends.contains(&dbclient.get_user(2, None).await.unwrap()));
    }

    #[sqlx::test]
    async fn test_save_friend(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient.save_friend(1, 2, None).await.unwrap();
        let friends: Vec<DbFriend> = dbclient.get_friends(1, None).await;
        assert_eq!(friends.len(), 1);
    }

    #[sqlx::test]
    async fn test_save_friend_cannot_add_yourself(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let failed: bool = dbclient.save_friend(1, 1, None).await.is_err();
        assert!(failed);
    }

    #[sqlx::test]
    async fn test_delete_friend(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(1, 3, None).await.unwrap();
        let friends_before: Vec<DbFriend> = dbclient.get_friends(1, None).await;
        assert_eq!(friends_before.len(), 2);
        dbclient.delete_friend(1, 2, None).await.unwrap();
        let friends_after: Vec<DbFriend> = dbclient.get_friends(1, None).await;
        assert_eq!(friends_after.len(), 1);
    }

    #[sqlx::test]
    fn test_delete_friend_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient.save_friend(1, 2, None).await.unwrap();
        assert!(dbclient.get_friend(1, 2, None).await.is_some());
        dbclient.delete_friend(1, 2, None).await.unwrap();
        let friend = sqlx::query_as!(DbFriend, "SELECT * FROM friends WHERE id = 1")
            .fetch_one(&pool)
            .await
            .unwrap();
        assert!(friend.is_deleted.is_some());
    }

    #[sqlx::test]
    async fn test_delete_friend_deletes_correct_friend(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(2, 3, None).await.unwrap();
        dbclient.save_friend(1, 3, None).await.unwrap();
        dbclient.delete_friend(1, 2, None).await.unwrap();
        assert!(dbclient.get_friend(1, 2, None).await.is_none());
        assert!(dbclient.get_friend(2, 3, None).await.is_some());
        assert!(dbclient.get_friend(1, 3, None).await.is_some());
    }

    #[sqlx::test]
    async fn test_delete_friends(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(1, 3, None).await.unwrap();
        dbclient.delete_friends(1, None).await.unwrap();
        let deleted_friends: Vec<DbFriend> = dbclient.get_friends(1, None).await;
        assert_eq!(deleted_friends.is_empty(), true);
    }

    #[sqlx::test]
    async fn test_delete_friends_is_soft_delete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient.save_friend(1, 2, None).await.unwrap();
        assert!(dbclient.get_friend(1, 2, None).await.is_some());
        dbclient.delete_friends(1, None).await.unwrap();
        let friend = sqlx::query_as!(DbFriend, "SELECT * FROM friends WHERE id = 1")
            .fetch_one(&pool)
            .await
            .unwrap();
        assert!(friend.is_deleted.is_some());
    }

    #[sqlx::test]
    async fn test_delete_friends_deletes_correct_friends(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_friend(2, 3, None).await.unwrap();
        dbclient.save_friend(1, 3, None).await.unwrap();
        dbclient.delete_friends(1, None).await.unwrap();
        assert!(dbclient.get_friend(1, 2, None).await.is_none());
        assert!(dbclient.get_friend(2, 3, None).await.is_some());
        assert!(dbclient.get_friend(1, 3, None).await.is_none());
    }

    // ------------------------------------------------------------------------- Friendrequests
    #[sqlx::test]
    async fn test_get_friendrequest(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.get_friendrequest(1, 2, None).await.unwrap();
    }

    #[sqlx::test]
    async fn test_get_friendrequests(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let friendreqs: Vec<DbFriendrequest> = dbclient.get_friendrequests(1, None).await;
        assert_eq!(friendreqs.is_empty(), false);
    }

    #[sqlx::test]
    async fn test_get_friendrequests_is_sorted_by_id(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("thomas", "thomas@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient
            .save_user("michelle", "michelle@mail.de", "1234asdf", None)
            .await
            .unwrap();
        dbclient.save_friendrequest(1, 2, None).await.unwrap();
        dbclient.save_friendrequest(1, 3, None).await.unwrap();
        dbclient.save_friendrequest(1, 4, None).await.unwrap();
        let friendrequests: Vec<DbFriendrequest> = dbclient.get_friendrequests(1, None).await;
        assert_eq!(friendrequests[0].id, 1);
        assert_eq!(friendrequests[1].id, 2);
        assert_eq!(friendrequests[2].id, 3);
    }

    #[sqlx::test]
    async fn test_get_friendrequest_by_id(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.get_friendrequest_by_id(1, None).await.unwrap();
    }

    #[sqlx::test]
    async fn test_save_friendrequest(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.save_friendrequest(1, 2, None).await.unwrap();
    }

    #[sqlx::test]
    async fn test_delete_friendrequest(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_friendrequest(1, None).await.unwrap();
        let deleted_friendreq: Option<DbFriendrequest> =
            dbclient.get_friendrequest_by_id(1, None).await;
        assert_eq!(deleted_friendreq.is_none(), true);
    }

    #[sqlx::test]
    fn test_delete_friendrequest_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient
            .save_user("marc", "marc@mail.de", "asdf1324", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "asdf1324", None)
            .await
            .unwrap();
        dbclient.save_friendrequest(1, 2, None).await.unwrap();
        assert!(dbclient.get_friendrequest(1, 2, None).await.is_some());
        dbclient.delete_friendrequest(1, None).await.unwrap();
        let friendrequest =
            sqlx::query_as!(DbFriendrequest, "SELECT * FROM friendrequests WHERE id = 1")
                .fetch_one(&pool)
                .await
                .unwrap();
        assert!(friendrequest.is_deleted.is_some());
    }

    #[sqlx::test]
    async fn test_delete_friendrequests(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friendrequests(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_friendrequests(1, None).await.unwrap();
        let deleted_friendrequests: Vec<DbFriendrequest> =
            dbclient.get_friendrequests(1, None).await;
        assert_eq!(deleted_friendrequests.is_empty(), true);
    }

    #[sqlx::test]
    fn test_delete_friendrequests_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient
            .save_user("marc", "marc@mail.de", "asdf1324", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "asdf1324", None)
            .await
            .unwrap();
        dbclient.save_friendrequest(1, 2, None).await.unwrap();
        assert!(dbclient.get_friendrequest(1, 2, None).await.is_some());
        dbclient.delete_friendrequests(1, None).await.unwrap();
        let friendrequest =
            sqlx::query_as!(DbFriendrequest, "SELECT * FROM friendrequests WHERE id = 1")
                .fetch_one(&pool)
                .await
                .unwrap();
        assert!(friendrequest.is_deleted.is_some());
    }
    // ------------------------------------------------------------------------- Blocked Users
    #[sqlx::test]
    async fn test_get_blocked_user(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient.block_user(1, 2, None).await.unwrap();
        let blocked_user: Option<DbBlockedUser> = dbclient.get_blocked_user(1, 2, None).await;
        assert!(blocked_user.is_some());
    }

    #[sqlx::test]
    async fn test_get_blocked_users(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient.block_user(1, 2, None).await.unwrap();
        dbclient.block_user(1, 3, None).await.unwrap();
        dbclient.block_user(2, 1, None).await.unwrap();
        let blocked_users: Vec<DbBlockedUser> = dbclient.get_blocked_users(1, None).await;
        assert_eq!(blocked_users.len(), 2);
    }

    #[sqlx::test]
    async fn test_block_user(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient.block_user(1, 2, None).await.unwrap();
        let blocked_user: Option<DbBlockedUser> = dbclient.get_blocked_user(1, 2, None).await;
        assert!(blocked_user.is_some());
    }

    #[sqlx::test]
    async fn test_block_user_who_is_already_blocked(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let fst_try = dbclient.block_user(1, 2, None).await;
        let snd_try = dbclient.block_user(1, 2, None).await;
        assert!(fst_try.is_ok());
        assert!(snd_try.is_err());
    }

    #[sqlx::test]
    async fn test_unblock_user(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient.block_user(1, 2, None).await.unwrap();
        let unblocked = dbclient.unblock_user(1, 2, None).await;
        assert!(unblocked.is_ok());
    }

    #[sqlx::test]
    async fn test_unblock_user_whow_wasnt_blocked(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let unblocked = dbclient.unblock_user(1, 2, None).await;
        assert!(unblocked.is_err());
    }

    #[sqlx::test]
    async fn test_unblock_users(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient.block_user(1, 2, None).await.unwrap();
        dbclient.block_user(1, 3, None).await.unwrap();
        let unblocked_all: bool = dbclient.unblock_users(1, None).await.is_ok();
        let blocked_users: Vec<DbBlockedUser> = dbclient.get_blocked_users(1, None).await;
        assert!(unblocked_all);
        assert_eq!(blocked_users.len(), 0);
    }

    // ------------------------------------------------------------------------- Posts
    #[sqlx::test]
    async fn test_get_post(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let post: DbPost = dbclient.get_post(1, None).await.unwrap();
        assert_eq!(post.id, 1);
        assert_eq!(post.content, "First post!");
    }

    #[sqlx::test]
    async fn test_get_posts(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let posts: Vec<DbPost> = dbclient.get_posts(1, None).await;
        assert_eq!(posts.len(), 3);
    }

    #[sqlx::test]
    async fn test_get_posts_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_post(1, "this is a post", 0, None)
            .await
            .unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient
            .save_post(1, "this is a post", 0, None)
            .await
            .unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient
            .save_post(1, "this is a post", 0, None)
            .await
            .unwrap();
        let posts: Vec<DbPost> = dbclient.get_posts(1, None).await;
        assert_eq!(posts[0].id, 3);
        assert_eq!(posts[1].id, 2);
        assert_eq!(posts[2].id, 1);
    }

    #[sqlx::test]
    async fn test_get_posts_of_user_with_no_posts(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let posts: Vec<DbPost> = dbclient.get_posts(999, None).await;
        assert_eq!(posts.len(), 0);
    }

    #[sqlx::test]
    async fn test_save_post(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient.save_post(1, "new post", 0, None).await.unwrap();
    }

    #[sqlx::test]
    async fn test_save_post_returns_saved_post(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let saved_post_id: usize = dbclient.save_post(1, "new post", 0, None).await.unwrap();
        let saved_post: DbPost = dbclient.get_post(saved_post_id, None).await.unwrap();
        let getted_post: DbPost = dbclient.get_post(1, None).await.unwrap();
        assert_eq!(saved_post.id, getted_post.id);
        assert_eq!(saved_post.author, getted_post.author);
        assert_eq!(saved_post.content, getted_post.content);
        assert_eq!(saved_post.created_at, getted_post.created_at);
    }

    #[sqlx::test]
    async fn test_save_post_but_content_is_too_long(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let mut content = String::new();
        for _ in 0..=255 {
            content.push_str("a");
        }
        let failed: bool = dbclient.save_post(1, content, 0, None).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_delete_post(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_post(1, None).await.unwrap();
        let deleted: bool = dbclient.get_post(1, None).await.is_none();
        assert_eq!(deleted, true);
    }

    #[sqlx::test]
    fn test_delete_post_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient
            .save_post(1, "i went to spain!", 0, None)
            .await
            .unwrap();
        assert!(dbclient.get_post(1, None).await.is_some());
        dbclient.delete_post(1, None).await.unwrap();
        let post = sqlx::query_as!(DbPost, "SELECT * FROM posts WHERE id = 1")
            .fetch_one(&pool)
            .await
            .unwrap();
        assert!(post.is_deleted.is_some());
    }

    #[sqlx::test]
    async fn test_delete_post_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let failed: bool = dbclient.delete_post(1, None).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_delete_post_deletes_all_likes(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        test_utils::init_test_post_likes(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_post(1, None).await.unwrap();
        let likes: Vec<DbPostLike> = dbclient.get_post_likes(1, None).await;
        assert_eq!(likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_post_deletes_all_comments(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        test_utils::init_test_post_comments(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_post(1, None).await.unwrap();
        let comments: Vec<DbPostComment> = dbclient.get_post_comments(1, None).await;
        assert_eq!(comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_posts(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_posts(1, None).await.unwrap();
        let posts: Vec<DbPost> = dbclient.get_posts(1, None).await;
        assert_eq!(posts.len(), 0);
    }

    #[sqlx::test]
    fn test_delete_posts_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient
            .save_post(1, "i went to spain!", 0, None)
            .await
            .unwrap();
        assert!(dbclient.get_post(1, None).await.is_some());
        dbclient.delete_posts(1, None).await.unwrap();
        let post = sqlx::query_as!(DbPost, "SELECT * FROM posts WHERE id = 1")
            .fetch_one(&pool)
            .await
            .unwrap();
        assert!(post.is_deleted.is_some());
    }

    #[sqlx::test]
    async fn test_delete_posts_deletes_also_likes(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_posts(1, None).await.unwrap();
        let fst_deleted_post_likes: Vec<DbPostLike> = dbclient.get_post_likes(1, None).await;
        let snd_deleted_post_likes: Vec<DbPostLike> = dbclient.get_post_likes(3, None).await;
        assert_eq!(fst_deleted_post_likes.len(), 0);
        assert_eq!(snd_deleted_post_likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_posts_deletes_also_comments(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_posts(1, None).await.unwrap();
        let fst_deleted_post_comments: Vec<DbPostComment> =
            dbclient.get_post_comments(1, None).await;
        let snd_deleted_post_comments: Vec<DbPostComment> =
            dbclient.get_post_comments(3, None).await;
        assert_eq!(fst_deleted_post_comments.len(), 0);
        assert_eq!(snd_deleted_post_comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_get_post_likes(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        test_utils::init_test_post_likes(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let likes: Vec<DbPostLike> = dbclient.get_post_likes(1, None).await;
        assert_eq!(likes.len(), 2);
    }

    #[sqlx::test]
    async fn test_get_post_likes_of_post_with_no_likes(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let likes: Vec<DbPostLike> = dbclient.get_post_likes(1, None).await;
        assert_eq!(likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_post_likes(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        test_utils::init_test_post_likes(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_post_likes(1, None).await.unwrap();
        let likes: Vec<DbPostLike> = dbclient.get_post_likes(1, None).await;
        assert_eq!(likes.len(), 0);
    }

    #[sqlx::test]
    fn test_delete_post_likes_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient
            .save_post(1, "i went to spain!", 0, None)
            .await
            .unwrap();
        dbclient.like_post(1, 1, None).await.unwrap();
        assert!(dbclient.get_post(1, None).await.is_some());
        assert!(dbclient.get_post_likes(1, None).await.len() == 1);
        dbclient.delete_post_likes(1, None).await.unwrap();
        let like = sqlx::query_as!(DbPostLike, "SELECT * FROM post_likes WHERE id = 1")
            .fetch_one(&pool)
            .await
            .unwrap();
        assert!(like.is_deleted.is_some());
    }

    #[sqlx::test]
    async fn test_like_post(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        dbclient.like_post(1, 2, None).await.unwrap();
        let liked_post: ClientPost = dbclient
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient, None), &config.domain())
            .await;
        assert_eq!(liked_post.likes.len(), 1);
        assert_eq!(liked_post.likes[0], "http://localhost:4000/v1/users/2/");
    }

    #[sqlx::test]
    async fn test_like_post_twice(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.like_post(1, 2, None).await.unwrap();
        let failed: bool = dbclient.like_post(1, 2, None).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_like_post_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let failed: bool = dbclient.like_post(1, 1, None).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_unlike_post(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.like_post(1, 2, None).await.unwrap();
        dbclient.unlike_post(1, 2, None).await.unwrap();
        let unliked_post: ClientPost = dbclient
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient, None), "...")
            .await;
        assert_eq!(unliked_post.likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_unlike_post_that_i_didnt_like(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let failed: bool = dbclient.unlike_post(1, 2, None).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_unlike_post_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let error: HttpError = dbclient.unlike_post(1, 1, None).await.unwrap_err();
        assert_eq!(error.message, ErrorMessage::NoPostFound.to_string());
    }

    #[sqlx::test]
    async fn test_get_post_comment(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        test_utils::init_test_post_comments(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let comment: DbPostComment = dbclient.get_post_comment(1, None).await.unwrap();
        assert_eq!(comment.content, "lol");
    }

    #[sqlx::test]
    async fn test_get_post_comment_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let none: bool = dbclient.get_post_comment(1, None).await.is_none();
        assert_eq!(none, true);
    }

    #[sqlx::test]
    async fn test_get_post_comments(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        test_utils::init_test_post_comments(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let comments: Vec<DbPostComment> = dbclient.get_post_comments(1, None).await;
        assert_eq!(comments.len(), 3);
    }

    #[sqlx::test]
    async fn test_get_post_comments_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_post(1, "this is a post", 0, None)
            .await
            .unwrap();
        dbclient
            .save_post_comment(1, 1, "this is a comment", None)
            .await
            .unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient
            .save_post_comment(1, 1, "this is a comment", None)
            .await
            .unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient
            .save_post_comment(1, 1, "this is a comment", None)
            .await
            .unwrap();
        let post_comments: Vec<DbPostComment> = dbclient.get_post_comments(1, None).await;
        assert_eq!(post_comments[0].id, 3);
        assert_eq!(post_comments[1].id, 2);
        assert_eq!(post_comments[2].id, 1);
    }

    #[sqlx::test]
    async fn test_get_post_comments_of_post_that_has_no_comments(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let comments: Vec<DbPostComment> = dbclient.get_post_comments(1, None).await;
        assert_eq!(comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_save_post_comment(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let config = Config::new_test();
        dbclient.save_post_comment(1, 1, "lol", None).await.unwrap();
        let post: ClientPost = dbclient
            .get_post(1, None)
            .await
            .unwrap()
            .into_client((&dbclient, None), config.domain())
            .await;
        assert_eq!(post.comments[0].id, 1);
        assert_eq!(post.comments[0].author, "http://localhost:4000/v1/users/1/");
        assert_eq!(post.comments[0].content, "lol");
        assert_eq!(post.comments.len(), 1);
    }

    #[sqlx::test]
    async fn test_save_post_comment_to_post_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let failed: bool = dbclient.save_post_comment(1, 1, "lol", None).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_save_post_comment_that_is_too_long(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let mut content = String::new();
        for _ in 0..=255 {
            content.push_str("a");
        }
        let failed: bool = dbclient
            .save_post_comment(1, 1, content, None)
            .await
            .is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_delete_post_comment(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.save_post(1, "test post", 0, None).await.unwrap();
        dbclient
            .save_post_comment(1, 1, "test post comment", None)
            .await
            .unwrap();
        dbclient.delete_post_comment(1, None).await.unwrap();
        let comments: Vec<DbPostComment> = dbclient.get_post_comments(1, None).await;
        assert_eq!(comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_post_comment_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient
            .save_post(1, "i went to spain!", 0, None)
            .await
            .unwrap();
        dbclient
            .save_post_comment(1, 1, "oh cool", None)
            .await
            .unwrap();
        assert!(dbclient.get_post(1, None).await.is_some());
        assert!(dbclient.get_post_comment(1, None).await.is_some());
        dbclient.delete_post_comment(1, None).await.unwrap();
        let comment = sqlx::query_as!(DbPostComment, "SELECT * FROM post_comments WHERE id = 1")
            .fetch_one(&pool)
            .await
            .unwrap();
        assert!(comment.is_deleted.is_some());
    }

    #[sqlx::test]
    async fn test_delete_post_comment_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let failed: bool = dbclient.delete_post_comment(1, None).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_delete_post_comments(pool: MySqlPool) {
        test_utils::init_test_posts(&pool).await;
        test_utils::init_test_post_comments(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_post_comments(1, None).await.unwrap();
        let comments: Vec<DbPostComment> = dbclient.get_post_comments(1, None).await;
        assert_eq!(comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_post_comments_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient
            .save_post(1, "i went to spain!", 0, None)
            .await
            .unwrap();
        dbclient
            .save_post_comment(1, 1, "oh cool", None)
            .await
            .unwrap();
        assert!(dbclient.get_post(1, None).await.is_some());
        assert!(dbclient.get_post_comment(1, None).await.is_some());
        dbclient.delete_post_comments(1, None).await.unwrap();
        let comment = sqlx::query_as!(DbPostComment, "SELECT * FROM post_comments WHERE id = 1")
            .fetch_one(&pool)
            .await
            .unwrap();
        assert!(comment.is_deleted.is_some());
    }

    // ------------------------------------------------------------------------- Messages
    #[sqlx::test]
    async fn test_get_message(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.get_message(1, None).await.unwrap();
    }

    #[sqlx::test]
    async fn test_get_message_which_doesnt_exist(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let message: Option<DbMessage> = dbclient.get_message(999, None).await;
        assert_eq!(message.is_none(), true);
    }

    #[sqlx::test]
    async fn test_get_message_that_is_soft_deleted(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_message(1, None).await.unwrap();
        let failed: bool = dbclient.get_message(1, None).await.is_none();
        assert!(failed);
    }

    #[sqlx::test]
    async fn test_get_messages(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let messages: Vec<DbMessage> = dbclient.get_messages(1, None).await;
        assert_eq!(messages.len(), 3);
    }

    #[sqlx::test]
    async fn test_get_messages_has_no_soft_deleted_message(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_message(1, None).await.unwrap();
        let messages: Vec<DbMessage> = dbclient.get_messages(1, None).await;
        assert_eq!(messages.len(), 2);
    }

    #[sqlx::test]
    async fn test_get_messages_of_user_that_doesnt_exist(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let messages: Vec<DbMessage> = dbclient.get_messages(999, None).await;
        assert_eq!(messages.len(), 0);
    }

    #[sqlx::test]
    async fn test_get_messages_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let first_id: usize = dbclient.save_message(1, 2, "hiii", None).await.unwrap();
        let first: DbMessage = dbclient.get_message(first_id, None).await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        let second_id: usize = dbclient.save_message(1, 2, "haha", None).await.unwrap();
        let second: DbMessage = dbclient.get_message(second_id, None).await.unwrap();
        let messages: Vec<DbMessage> = dbclient.get_messages(1, None).await;
        assert_eq!(first.id, messages[1].id);
        assert_eq!(second.id, messages[0].id);
    }

    #[sqlx::test]
    async fn test_save_message(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_message(1, 2, "Oh hi Marc", None)
            .await
            .unwrap();
    }

    #[sqlx::test]
    async fn test_save_message_with_no_content(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let failed: bool = dbclient.save_message(1, 2, "", None).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_save_message_to_user_who_isnt_friend(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let failed: bool = dbclient.save_message(1, 999, "hi", None).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_save_message_to_yourself(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let message_id: usize = dbclient.save_message(1, 1, "hi", None).await.unwrap();
        let message: DbMessage = dbclient.get_message(message_id, None).await.unwrap();
        assert_eq!(message.sender, message.receiver);
    }

    #[sqlx::test]
    async fn test_save_message_to_user_who_is_blocked(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_blocked_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let failed: bool = dbclient.save_message(1, 2, "hiii", None).await.is_err();
        assert!(failed);
    }

    #[sqlx::test]
    async fn test_update_message(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.update_message(1, "test", None).await.unwrap();
    }

    #[sqlx::test]
    async fn test_update_message_updates_content(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.update_message(1, "test", None).await.unwrap();
        let updated_content: String = dbclient.get_message(1, None).await.unwrap().content;
        assert_eq!(updated_content, "test");
    }

    #[sqlx::test]
    async fn test_update_message_updates_edited_at(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let edited_at: Option<DateTime<Utc>> =
            dbclient.get_message(1, None).await.unwrap().edited_at;
        dbclient.update_message(1, "test", None).await.unwrap();
        let updated_edited_at: Option<DateTime<Utc>> =
            dbclient.get_message(1, None).await.unwrap().edited_at;
        assert_ne!(edited_at, updated_edited_at);
    }

    #[sqlx::test]
    async fn test_update_message_with_no_new_content(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let failed: bool = dbclient.update_message(1, "", None).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_update_message_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        let failed: bool = dbclient.update_message(1, "test", None).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_delete_message_for_sender(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_message_for_sender(1, None).await.unwrap();
        let message: DbMessage = dbclient.get_message(1, None).await.unwrap();
        assert!(message.is_deleted_by_sender.is_some());
    }

    #[sqlx::test]
    async fn test_delete_message_for_receiver(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_message_for_receiver(1, None).await.unwrap();
        let message: DbMessage = dbclient.get_message(1, None).await.unwrap();
        assert!(message.is_deleted_by_receiver.is_some());
    }

    #[sqlx::test]
    async fn test_delete_message_that_is_i_sent_to_myself(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_message(1, 1, "this is a message for just myself", None)
            .await
            .unwrap();
        dbclient.delete_message(1, None).await.unwrap();
        let is_deleted: bool = dbclient.get_message(1, None).await.is_none();
        assert!(is_deleted);
    }

    #[sqlx::test]
    async fn test_delete_message(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_message(1, None).await.unwrap();
        let is_deleted: bool = dbclient.get_message(1, None).await.is_none();
        assert!(is_deleted);
    }

    #[sqlx::test]
    async fn test_delete_message_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient
            .save_user("marc", "marc@mail.de", "asdf1234", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "asdf1234", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient
            .save_message(1, 2, "how are you?", None)
            .await
            .unwrap();
        dbclient.delete_message(1, None).await.unwrap();
        let message = sqlx::query_as!(DbMessage, "SELECT * FROM messages WHERE id = 1")
            .fetch_one(&pool)
            .await
            .unwrap();
        assert!(message.is_deleted.is_some());
    }

    #[sqlx::test]
    async fn test_delete_messages(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_messages(1, None).await.unwrap();
        let messages: Vec<DbMessage> = dbclient.get_messages(1, None).await;
        assert_eq!(messages.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_messages_is_softdelete(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool.clone());
        dbclient
            .save_user("marc", "marc@mail.de", "asdf1234", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "asdf1234", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient
            .save_message(1, 2, "how are you?", None)
            .await
            .unwrap();
        dbclient
            .save_message(2, 1, "im fine and you?", None)
            .await
            .unwrap();
        dbclient.delete_messages(1, None).await.unwrap();
        let messages = sqlx::query_as!(DbMessage, "SELECT * FROM messages WHERE id = 1 OR id = 2")
            .fetch_all(&pool)
            .await
            .unwrap();
        assert!(messages[0].is_deleted.is_some());
        assert!(messages[1].is_deleted.is_some());
    }

    #[sqlx::test]
    async fn test_get_chat(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let chat: Vec<DbMessage> = dbclient.get_chat(1, 2, None).await;
        assert_eq!(chat.len(), 3);
    }

    #[sqlx::test]
    async fn test_get_chat_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::from_pool(pool);
        dbclient
            .save_user("marc", "marc@mail.de", "1234adsf", None)
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234adsf", None)
            .await
            .unwrap();
        dbclient.save_friend(1, 2, None).await.unwrap();
        dbclient.save_message(1, 2, "hi", None).await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient.save_message(1, 2, "hi", None).await.unwrap();
        let chat: Vec<DbMessage> = dbclient.get_chat(1, 2, None).await;
        assert_eq!(chat[0].id, 2);
        assert_eq!(chat[1].id, 1);
    }

    #[sqlx::test]
    async fn tet_get_chat_with_whom_there_are_no_messages(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let chat: Vec<DbMessage> = dbclient.get_chat(1, 2, None).await;
        assert_eq!(chat.len(), 0);
    }

    #[sqlx::test]
    async fn test_get_chat_but_isnt_friend_anymore(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        let chat: Vec<DbMessage> = dbclient.get_chat(1, 2, None).await;
        assert_eq!(chat.len(), 3);
    }

    #[sqlx::test]
    async fn test_delete_chat(pool: MySqlPool) {
        test_utils::init_test_users(&pool).await;
        test_utils::init_test_friends(&pool).await;
        test_utils::init_test_messages(&pool).await;
        let dbclient = DbClient::from_pool(pool);
        dbclient.delete_chat(1, 2, None).await.unwrap();
        let chat: Vec<DbMessage> = dbclient.get_chat(1, 2, None).await;

        for message in chat {
            let is_deleted: bool =
                message.is_deleted_by_sender.is_none() || message.is_deleted_by_receiver.is_none();
            assert!(is_deleted);
        }
    }
}
