use crate::app::SNAPSHOTS_DIRECTORY;
#[cfg(debug_assertions)] // DEV
use crate::cli::load_fixtures;
use crate::config::Config;
use chrono::{offset::Utc, DateTime};
use clap::{Args, Parser, Subcommand};
use inquire::Select;
use log::{debug, error, info};
use std::fs;
use std::path::PathBuf;
use std::process::Command;

#[derive(Parser, Debug)]
#[command(version, about)]
pub struct Cli {
    /// Set a custom config file
    #[arg(short, long, value_name = "FILE")]
    pub config: Option<PathBuf>,
    #[command(subcommand)]
    pub commands: Commands,
}

#[derive(Subcommand, PartialEq, Debug)]
pub enum Commands {
    /// Start the server
    Run,
    /// Manage the langunion database
    Db {
        #[command(subcommand)]
        commands: DbCommands,
    },
}

#[derive(Args, Debug, PartialEq, Clone)]
pub struct AuthFlags {
    /// If the database is on a remote server or in a container,
    /// you'll probably have to add the root user of that machine
    #[arg(short, long, value_name = "USERNAME")]
    username: Option<String>,
    /// If the database is on a remote server or in a container,
    /// you'll probably have to add the password of the root user of that machine
    #[arg(short, long, value_name = "PASSWORD")]
    password: Option<String>,
}

#[derive(Subcommand, PartialEq, Debug)]
pub enum DbCommands {
    /// Manage database snapshots
    Snapshot {
        #[command(subcommand)]
        commands: SnapshotCommands,
    },
    /// Create langunion database
    Create {
        #[command(flatten)]
        auth_flags: AuthFlags,
    },
    /// Change or create admin user for langunion db according to the config.
    SetAdmin {
        #[command(flatten)]
        auth_flags: AuthFlags,
    },
    /// Load fixtures into langunion db
    #[cfg(debug_assertions)] // DEV
    LoadFixtures,
}

#[derive(Subcommand, PartialEq, Debug)]
pub enum SnapshotCommands {
    /// Create a new snapshot
    Create {
        #[command(flatten)]
        auth_flags: AuthFlags,
    },
    /// Rollback to a snapshot
    Rollback {
        #[command(flatten)]
        auth_flags: AuthFlags,
    },
}

impl Cli {
    /// Execute apropriate action according to the subcommand
    pub async fn do_action(self, config: Config) {
        match &self.commands {
            Commands::Db { commands, .. } => match commands {
                DbCommands::Snapshot { commands, .. } => match commands {
                    SnapshotCommands::Create { auth_flags } => {
                        let auth_flags = auth_flags.clone();
                        self.db_snapshot_create(config, auth_flags)
                    }
                    SnapshotCommands::Rollback { auth_flags } => {
                        let auth_flags = auth_flags.clone();
                        self.db_snapshot_rollback(config, auth_flags)
                    }
                },
                DbCommands::Create { auth_flags } => {
                    let auth_flags = auth_flags.clone();
                    self.db_create(config, auth_flags)
                }
                DbCommands::SetAdmin { auth_flags } => {
                    let auth_flags = auth_flags.clone();
                    self.db_set_admin(config, auth_flags)
                }
                #[cfg(debug_assertions)] // DEV
                DbCommands::LoadFixtures => load_fixtures(config).await,
            },
            _ => panic!("Unhandled command: {:?}", self.commands),
        }
    }

    fn db_snapshot_create(self, config: Config, auth_flags: AuthFlags) {
        let now: usize = Utc::now().timestamp() as usize;
        let path: String = SNAPSHOTS_DIRECTORY
            .as_path()
            .join(format!("{now}.sql"))
            .to_str()
            .unwrap()
            .to_string();

        let mut command = Command::new("mariadb-dump");
        command
            .arg("-h")
            .arg(&config.database_host)
            .arg("-P")
            .arg(&config.database_port.to_string());

        if let Some(username) = auth_flags.username {
            command.arg(format!("-u{username}"));
        } else {
            let username = &config.database_user;
            command.arg(format!("-u{username}"));
        }

        if let Some(password) = auth_flags.password {
            command.arg(format!("-p{password}"));
        } else {
            let password = &config.database_password;
            command.arg(format!("-p{password}"));
        }

        command
            .arg(&config.database_database_name)
            .arg("-r")
            .arg(&path);

        debug!("Create snapshot command: {:?}", command);

        let command_succeeded: bool = command.status().unwrap().success();
        let is_snapshot_file_empty: bool = fs::read(&path).unwrap().is_empty();

        if command_succeeded && !is_snapshot_file_empty {
            debug!("Created new snapshot: {path}");
            info!("Created new snapshot");
        } else {
            fs::remove_file(&path).unwrap();
            error!("Could not create a new snapshot");
            panic!("Could not create a new snapshot");
        }
    }

    fn db_snapshot_rollback(self, config: Config, auth_flags: AuthFlags) {
        #[derive(Debug)]
        struct Snapshot {
            path: PathBuf,
            _filename: String,
            timestamp_seconds: usize,
        }

        impl std::fmt::Display for Snapshot {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                let timestamp_pretty: String =
                    DateTime::from_timestamp(self.timestamp_seconds as i64, 0)
                        .unwrap()
                        .format("%F %a %T")
                        .to_string();

                write!(f, "{}", timestamp_pretty)
            }
        }

        let mut snapshots: Vec<Snapshot> = {
            match fs::read_dir(SNAPSHOTS_DIRECTORY.as_path()) {
                Ok(dir) => {
                    let mut snapshots = Vec::new();
                    for entry in dir {
                        if let Ok(entry) = entry {
                            let filename: String = entry.file_name().to_str().unwrap().to_string();
                            let timestamp_seconds: usize =
                                filename.split(".sql").collect::<Vec<&str>>()[0]
                                    .parse()
                                    .unwrap();

                            snapshots.push(Snapshot {
                                path: entry.path(),
                                _filename: filename,
                                timestamp_seconds,
                            });
                        } else {
                            continue;
                        }
                    }
                    snapshots
                }
                Err(e) => {
                    error!("{e}");
                    Vec::new()
                }
            }
        };
        snapshots.sort_by_key(|s| s.timestamp_seconds);
        snapshots = snapshots.into_iter().rev().collect();

        debug!("Found {} snapshots", snapshots.len());

        match Select::new("Please choose a snapshot to rollback to", snapshots).prompt() {
            Ok(choice) => {
                let mut command = Command::new("mariadb");
                command
                    .arg("-h")
                    .arg(&config.database_host)
                    .arg("-P")
                    .arg(&config.database_port.to_string());

                if let Some(username) = auth_flags.username {
                    command.arg(format!("-u{username}"));
                } else {
                    let username = &config.database_user;
                    command.arg(format!("-u{username}"));
                }

                if let Some(password) = auth_flags.password {
                    command.arg(format!("-p{password}"));
                } else {
                    let password = &config.database_password;
                    command.arg(format!("-p{password}"));
                }

                let path: &str = choice.path.to_str().unwrap();
                command
                    .arg(&config.database_database_name)
                    .arg("-e")
                    .arg(format!("source {path}"));

                debug!("Path to snapshot file: {path}");
                debug!("Rollback command: {:?}", command);

                if command.status().unwrap().success() {
                    info!("Rollbacked to {:#?}", choice);
                } else {
                    error!("Could not rollback to {:#?}", choice);
                }
            }
            Err(e) => error!("{e}"),
        };
    }

    fn db_create(self, config: Config, auth_flags: AuthFlags) {
        let database_name: &str = &config.database_database_name;
        let sql_command: String = format!("CREATE DATABASE IF NOT EXISTS {database_name};");

        if let Err(e) = DbCommands::execute_sql(sql_command, auth_flags, config) {
            error!("{e}");
        } else {
            info!("Created langunion_db database");
        }
    }

    fn db_set_admin(self, config: Config, auth_flags: AuthFlags) {
        let database_user: &str = &config.database_user;
        let database_password: &str = &config.database_password;
        // WTF why do i need to clone only this one
        let database_name: &str = &config.database_database_name.clone();

        let sql_command: String = format!(
            "CREATE OR REPLACE USER '{database_user}'@'%' IDENTIFIED BY '{database_password}'; \
             GRANT ALL PRIVILEGES ON {database_name}.* TO '{database_user}'@'%'; \
             FLUSH PRIVILEGES;"
        );

        if let Err(e) = DbCommands::execute_sql(sql_command, auth_flags, config) {
            error!("{e}");
        } else {
            info!("Created new admin user for {database_name} Database");
        }
    }
}

impl DbCommands {
    fn execute_sql(
        sql_command: impl Into<String>,
        auth_flags: AuthFlags,
        config: Config,
    ) -> Result<(), std::io::Error> {
        let sql_command: String = sql_command.into();

        debug!("Executing '{sql_command}'");

        let mut command = Command::new("mariadb");

        command
            .arg("-h")
            .arg(&config.database_host)
            .arg("-P")
            .arg(&config.database_port.to_string());

        if let Some(username) = auth_flags.username {
            command.arg(format!("-u{username}"));
        } else {
            command.arg(format!("-u{}", &config.database_user));
        }

        if let Some(password) = auth_flags.password {
            command.arg(format!("-p{password}"));
        } else {
            command.arg(format!("-p{}", &config.database_password));
        }

        command.arg("-e").arg(sql_command);

        debug!("Executing command: {:?}", command);

        // TODO unwraping is a bad idea because of bad error response
        command.spawn().unwrap().wait()?;
        Ok(())
    }
}
