use crate::config::Config;
use crate::dbclient::DbClient;
use fake::faker::{internet::en::FreeEmail, internet::en::Username, lorem::en::Sentence};
use fake::{Dummy, Fake, Faker};
use inquire::Confirm;
use log::{error, info};

pub async fn load_fixtures(config: Config) {
    let confirm = Confirm::new("Loading fixtures into the database means also that all data will be deleted beforhand. Are you sure?")
        .with_default(false)
        .with_help_message("Use this command only in development environments, never in production!")
        .prompt();

    match confirm {
        Ok(true) => {
            let dbclient = DbClient::from_url(config.database_url()).await;
            drop_all(&dbclient).await;
            load_dev_user(&dbclient).await;
            load_fake_users(&dbclient).await;
            load_fake_friends(&dbclient).await;
            load_fake_friendrequests(&dbclient).await;
            load_fake_messages(&dbclient).await;
            load_fake_posts(&dbclient).await;
            load_fake_post_likes_and_comments(&dbclient).await;
            load_fake_blocked_users(&dbclient).await;
            unfriend_users(&dbclient).await;
            info!("Loaded all fixtures into db");
        }
        _ => error!("Aborted 'load-fixtures' command"),
    }
}

async fn drop_all(dbclient: &DbClient) {
    let mut transaction = dbclient.begin_transaction().await.unwrap();
    dbclient
        .execute("TRUNCATE TABLE users", Some(&mut transaction))
        .await
        .unwrap();
    dbclient
        .execute("TRUNCATE TABLE tokens", Some(&mut transaction))
        .await
        .unwrap();
    dbclient
        .execute("TRUNCATE TABLE messages", Some(&mut transaction))
        .await
        .unwrap();
    dbclient
        .execute("TRUNCATE TABLE friendrequests", Some(&mut transaction))
        .await
        .unwrap();
    dbclient
        .execute("TRUNCATE TABLE friends", Some(&mut transaction))
        .await
        .unwrap();
    dbclient
        .execute("TRUNCATE TABLE blocked_users", Some(&mut transaction))
        .await
        .unwrap();
    dbclient
        .execute("TRUNCATE TABLE posts", Some(&mut transaction))
        .await
        .unwrap();
    dbclient
        .execute("TRUNCATE TABLE post_likes", Some(&mut transaction))
        .await
        .unwrap();
    dbclient
        .execute("TRUNCATE TABLE post_comments", Some(&mut transaction))
        .await
        .unwrap();
    DbClient::commit_transaction(transaction).await.unwrap();
}

async fn load_dev_user(dbclient: &DbClient) {
    let _ = dbclient
        .save_user("langunion", "dev@langunion.com", "asdfasdf", None)
        .await;
}

async fn load_fake_users(dbclient: &DbClient) {
    #[derive(Dummy)]
    struct FakeUser {
        #[dummy(faker = "Username()")]
        username: String,
        #[dummy(faker = "FreeEmail()")]
        email: String,
    }

    for _ in 0..=100 {
        let user: FakeUser = Faker.fake();
        let username: String = {
            let mut username: String = user.username;
            if username.len() < 4 {
                let difference: i8 = (username.len() - 4) as i8 * (-1);
                for _ in 0..difference {
                    username.push_str("x");
                }
                username
            } else if username.len() > 20 {
                let (username, _) = username.split_at(19);
                log::debug!("{}", username);
                username.into()
            } else {
                username
            }
        };

        dbclient
            .save_user(username, user.email, "asdfasdf", None)
            .await
            .unwrap();
    }
}

async fn load_fake_friends(dbclient: &DbClient) {
    for i in 2..=30 {
        dbclient.save_friend(1, i, None).await.unwrap();
    }
}

async fn load_fake_friendrequests(dbclient: &DbClient) {
    for i in 31..=40 {
        dbclient.save_friendrequest(1, i, None).await.unwrap();
    }

    for i in 41..=50 {
        dbclient.save_friendrequest(i, 1, None).await.unwrap();
    }
}

async fn load_fake_messages(dbclient: &DbClient) {
    for i in 1..=30 {
        for _ in 1..=50 {
            let content: String = Sentence(10..30).fake();
            dbclient.save_message(1, i, content, None).await.unwrap();

            let content: String = Sentence(10..30).fake();
            dbclient.save_message(i, 1, content, None).await.unwrap();
        }
    }
}

async fn load_fake_posts(dbclient: &DbClient) {
    for i in 1..=100 {
        let content: String = Sentence(10..30).fake();
        dbclient.save_post(i, content, 0, None).await.unwrap();
    }
}

async fn load_fake_post_likes_and_comments(dbclient: &DbClient) {
    for i in 1..=30 {
        // dev user comments/likes on friend's post
        let comment: String = Sentence(1..30).fake();
        dbclient
            .save_post_comment(i, 1, comment, None)
            .await
            .unwrap();
        let _ = dbclient.like_post(i, 1, None).await;

        // Friend comments/likes on dev user's post
        let comment: String = Sentence(1..30).fake();
        dbclient
            .save_post_comment(1, i, comment, None)
            .await
            .unwrap();
        let _ = dbclient.like_post(1, i, None).await;
    }
}

async fn load_fake_blocked_users(dbclient: &DbClient) {
    for i in 21..=30 {
        dbclient.block_user(1, i, None).await.unwrap();
    }
}

async fn unfriend_users(dbclient: &DbClient) {
    for i in 11..=20 {
        dbclient.delete_friend(1, i, None).await.unwrap();
    }
}
