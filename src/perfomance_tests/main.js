import http from "k6/http";
import { check ,sleep, group } from "k6";

export const options = {
    thresholds: {
        http_req_failed: [{ threshold: "rate<0.01", abortOnFail: true }],
        // 99% of requests should be below 1s
        http_req_duration: ["p(99)<1000"],
    },
    stages: [
        { duration: "10s", target: 20 },
        { duration: "50s", target: 20 },
        { duration: "50s", target: 40 },
        { duration: "50s", target: 60 },
        { duration: "50s", target: 80 },
        { duration: "50s", target: 100 },
        { duration: "50s", target: 120 },
        { duration: "50s", target: 140 },
        { duration: "50s", target: 150 },
        { duration: "50s", target: 160 },
        { duration: "50s", target: 170 },
        { duration: "50s", target: 180 },
        { duration: "50s", target: 190 },
        { duration: "50s", target: 200 },
    ],
};

export function setup() {
    const response = http.post(
        "http://localhost:4001/v1/tokens/",
        JSON.stringify({
            email: "dev@langunion.com",
            password: "asdfasdf",

        }),
        { headers: { "Content-Type": "application/json" } }
    );

    check(response, { "got auth token": (r) => !!response.json().access });
    return response.json().access;
}

export default function (bearer) {
    group("", () => {
        const res = http.get(
            "http://localhost:4001/v1/users/1",
            {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${bearer}`,
                },
            }
        );
        check(res, {
            "response code was 200": (res) => res.status == 200,
            "is authenticated": (r) => r.json().authenticated === true,
        });       
    });
}
