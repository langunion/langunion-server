#!/bin/bash
docker pull mariadb:latest
cargo install sqlx-cli

docker run --name langunion_db \
       -p 3306:3306 \
       -e MARIADB_ROOT_USER=root \
       -e MARIADB_ROOT_PASSWORD=root \
       -e MARIADB_DATABASE=languniondb \
       -v $PWD/docker/db/init.sql:/docker-entrypoint-initdb.d/init.sql \
       -d mariadb:lts

echo "Wait a minute so the db starts"
sleep 60
sqlx migrate run
