CREATE DATABASE IF NOT EXISTS languniondb;

CREATE OR REPLACE USER 'langunion'@'%' IDENTIFIED BY 'langunionpassword';
GRANT ALL PRIVILEGES ON languniondb.* TO 'langunion'@'%';
GRANT ALL PRIVILEGES ON *.* TO 'langunion'@'%'; -- Added for integration test using sqlx

FLUSH PRIVILEGES;
