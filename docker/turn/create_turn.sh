#!/bin/bash
docker pull coturn/coturn 

docker run \
       --name langunion_turn \
       --network host \
       -d coturn/coturn \
       -n \
       --listening-port=3478 \
       --min-port=49152 --max-port=65535 \
       --external-ip='$(detect-external-ip)' \
       --verbose \
       --fingerprint \
       --lt-cred-mech \
       --server-name=turn.langunion.app \
       --realm=langunion.app \
       --user=turnlangunionapp:turnlangunionapppassword \
       --log-file=stdout \
       --pidfile=/var/tmp/turnserver.pid
