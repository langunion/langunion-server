<img 
    src="https://gitlab.com/langunion/langunion-server/-/raw/master/icon.webp?ref_type=heads"
    align="right"
    width="100"
    height="100" 
/>

# langunion-server 
> Where language and community meets.  
> Licensed under `GNU GPLv3 or later`

**langunion-server** is the backend server for all [langunion ](https://www.langunion.com) products.

It is used for all of these langunion projects:

- [Babbeln](https://gitlab.com/langunion/langunion-babbeln)
- [School](https://gitlab.com/langunion/langunion-school)
- [Website](https://gitlab.com/langunion/langunion-website)

The server handles:

* [ ] Instant messaging
* [ ] HD audio and video
* [ ] Content sharing

And many more!

## Running your own instance

At the moment there is no binary that you could download, so you have to compile it on your own.
For compilation `rust` and `docker`.

Use these make commands to make your life easier:

|                |                                                             |
|----------------|-------------------------------------------------------------|
| prepare        | Install all the necessary tools for running the dev server  |
| start          | Start the dev server                                        |
| install        | Install a binary on the current system                      |
|                |                                                             |
| swagger_start  | Start the swagger/openapi server for rest api documentation |
| swagger_stop   | Manually stop the swagger server                            |
|                |                                                             |
| asyncapi_start | Start the asyncapi server for websocket api documentation   |
| asyncapi_stop  | Manually stop the asyncapi server                           |
|                |                                                             |
| db_create      | Create the mariadb server                                   |
| db_start       | Start the mariadb server                                    |
| db_stop        | Stop the mariadb server                                     |
| db_nuke        | Remove the db completely                                    |

## Security

www.langunion.com/secruity

## Contributing

If you are looking to contribute to langunion, first of all, thank you! Please
see our [developer guide](www.langunion.com/developer-guide).

<br />
<br />

<footer>
    <p style="font-size: smaller; text-align: center;">
        Built with ❤️ by the at the moment only me Marc.
    </p>
</footer>
