prepare:
	@pacman -S rustup
	@rustup override 1.82.0
start:
	@cargo watch -x "run run"
build:
	@cargo build --release
test:
	@cargo test
test_perfomance:
	@ K6_WEB_DASHBOARD=true K6_WEB_DASHBOARD_OPEN=true k6 run ./perfomance_tests/main.js
load_fixtures:
	@cargo build
	@./target/debug/langunion-server db load-fixtures

db_create:
	@bash ./docker/db/create_db.sh
db_start:
	@docker start langunion_db
db_stop:
	@docker stop langunion_db
db_nuke: db_stop
	@docker rm langunion_db
